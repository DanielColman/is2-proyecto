#!/bin/bash
echo "---Base de datos bdpro para entorno de producción---"
echo "Borrando base de datos bdpro existente..."
dropdb -i --if-exists bdpro
if [ "$?" -ne 0 ]
then
    echo -e "No se pudo borrar la base de datos bdpro,  verifique que no esté siendo usada."
    exit 1
fi
echo "Se ha borrado la base de datos bdpro."
echo "Creando la base de datos bdpro..."
createdb bdpro
if [ "$?" -ne 0 ]
then
    echo -e "No se pudo crear bdpro"
    exit 2
fi
echo "Se ha creado la base de datos bdpro"

source .env/bin/activate
#python manage.py makemigrations
#python manage.py migrate
#python bddev_populate.py

cd /home/daniel
PGPASSWORD="postgres" pg_restore -h localhost -p 5432 -U postgres -d bdpro -v "bddevbu"
echo "bdpro se cargó exitosamente."
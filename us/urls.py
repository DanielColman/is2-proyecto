from django.urls import path, re_path


from django.conf.urls import include, url
from . import views

"""
Lista de URLs de User Stories
"""

urlpatterns = [
    url(r'^(?P<pk>[0-9]+)', views.product_backlog, name='p_b'),
    url(r'^elegir/(?P<pk>[0-9]+)', views.product_backlog_para_elegir, name='p_b2'),
    url(r'^sprint/(?P<pk>[0-9]+)', views.sprint_backlog_para_elegir, name='s_b'),
    url(r'^sprintb/(?P<pk>[0-9]+)', views.sprint_backlog, name='s_b2'),
    url(r'^finalizados/(?P<pk>[0-9]+)', views.finalizados, name='finalizados'),
    url(r'^crear/(?P<pk>[0-9]+)', views.crear),
    url(r'^crear2/(?P<pk>[0-9]+)', views.crear2),
    url(r'^add/(?P<pk>[0-9]+)', views.add_tablero),
    url(r'^modificar/(?P<pk>[0-9]+)', views.modificar),
    url(r'^eliminar/(?P<pk>[0-9]+)', views.eliminar),
    url(r'^avanzar/(?P<pk>[0-9]+)', views.avanzar),
    url(r'^retroceder/(?P<pk>[0-9]+)', views.retroceder),
    url(r'^adjuntar/(?P<pk>[0-9]+)', views.adjuntar, name='adjuntar'),
    url(r'^buscarus/(?P<pk>[0-9]+)', views.buscarus),
    url(r'^a_sprint/(?P<proyecto_id>[0-9]+)/(?P<us_id>[0-9]+)', views.a_sprint),
    url(r'^a_product/(?P<proyecto_id>[0-9]+)/(?P<us_id>[0-9]+)', views.a_product),
    
   re_path(r'^daily/(?P<us_id>[0-9]+)/(?P<adelante>[0-1]+)', views.addDaily, name='cambio_estado'),

   #url(r'^daily/(?P<us_id>[0-9]+)/(?P<adelante>[0-1]+)', views.addDaily, name='cambio_estado'),
    url(r'^listar_archivos_notas/(?P<us_id>[0-9]+)', views.listar_archivos_notas),
    url(r'^calidad/(?P<us_id>[0-9]+)', views.calidad),
    url(r'^calidad_listo/(?P<us_id>[0-9]+)', views.calidad_listo),
    url(r'^horas/(?P<us_id>[0-9]+)', views.horas),
    url(r'^reportebacklog/(?P<id_proyecto>[0-9]+)', views.reportebacklog),
    url(r'^reporteprioridad/(?P<pk>[0-9]+)', views.reporteprioridad),
    url(r'^horasreales/(?P<pk>[0-9]+)', views.horasreales),
    url(r'^reportesprint/(?P<pk>[0-9]+)', views.reportesprint),
    url(r'^reportesprint2/(?P<pk>[0-9]+)', views.reportesprint2),

]

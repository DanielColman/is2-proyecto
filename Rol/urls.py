from django.conf.urls import include, url
from . import views

"""
Lista de urls de Rol
"""

urlpatterns = [
        url(r'^$', views.index),
        url(r'^visualizar/(?P<id_rol>[0-9]+)/$', views.list_rol, name='list_rol'),
        url(r'^confirmado$', views.confirmado, name='confirmado'),
        url(r'^create$', views.rol_view, name='rol_crear'),
        url(r'^create2$', views.rol_view2, name='rol_crear2'),
        url(r'^create/confirmado.html$', views.confirmado2, name='confirmado'),
        url(r'^create2/confirmado3.html$', views.confirmado3, name='confirmado3'),
        url(r'^update/(?P<id_rol>[0-9]+)/$', views.modificar_rol, name='update_rol'),
        url(r'^update/(?P<pk>[0-9]+)/confirmado.html$', views.confirmado, name='confirmado'),
        url(r'^eliminar/(?P<id_rol>\d+)/$', views.rol_eliminar, name='rol_eliminar'),
        url(r'^eliminar/(?P<pk>[0-9]+)/confirmado.html$', views.confirmado, name='confirmado'),
        url(r'^search/$', views.rol_buscar, name='search'),
        url(r'^search/$', views.rol_buscar, name='rol_buscar'),
]
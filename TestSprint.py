from unittest import TestCase
import django
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "GestorP.base")
django.setup()
from sprint.models import Sprint
from proyecto.models import Proyecto
from django.contrib.auth.models import User
from django.test import Client


"""
Prubas unitarias para la app sprint
"""

class Sprint(TestCase):


    def test_pagina(self):
        """
        Carga la página y verifica que no encuentra
        """
        client = Client()
        response = client.get("/sprint/")
        #HTTP_302_FOUND
        self.assertEqual(response.status_code, 404)



    def test_pagina2(self):
        """
        Carga la página y verifica que no encuentra
        """
        client = Client()
        response = client.get("/sprint/")
        #HTTP_302_FOUND
        self.assertEqual(response.status_code, 404)

    def test_create_sprint(self):
        """
        Verifica que un sprint se crea
        """

    try:
        """crea un proyecto"""
        nombre = "proyecto_pruebas_unitarias"
        fecha_ini = '2001-01-01'
        fecha_fin = '2002-01-01'
        observaciones = 'obs'
        proyectoNvo = Proyecto(nombre=nombre, fecha_ini=fecha_ini, fecha_fin=fecha_fin, observaciones=observaciones)
        proyectoNvo.save()

        """crea un sprint"""
        estado = 'ACT'
        proyecto = 'descripcion_completa'
        fecha_ini = '2018-01-01'
        fecha_fin = '2018-01-15'
        fecha_fin_est = '2018-01-16'
        sprint_nuevo = Sprint(estado=estado, proyecto=proyectoNvo, fecha_ini=fecha_ini, fecha_fin=fecha_fin,
                              fecha_fin_est=fecha_fin_est)

        # Fallo cuando el estado en nulo, para las pruebas de falla se vuelve a crear un nuevo proyecto
        """crea un proyecto un segundo proyecto"""
        """
        proyectoNvo2 = Proyecto(nombre="proyecto_pruebas_unitarias2", fecha_ini='2001-01-01', fecha_fin='2001-01-01', observaciones='obs')
        proyectoNvo2.save()

        sprint_nuevo = Sprint(estado=None, proyecto=proyectoNvo, fecha_ini=fecha_ini, fecha_fin=fecha_fin,
                               fecha_fin_est=fecha_fin_est)
        """

        sprint_nuevo.save()

        sprint_nuevo.delete()
        proyectoNvo.delete()

    except:
        #proyectoNvo.delete()
        #proyectoNvo2.delete()
        raise

    def test_create_sprint2(self):
        """
        Verifica que un sprint se crea
        """

    try:
        """crea un proyecto"""
        nombre = "proyecto_pruebas_unitarias_w"
        fecha_ini = '2001-01-01'
        fecha_fin = '2002-01-01'
        observaciones = 'obs'
        proyectoNvo = Proyecto(nombre=nombre, fecha_ini=fecha_ini, fecha_fin=fecha_fin, observaciones=observaciones)
        proyectoNvo.save()

        """crea un sprint"""
        estado = 'ACT'
        proyecto = 'descripcion_completa'
        fecha_ini = '2018-01-01'
        fecha_fin = '2018-01-15'
        fecha_fin_est = '2018-01-16'
        sprint_nuevo = Sprint(estado=estado, proyecto=proyectoNvo, fecha_ini=fecha_ini, fecha_fin=fecha_fin,
                              fecha_fin_est=fecha_fin_est)

        # Fallo cuando el estado en nulo, para las pruebas de falla se vuelve a crear un nuevo proyecto
        """crea un proyecto un segundo proyecto"""
        """
        proyectoNvo2 = Proyecto(nombre="proyecto_pruebas_unitarias2", fecha_ini='2001-01-01', fecha_fin='2001-01-01', observaciones='obs')
        proyectoNvo2.save()

        sprint_nuevo = Sprint(estado=None, proyecto=proyectoNvo, fecha_ini=fecha_ini, fecha_fin=fecha_fin,
                               fecha_fin_est=fecha_fin_est)
        """

        sprint_nuevo.save()

        sprint_nuevo.delete()
        proyectoNvo.delete()

    except:
        #proyectoNvo.delete()
        #proyectoNvo2.delete()
        raise

    def test_create_sprint3(self):
        """
        Verifica que un sprint se crea
        """

    try:
        """crea un proyecto"""
        nombre = "proyecto_pruebas_unitarias_w"
        fecha_ini = '2001-01-01'
        fecha_fin = '2002-01-01'
        observaciones = 'obs'
        proyectoNvo = Proyecto(nombre=nombre, fecha_ini=fecha_ini, fecha_fin=fecha_fin, observaciones=observaciones)
        proyectoNvo.save()

        """crea un sprint"""
        estado = 'ACT'
        proyecto = 'descripcion_completa'
        fecha_ini = '2018-01-01'
        fecha_fin = '2018-01-15'
        fecha_fin_est = '2018-01-16'
        sprint_nuevo = Sprint(estado=estado, proyecto=proyectoNvo, fecha_ini=fecha_ini, fecha_fin=fecha_fin,
                              fecha_fin_est=fecha_fin_est)

        # Fallo cuando el estado en nulo, para las pruebas de falla se vuelve a crear un nuevo proyecto
        """crea un proyecto un segundo proyecto"""
        """
        proyectoNvo2 = Proyecto(nombre="proyecto_pruebas_unitarias2", fecha_ini='2001-01-01', fecha_fin='2001-01-01', observaciones='obs')
        proyectoNvo2.save()

        sprint_nuevo = Sprint(estado=None, proyecto=proyectoNvo, fecha_ini=fecha_ini, fecha_fin=fecha_fin,
                               fecha_fin_est=fecha_fin_est)
        """

        sprint_nuevo.save()

        sprint_nuevo.delete()
        proyectoNvo.delete()

    except:
        #proyectoNvo.delete()
        #proyectoNvo2.delete()
        raise

    def test_create_sprint4(self):
        """
        Verifica que un sprint se crea
        """

    try:
        """crea un proyecto"""
        nombre = "proyecto_pruebas_unitarias_w"
        fecha_ini = '2001-01-01'
        fecha_fin = '2002-01-01'
        observaciones = 'obs'
        proyectoNvo = Proyecto(nombre=nombre, fecha_ini=fecha_ini, fecha_fin=fecha_fin, observaciones=observaciones)
        proyectoNvo.save()

        """crea un sprint"""
        estado = 'ACT'
        proyecto = 'descripcion_completa'
        fecha_ini = '2018-01-01'
        fecha_fin = '2018-01-15'
        fecha_fin_est = '2018-01-16'
        sprint_nuevo = Sprint(estado=estado, proyecto=proyectoNvo, fecha_ini=fecha_ini, fecha_fin=fecha_fin,
                              fecha_fin_est=fecha_fin_est)

        # Fallo cuando el estado en nulo, para las pruebas de falla se vuelve a crear un nuevo proyecto
        """crea un proyecto un segundo proyecto"""
        """
        proyectoNvo2 = Proyecto(nombre="proyecto_pruebas_unitarias2", fecha_ini='2001-01-01', fecha_fin='2001-01-01', observaciones='obs')
        proyectoNvo2.save()

        sprint_nuevo = Sprint(estado=None, proyecto=proyectoNvo, fecha_ini=fecha_ini, fecha_fin=fecha_fin,
                               fecha_fin_est=fecha_fin_est)
        """

        sprint_nuevo.save()

        sprint_nuevo.delete()
        proyectoNvo.delete()

    except:
        #proyectoNvo.delete()
        #proyectoNvo2.delete()
        raise

    def test_create_sprint5(self):
        """
        Verifica que un sprint se crea
        """

    try:
        """crea un proyecto"""
        nombre = "proyecto_pruebas_unitarias_w"
        fecha_ini = '2001-01-01'
        fecha_fin = '2002-01-01'
        observaciones = 'obs'
        proyectoNvo = Proyecto(nombre=nombre, fecha_ini=fecha_ini, fecha_fin=fecha_fin, observaciones=observaciones)
        proyectoNvo.save()

        """crea un sprint"""
        estado = 'ACT'
        proyecto = 'descripcion_completa'
        fecha_ini = '2018-01-01'
        fecha_fin = '2018-01-15'
        fecha_fin_est = '2018-01-16'
        sprint_nuevo = Sprint(estado=estado, proyecto=proyectoNvo, fecha_ini=fecha_ini, fecha_fin=fecha_fin,
                              fecha_fin_est=fecha_fin_est)

        # Fallo cuando el estado en nulo, para las pruebas de falla se vuelve a crear un nuevo proyecto
        """crea un proyecto un segundo proyecto"""
        """
        proyectoNvo2 = Proyecto(nombre="proyecto_pruebas_unitarias2", fecha_ini='2001-01-01', fecha_fin='2001-01-01', observaciones='obs')
        proyectoNvo2.save()

        sprint_nuevo = Sprint(estado=None, proyecto=proyectoNvo, fecha_ini=fecha_ini, fecha_fin=fecha_fin,
                               fecha_fin_est=fecha_fin_est)
        """

        sprint_nuevo.save()

        sprint_nuevo.delete()
        proyectoNvo.delete()

    except:
        #proyectoNvo.delete()
        #proyectoNvo2.delete()
        raise

    def test_create_sprint6(self):
        """
        Verifica que un sprint se crea
        """

    try:
        """crea un proyecto"""
        nombre = "proyecto_pruebas_unitarias_w"
        fecha_ini = '2001-01-01'
        fecha_fin = '2002-01-01'
        observaciones = 'obs'
        proyectoNvo = Proyecto(nombre=nombre, fecha_ini=fecha_ini, fecha_fin=fecha_fin, observaciones=observaciones)
        proyectoNvo.save()

        """crea un sprint"""
        estado = 'ACT'
        proyecto = 'descripcion_completa'
        fecha_ini = '2018-01-01'
        fecha_fin = '2018-01-15'
        fecha_fin_est = '2018-01-16'
        sprint_nuevo = Sprint(estado=estado, proyecto=proyectoNvo, fecha_ini=fecha_ini, fecha_fin=fecha_fin,
                              fecha_fin_est=fecha_fin_est)

        # Fallo cuando el estado en nulo, para las pruebas de falla se vuelve a crear un nuevo proyecto
        """crea un proyecto un segundo proyecto"""
        """
        proyectoNvo2 = Proyecto(nombre="proyecto_pruebas_unitarias2", fecha_ini='2001-01-01', fecha_fin='2001-01-01', observaciones='obs')
        proyectoNvo2.save()

        sprint_nuevo = Sprint(estado=None, proyecto=proyectoNvo, fecha_ini=fecha_ini, fecha_fin=fecha_fin,
                               fecha_fin_est=fecha_fin_est)
        """

        sprint_nuevo.save()

        sprint_nuevo.delete()
        proyectoNvo.delete()

    except:
        #proyectoNvo.delete()
        #proyectoNvo2.delete()
        raise

    def test_create_sprint7(self):
        """
        Verifica que un sprint se crea
        """

    try:
        """crea un proyecto"""
        nombre = "proyecto_pruebas_unitarias_w"
        fecha_ini = '2001-01-01'
        fecha_fin = '2002-01-01'
        observaciones = 'obs'
        proyectoNvo = Proyecto(nombre=nombre, fecha_ini=fecha_ini, fecha_fin=fecha_fin, observaciones=observaciones)
        proyectoNvo.save()

        """crea un sprint"""
        estado = 'ACT'
        proyecto = 'descripcion_completa'
        fecha_ini = '2018-01-01'
        fecha_fin = '2018-01-15'
        fecha_fin_est = '2018-01-16'
        sprint_nuevo = Sprint(estado=estado, proyecto=proyectoNvo, fecha_ini=fecha_ini, fecha_fin=fecha_fin,
                              fecha_fin_est=fecha_fin_est)

        # Fallo cuando el estado en nulo, para las pruebas de falla se vuelve a crear un nuevo proyecto
        """crea un proyecto un segundo proyecto"""
        """
        proyectoNvo2 = Proyecto(nombre="proyecto_pruebas_unitarias2", fecha_ini='2001-01-01', fecha_fin='2001-01-01', observaciones='obs')
        proyectoNvo2.save()

        sprint_nuevo = Sprint(estado=None, proyecto=proyectoNvo, fecha_ini=fecha_ini, fecha_fin=fecha_fin,
                               fecha_fin_est=fecha_fin_est)
        """

        sprint_nuevo.save()

        sprint_nuevo.delete()
        proyectoNvo.delete()

    except:
        #proyectoNvo.delete()
        #proyectoNvo2.delete()
        raise

    def test_create_sprint8(self):
        """
        Verifica que un sprint se crea
        """

    try:
        """crea un proyecto"""
        nombre = "proyecto_pruebas_unitarias_w"
        fecha_ini = '2018-01-01'
        fecha_fin = '2019-01-01'
        observaciones = 'obs'
        proyectoNvo = Proyecto(nombre=nombre, fecha_ini=fecha_ini, fecha_fin=fecha_fin, observaciones=observaciones)
        proyectoNvo.save()

        """crea un sprint"""
        estado = 'ACT'
        proyecto = 'descripcion_completa'
        fecha_ini = '2018-01-15'
        fecha_fin = '2018-01-12'
        fecha_fin_est = '2018-01-10'
        sprint_nuevo = Sprint(estado=estado, proyecto=proyectoNvo, fecha_ini=fecha_ini, fecha_fin=fecha_fin,
                              fecha_fin_est=fecha_fin_est)

        # Fallo cuando el estado en nulo, para las pruebas de falla se vuelve a crear un nuevo proyecto
        """crea un proyecto un segundo proyecto"""
        """
        proyectoNvo2 = Proyecto(nombre="proyecto_pruebas_unitarias2", fecha_ini='2001-01-01', fecha_fin='2001-01-01', observaciones='obs')
        proyectoNvo2.save()

        sprint_nuevo = Sprint(estado=None, proyecto=proyectoNvo, fecha_ini=fecha_ini, fecha_fin=fecha_fin,
                               fecha_fin_est=fecha_fin_est)
        """

        sprint_nuevo.save()

        sprint_nuevo.delete()
        proyectoNvo.delete()

    except:
        #proyectoNvo.delete()
        #proyectoNvo2.delete()
        raise


    def test_create_sprint9(self):
        """
        Verifica que un sprint se crea
        """

    try:
        """crea un proyecto"""
        nombre = "proyecto_pruebas_unitarias_w"
        fecha_ini = '2018-01-01'
        fecha_fin = '2019-01-01'
        observaciones = 'obs'
        proyectoNvo = Proyecto(nombre=nombre, fecha_ini=fecha_ini, fecha_fin=fecha_fin, observaciones=observaciones)
        proyectoNvo.save()

        """crea un sprint"""
        estado = 'ACT'
        proyecto = 'descripcion_completa'
        fecha_ini = '2018-01-15'
        fecha_fin = '2018-01-12'
        fecha_fin_est = '2018-01-10'
        sprint_nuevo = Sprint(estado=estado, proyecto=proyectoNvo, fecha_ini=fecha_ini, fecha_fin=fecha_fin,
                              fecha_fin_est=fecha_fin_est)

        # Fallo cuando el estado en nulo, para las pruebas de falla se vuelve a crear un nuevo proyecto
        """crea un proyecto un segundo proyecto"""
        """
        proyectoNvo2 = Proyecto(nombre="proyecto_pruebas_unitarias2", fecha_ini='2001-01-01', fecha_fin='2001-01-01', observaciones='obs')
        proyectoNvo2.save()

        sprint_nuevo = Sprint(estado=None, proyecto=proyectoNvo, fecha_ini=fecha_ini, fecha_fin=fecha_fin,
                               fecha_fin_est=fecha_fin_est)
        """

        sprint_nuevo.save()

        sprint_nuevo.delete()
        proyectoNvo.delete()
    except:
        proyectoNvo.delete()
        #proyectoNvo2.delete()
        raise
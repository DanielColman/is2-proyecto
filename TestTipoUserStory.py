from unittest import TestCase
import django
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "GestorP.base")
django.setup()
from django.test import Client
from tablero.models import Tablero
from proyecto.models import Proyecto
from tablero.models import Fase


"""Prueba unitaria para la app Tablero y Fase"""

class Table(TestCase):
    def test_pagina(self):
        """
        Carga la página y verifica que encuentra
        """
        """crea un proyecto"""
        nombre = "test proyecto"
        fecha_ini = '2001-01-01'
        fecha_fin = '2002-01-01'
        observaciones = 'obs'
        proyectoNvo = Proyecto(nombre=nombre, fecha_ini=fecha_ini, fecha_fin=fecha_fin, observaciones=observaciones)
        proyectoNvo.save()

        client = Client()
        """prueba que se pueda ingresar en la página"""
        response = client.get('/tablero/crear/'+str(proyectoNvo.id))
        # HTTP_301_FOUND
        self.assertEqual(response.status_code, 301)

        proyectoNvo.delete()

    def test_create_tablero(self):
        """
        Verifica que un tablero se crea
        """
        try:
            """crea un proyecto"""
            nombre = "test proyecto"
            fecha_ini = '2001-01-01'
            fecha_fin = '2002-01-01'
            observaciones = 'obs'
            proyectoNvo = Proyecto(nombre=nombre, fecha_ini=fecha_ini, fecha_fin=fecha_fin, observaciones=observaciones)
            proyectoNvo.save()
            """crea un tablero y lo asocia al proyecto creado anteriormente"""
            nombre = "Tablero1"
            descripcion = 'descripcion de tablero1'

            tableroNvo = Tablero(nombre=nombre,descripcion=descripcion,proyecto= proyectoNvo)
            tableroNvo.save()

            #Intentar crear dos tableros para un mismo proyecto genera un ERROR
            #tableroNvo2 = Tablero(nombre=nombre,descripcion=descripcion,proyecto= proyectoNvo)
            #tableroNvo2.save()
            #tableroNvo2.delete()
            tableroNvo.delete()
            proyectoNvo.delete()
        except:
            tableroNvo.delete()
            proyectoNvo.delete()
            raise

    def test_create_fase(self):
        """
        Verifica que una fase se crea
        """
        try:
            nombre = "fase1"
            descripcion = 'descripcion de fase'
            orden=1

            faseNva = Fase(nombre=nombre, descripcion=descripcion, orden=orden)
            faseNva.save()

            #al campo int se le asigna una cadena para generar un fallo
            #faseNva2 =Fase(nombre=nombre,descripcion=descripcion,orden="dasd")
            #faseNva2.save()


            faseNva.delete()
            #faseNva2.delete()
        except:
            faseNva.delete()
            #faseNva2.delete()
            raise
    def test_create_fase2(self):
        """
        Verifica que una fase se crea
        """
        try:
            nombre = "fase2"
            descripcion = 'descripcion de fase2'
            orden="asd"

            faseNva2 = Fase(nombre=nombre, descripcion=descripcion, orden=orden)
            faseNva2.save()

            #faseNva.delete()
            faseNva2.delete()
        except:
            #faseNva.delete()
            faseNva2.delete()
            raise


    def test_modificar_tablero(self):
        """modifica los datos de un tablero"""

        try:
            """crea un tablero """
            nombre = "Test Tablero"
            #nombre=None
            descripcion = 'descripcion de tablero1'
            tableroNvo = Tablero(nombre=nombre, descripcion=descripcion)
            """guarda el tablero y le modifica el nombre"""
            tableroNvo.save()
            tableroNvo.nombre="Tab1"
            tableroNvo.save()

            tableroNvo.delete()
        except:
            tableroNvo.delete()
            raise

    def test_modificar_fase2(self):
        """
        Modifica los datos de una fase
        """
        try:
            """crea una fase"""
            nombre = "fase1"
            descripcion = 'descripcion de fase'
            orden = 1
            faseNva = Fase(nombre=nombre, descripcion=descripcion, orden=orden)
            faseNva.save()

            """modifica el nombre de la fase"""
            faseNva.nombre="fase2"
            #faseNva.orden = 'cadena'
            faseNva.save()

            faseNva.delete()
        except:
            faseNva.delete()
            raise


    def test_create_tablero2(self):
        """
        Verifica que un tablero se crea
        """
        try:
            """crea un proyecto"""
            nombre = "test proyecto"
            fecha_ini = '2001-01-01'
            fecha_fin = '2002-01-01'
            observaciones = 'obs'
            proyectoNvo = Proyecto(nombre=nombre, fecha_ini=fecha_ini, fecha_fin=fecha_fin, observaciones=observaciones)
            proyectoNvo.save()
            """crea un tablero y lo asocia al proyecto creado anteriormente"""
            nombre = "Tablero1"
            descripcion = 'descripcion de tablero1'

            tableroNvo = Tablero(nombre=nombre,descripcion=descripcion,proyecto= proyectoNvo)
            tableroNvo.save()

            #Intentar crear dos tableros para un mismo proyecto genera un ERROR
            tableroNvo2 = Tablero(nombre=nombre,descripcion=descripcion,proyecto= proyectoNvo)
            tableroNvo2.save()
            tableroNvo2.delete()
            tableroNvo.delete()
            proyectoNvo.delete()
        except:
            tableroNvo.delete()
            proyectoNvo.delete()
            raise
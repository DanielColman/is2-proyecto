from django.conf.urls import include, url
from . import views

"""
Las url para proyecto
"""


urlpatterns = [
    url(r'^$', views.listar_proyectos),
    url(r'^create/$', views.crear_proyecto, name='create_proyecto'),
    url(r'^update/(?P<id_proyecto>[0-9]+)/$', views.modificar_proyecto, name='update_proyecto'),
    url(r'^update/(?P<pk>[0-9]+)/confirmado.html$', views.confirmado, name='confirmado'),
    url(r'^update/(?P<pk>[0-9]+)/eliminado.html$', views.eliminado, name='eliminado'),
    url(r'^eliminado.html$', views.eliminado2, name='eliminado'),
    url(r'^buscar/$', views.buscar_proyecto, name='buscar_proyecto'),
]
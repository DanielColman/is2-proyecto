from django.contrib import admin
from team.models import TeamMember, Team


# Register your models here.
admin.site.register(TeamMember)
admin.site.register(Team)


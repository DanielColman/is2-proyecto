import os

def populate():
    #CREAR PERMISOS
    add_permiso(1,"Consultar Usuarios del Proyecto")
    add_permiso(2,"Crear Rol")
    add_permiso(3,"Modificar Rol")
    add_permiso(4,"Buscar Rol")
    add_permiso(5,"Consultar Rol")
    add_permiso(6,"Modificar Proyecto")
    add_permiso(7,"Inactivar Proyecto")
    add_permiso(8,"Consultar Proyecto")

    #CREAR ROLES
    add_rol('scrum','Es el Scrum Master del proyecto', [1,2,3,4,5,6,7,8])
    add_rol('developer', 'Es un desarrollador del proyecto', [1,5,8])

    #CREAR USUARIOS
    add_usuario('daniel', 'danielcolman@outlook.com', 'proyecto', 'Daniel', 'Colman', '12345678', '021751438', 'Developer',
                'Mariano Roque Alonso', 8)
    add_usuario('miriam', 'miriam@gmail.com', 'proyecto', 'Miriam', 'Godoy', '4841518', '021751439', 'Developer',
                'Asuncion', 7)
    add_usuario('blas', 'blasmiranda@gmail.com', 'proyecto', 'Blas', 'Miranda', '4841519', '021751440', 'Developer',
                'Fernando de la Mora', 9)
    add_usuario('lucia', 'lucia@gmail.com', 'proyecto', 'Lucia', 'Salinas', '4841617', '021752438', 'Developer',
                'San Lorenzo', 5)
    add_usuario('noemi', 'noemi@gmail.com', 'proyecto', 'Noemi', 'Gauto', '690745', '0971180176', 'Developer',
                'Benjamin Aceval', 10)
    add_usuario('jazmin', 'jazmin@gmail.com', 'proyecto', 'Jazmin', 'Insfran', '4288835', '021640692', 'Developer',
                'America 163', 15)
    add_usuario('joel', 'joel@gmail.com', 'proyecto', 'Joel', 'Acosta', '4258632', '02168953', 'Developer',
                'Tuyutti', 20)
    add_usuario('lucas', 'lucas@gmail.com', 'proyecto', 'Lucas', 'Gamarra', '848851', '0971790393', 'Developer',
                'las mercedes', 20)
    add_usuario('emilio', 'emilio@gmail.com', 'proyecto', 'Emilio', 'Paiva', '4288832', '0978962104', 'Developer',
                'los laureles', 8)
    add_usuario('arami', 'arami@gmail.com', 'proyecto', 'Arami', 'Leguizamon', '759883', '02185632', 'Developer',
                'carmelitas', 30)

    #CREAR PROYECTOS
    #PROYECTO A INICIAR
    add_proyecto('a iniciar', None, None, None, None, None, 'PEN', [('daniel','scrum')])
    add_us('s1', 'User Story 1', 50, 8, 7, 9, 8, '2019-05-23 15:40:39', None, None, 0, None, 'a iniciar', 1,
           0)
    add_us('s2', 'User Story 2', 30, 3, 4, 5, 2, '2019-05-23 15:40:39', None, None, 0, None, 'a iniciar', 1,
           0)
    add_us('s3', 'User Story 3', 60, 3, 4, 5, 2, '2019-05-23 15:40:39', None, None, 0, None, 'a iniciar', 1,
           0)

    #PROYECTO CONFIGURADO Y EJECUTADO
    add_proyecto('configurado', '2018-05-23', '2018-05-23', None, '2018-08-23', 'Proyecto configurado y ejecutado', 'ACT', [('jazmin', 'scrum'),('augusto','developer'),('fatima','developer'),('maricris','developer')])
    add_tablero('TUS1','Tipo de User Story 1','configurado',[('Fase1','Fase 1',1),('Fase2','Fase 2',2)])
    add_tablero('TUS2', 'Tipo de User Story 2', 'configurado',[('Fase1','Fase 1',1),('Fase2','Fase 2',2),('Fase3','Fase 3',3)])
    add_sprint('ACT', 'configurado', '2019-05-23',None, '2018-05-30', 'US1;User Story 1;50;8;7;9;8:US5;User Story 5;45;9;8;7;7:US2;User Story 2;60;4;8;5;5:US6;User Story 6;30;5;9;4;5:US3;User Story 3;40;3;4;5;4:US4;User Story 4;80;2;7;4;4:',[('jazmin',8),('maricris',7),('fatima',6),('augusto',5)])
    #US1
    add_us('US1', 'User Story 1', 50, 8, 7, 9, 8, '2018-05-23 15:40:39', 'TUS1', 'Fase1', 1, 'fatima', 'configurado', 1,0)
    add_daily('US1','Daily US1 1', 4, True)
    add_daily('US1', 'Daily US1 2', 5, True)
    add_daily('US1', 'Daily US1 3', 2, True)
    #US2
    add_us('US2', 'User Story 2', 60, 4, 8, 5, 5, '2018-05-23 15:40:57', 'TUS1', 'Fase1', 1, 'maricris', 'configurado', 1, 0)
    add_daily('US2', 'Daily US2 1', 4, True)
    add_daily('US2', 'Daily US2 2', 5, True)
    #US3
    add_us('US3', 'User Story 3', 40, 3, 4, 5, 4, '2018-05-23 15:41:11', 'TUS2', 'Fase1', 1, 'fatima', 'configurado', 1, 0)
    add_daily('US3', 'Daily US3 1', 2, True)
    add_daily('US3', 'Daily US3 2', 3, True)
    add_daily('US3', 'Daily US3 3', 2, True)
    #US4
    add_us('US4', 'User Story 4', 80, 2, 7, 4, 4, '2018-05-23 15:41:24', 'TUS2', 'Fase1', 1, 'augusto', 'configurado', 1, 0)
    add_daily('US4', 'Daily US4 1', 5, True)
    #US5
    add_us('US5', 'User Story 5', 45, 9, 8, 7, 7, '2018-05-23 15:41:40', 'TUS1', 'Fase1', 1, 'augusto', 'configurado', 1, 0)
    add_daily('US5', 'Daily US5 1', 2, True)
    add_daily('US5', 'Daily US5 2', 3, True)
    add_daily('US5', 'Daily US5 3', 6, True)
    add_daily('US5', 'Daily US5 4', 2, True)
    add_daily('US5', 'Daily US5 5', 1, True)
    #US6
    add_us('US6', 'User Story 6', 30, 5, 9, 4, 5, '2018-05-23 15:41:51', 'TUS2', 'Fase1', 1, 'jazmin', 'configurado', 1, 0)
    add_daily('US6', 'Daily US6 1', 2, True)
    add_daily('US6', 'Daily US6 2', 3, True)
    add_daily('US6', 'Daily US6 3', 4, True)
    add_daily('US6', 'Daily US6 4', 1, True)
    add_daily('US6', 'Daily US6 5', 2, True)
    add_daily('US6', 'Daily US6 6', 3, True)
    #US7
    add_us('US7', 'User Story 7', 50, 2, 5, 1, 2, '2018-05-23 15:48:00', None, None, 0, None, 'configurado', None, 0)

    #PROYECTO A FINALIZAR
    add_proyecto('a finalizar', '2018-05-24', '2018-05-24', None, '2018-08-26', 'Proyecto a finalizar',
                 'ACT',
                 [('fatima', 'scrum'), ('augusto', 'developer'), ('jazmin', 'developer'), ('maricris', 'developer')])
    add_tablero('Tipo 1', 'Tipo de User Story 1', 'a finalizar', [('f1', 'Fase 1', 1), ('f2', 'Fase 2', 2)])
    add_tablero('Tipo 2', 'Tipo de User Story 2', 'a finalizar',
                [('f1', 'Fase 1', 1), ('f2', 'Fase 2', 2), ('f3', 'Fase 3', 3)])
    add_sprint('TER', 'a finalizar', '2018-05-24', None, '2018-05-24',
               'u1;User Story 1;30;9;8;7;7:u2;User Story 2;40;9;7;5;6:u3;User Story 3;45;9;7;4;6:u4;User Story 4;20;8;6;3;5:u5;User Story 5;30;9;6;3;5:u6;User Story 6;20;1;9;4;4:',
               [('jazmin', 3), ('maricris', 2), ('fatima', 3), ('augusto', 4)])
    #u1
    add_us('u1', 'User Story 1', 30, 9, 8, 7, 7, '2018-05-24 11:13:58', 'Tipo 1', 'f1', 1, 'fatima', 'a finalizar', 1,
           1)
    add_daily('u1', 'Daily u1 1', 2, True)
    add_daily('u1', 'Daily u1 2', 4, True)
    add_daily('u1', 'Daily u1 3', 3, True)
    add_daily('u1', 'Daily u1 4', 2, True)
    add_daily('u1', 'Daily u1 5', 1, True)
    #u2
    add_us('u2', 'User Story 2', 40, 9, 7, 5, 7, '2018-05-24 11:13:58', 'Tipo 1', 'f1', 1, 'jazmin', 'a finalizar', 1,
           1)
    add_daily('u2', 'Daily u2 1', 3, True)
    add_daily('u2', 'Daily u2 2', 4, True)
    add_daily('u2', 'Daily u2 3', 5, True)
    add_daily('u2', 'Daily u2 4', 1, True)
    add_daily('u2', 'Daily u2 5', 2, True)
    #u3
    add_us('u3', 'User Story 3', 45, 9, 7, 4, 6, '2018-05-24 11:13:58', 'Tipo 1', 'f1', 1, 'augusto', 'a finalizar', 1,
           1)
    add_daily('u3', 'Daily u3 1', 3, True)
    add_daily('u3', 'Daily u3 2', 2, True)
    add_daily('u3', 'Daily u3 3', 5, True)
    add_daily('u3', 'Daily u3 4', 4, True)
    add_daily('u3', 'Daily u3 5', 1, True)
    #u4
    add_us('u4', 'User Story 4', 20, 8, 6, 3, 5, '2018-05-24 11:13:58', 'Tipo 2', 'f1', 1, 'maricris', 'a finalizar', 1,
           1)
    add_daily('u4', 'Daily u4 1', 3, True)
    add_daily('u4', 'Daily u4 2', 2, True)
    add_daily('u4', 'Daily u4 3', 6, True)
    add_daily('u4', 'Daily u4 4', 5, True)
    add_daily('u4', 'Daily u4 5', 2, True)
    add_daily('u4', 'Daily u4 6', 6, True)
    add_daily('u4', 'Daily u4 7', 1, True)
    add_daily('u4', 'Daily u4 8', 3, True)
    #u5
    add_us('u5', 'User Story 5', 30, 9, 6, 3, 5, '2018-05-24 11:13:58', 'Tipo 2', 'f1', 1, 'fatima', 'a finalizar', 1,
           1)
    add_daily('u5', 'Daily u5 1', 3, True)
    add_daily('u5', 'Daily u5 2', 2, True)
    add_daily('u5', 'Daily u5 3', 6, True)
    add_daily('u5', 'Daily u5 4', 5, True)
    add_daily('u5', 'Daily u5 5', 1, True)
    add_daily('u5', 'Daily u5 6', 7, True)
    add_daily('u5', 'Daily u5 7', 3, True)
    add_daily('u5', 'Daily u5 8', 1, True)

    #u6
    add_us('u6', 'User Story 6', 20, 1, 9, 4, 4, '2018-05-24 11:13:58', 'Tipo 2', 'f1', 1, 'jazmin', 'a finalizar', 1,
           1)
    add_daily('u6', 'Daily u6 1', 2, True)
    add_daily('u6', 'Daily u6 2', 6, True)
    add_daily('u6', 'Daily u6 3', 8, True)
    add_daily('u6', 'Daily u6 4', 2, True)
    add_daily('u6', 'Daily u6 5', 8, True)
    add_daily('u6', 'Daily u6 6', 4, True)
    add_daily('u6', 'Daily u6 7', 5, True)
    add_daily('u6', 'Daily u6 8', 6, True)
    #Reunion Retrospectiva
    add_reunion('a finalizar','Reunion Retrospectiva del Sprint')


print ("Terminado.")

def add_permiso(orden, nombre):
    p = Permiso(orden=orden,nombre=nombre)
    p.save()
    return p

def add_rol(nombre, descripcion, orden_permisos):
    r = Rol(nombre=nombre, descripcion=descripcion)
    r.save()

    for i in orden_permisos:
        p = Permiso.objects.get(orden=i)
        r.permiso.add(p)
        r.save()
    return r

def add_usuario(username, correo, contrasena, nombre, apellido, cedula, telefono, descripcion, direccion, horas_laborales):
    new_user = User.objects.create_user(username, correo, contrasena)
    new_user.first_name = nombre
    new_user.last_name = apellido
    new_user.usuario.ci = cedula
    new_user.usuario.telefono = telefono
    new_user.usuario.descripcion = descripcion
    new_user.usuario.direccion = direccion
    new_user.usuario.horas_laborales = horas_laborales
    new_user.save()

def add_proyecto(nombre, fecha_ini, fecha_ini_est, fecha_fin, fecha_fin_est, observaciones, estado, teammebers):
    proyectoNvo = Proyecto(nombre=nombre, fecha_ini=fecha_ini, fecha_fin_est=fecha_fin_est, fecha_fin=fecha_fin, fecha_ini_est=fecha_ini_est,observaciones=observaciones, estado=estado)
    proyectoNvo.save()
    team = Team.objects.create(id_proyecto=proyectoNvo)
    tm = []
    for teammeber in teammebers:
        rol = Rol.objects.get(nombre=teammeber[1])
        user = User.objects.get(username=teammeber[0])
        usuario = user.usuario
        tm.append(TeamMember.objects.create(id_usuario=usuario,id_rol=rol))
    for teammeber in tm:
        team.id_team_member.add(teammeber)
    team.save()

def add_tablero(nombre,descripcion,proyecto_n,fases):
    proyecto = Proyecto.objects.get(nombre=proyecto_n)
    tableroNvo = Tablero(nombre=nombre, descripcion=descripcion, proyecto=proyecto)
    tableroNvo.save()
    for fase in fases:
        faseNva = Fase.objects.create(nombre=fase[0],descripcion=fase[1],orden=fase[2],tablero=tableroNvo)
        faseNva.save()
    tableroNvo.save()

def add_sprint(estado, proyecto_n, fecha_ini, fecha_fin, fecha_fin_est, para_importar, teammembers):
    estado = estado
    proyecto = Proyecto.objects.get(nombre=proyecto_n)
    sprintNvo = Sprint(estado=estado, proyecto=proyecto, fecha_ini=fecha_ini, fecha_fin=fecha_fin, fecha_fin_est=fecha_fin_est, para_importar=para_importar)
    sprintNvo.save()
    for tm in teammembers:
        teammember = TeamMember.objects.get(id_usuario__user__username=tm[0], team__id_proyecto_id=proyecto)
        hs = Horas.objects.create(sprint=sprintNvo, teammember=teammember, horas_trabajadas=tm[1])
        hs.save()

def add_us(descripcion_breve, descripcion_completa, tiempo_finalizacion, valor_negocio, prioridad, valor_tecnico, prioridad_promedio, fecha_creacion, tablero_n, fase_n, estado, usuario_n, proyecto_n, sprint, aprobado):
    descripcion_breve=descripcion_breve
    descripcion_completa=descripcion_completa
    tiempo_finalizacion=tiempo_finalizacion
    valor_tecnico=valor_tecnico
    valor_negocio=valor_negocio
    prioridad=prioridad
    prioridad_promedio=prioridad_promedio
    fecha_creacion=fecha_creacion
    proyecto = Proyecto.objects.get(nombre=proyecto_n)
    try:
        tablero=Tablero.objects.get(nombre=tablero_n)
        fase=Fase.objects.get(nombre=fase_n, tablero=tablero)
        user = User.objects.get(username=usuario_n)
        #usuario=Usuario.objects.get(user=user)
        sprint = Sprint.objects.get(proyecto=proyecto)
    except:
        tablero = None
        fase = None
        user = None
        sprint = None
    usNvo = Us(descripcion_breve=descripcion_breve, descripcion_completa=descripcion_completa, tiempo_finalizacion=tiempo_finalizacion, valor_negocio=valor_negocio, prioridad=prioridad, valor_tecnico=valor_tecnico, prioridad_promedio=prioridad_promedio, fecha_creacion=fecha_creacion, tablero=tablero, fase=fase, estado=estado, usuario=user, proyecto=proyecto, sprint=sprint, aprobado=aprobado)
    usNvo.save()

def add_daily(us, daily, tiempo, avanza):
    us = Us.objects.get(descripcion_breve=us)
    fase = us.fase
    estado = us.estado
    user = User.objects.get(pk = us.usuario.pk)
    dailyNvo = Daily(us=us,daily=daily,tiempo=tiempo,avanza=avanza,estado=estado,fase=fase,usuario=user)
    dailyNvo.save()
    fases = Fase.objects.filter(tablero=us.tablero)
    fases = list(fases)
    cant_fases = len(fases)
    if avanza:
        if us.estado < 3:
            """ Cambio de estado"""
            us.estado = us.estado + 1
        elif us.estado == 3:
            if us.fase.orden != cant_fases:
                nva_fase = Fase.objects.get(orden=us.fase.orden + 1, tablero=us.tablero)
                us.fase = nva_fase
                """ Cambio de estado"""
                us.estado = 1
    else:
        fase = Fase.objects.get(pk=us.fase.pk, tablero=us.tablero)
        if fase.orden != 1:
            """ Si la fase no es del primer orden """
            if us.estado == 1:
                n_fase = Fase.objects.get(orden=fase.orden - 1, tablero=us.tablero)
                us.fase = n_fase
                """ Cambio de estado"""
                us.estado = 3
            elif us.estado == 3:
                """ Cambio de estado"""
                us.estado = 2
            elif us.estado == 2:
                """ Cambio de estado"""
                us.estado = 1
        if fase.orden == 1:
            if us.estado == 3:
                """ Cambio de estado"""
                us.estado = 2
            else:
                """ Cambio de estado"""
                us.estado = 1
    us.save()

def add_reunion(proyecto_n, reunion):
    proyecto = Proyecto.objects.get(nombre=proyecto_n)
    sprint = Sprint.objects.get(proyecto = proyecto)
    reunion = Reunion(sprint=sprint,reunion=reunion)
    reunion.save()

if __name__ == '__main__':
    print ("Inicio de la poblacion de la base de datos")
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'GestorP.settings.dev')
    import django

    django.setup()
    from proyecto.models import Proyecto
    from Rol.models import Rol, Permiso
    from sprint.models import Sprint, Horas, Reunion
    from tablero.models import Tablero, Fase
    from us.models import Us, Daily
    from usuario.models import Usuario
    from team.models import Team, TeamMember
    from django.contrib.auth.models import User
    from django.utils import timezone
    from datetime import timedelta

    populate()


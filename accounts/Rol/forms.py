from django import forms
from Rol.models import *
from django import forms
from django.forms import CharField, Form

class BuscarRol(Form):
    """
    Formulario para buscar un Rol por nombre
    """
    Nombre = CharField()

class RolForm(forms.ModelForm):
    """
    Formulario para crear un Rol
    """
    class Meta:
        model = Rol
        fields = [
            'nombre',
            'descripcion',
            'permiso'
        ]
        labels = {
            'nombre': 'Nombre ',
            'descripcion': 'Descripcion ',
            'permiso': 'Permiso ',

        }
        widgets = {
        'permiso': forms.CheckboxSelectMultiple(),
        }


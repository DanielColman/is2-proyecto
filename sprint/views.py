from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.db.models import Q
from django.http import HttpResponseForbidden
from django.shortcuts import render, redirect
from django.contrib import messages
from datetime import date, datetime

# Create your views here.
import proyecto
import sprint
from proyecto.models import Proyecto
from sprint.forms import IniciarSprintForm, ReunionForm
from sprint.models import Sprint, Reunion, Horas
from tablero.models import Tablero, Fase
from team.models import TeamMember, Team
from us.models import Us
from usuario.models import Usuario

@login_required
@transaction.atomic
def iniciar(request, proyecto_id):
    """
    Vista para iniciar un Sprint. Se selecciona la fecha de finalización estimada,
    los teammembers del proyecto que van a formar parte del Team de Sprint, con
    la cantidad de horas que cada teammember va a dedicar a el Sprint.
    :param request:
    :param proyecto_id: id del proyecto dentro del cual se iniciara el sprint
    """
    usuarios = TeamMember.objects.filter(team__id_proyecto=proyecto_id)
    usuarios = list(usuarios)
    proyecto = Proyecto.objects.get(id = proyecto_id)

    teammember = TeamMember.objects.filter(id_usuario=request.user.usuario.id)
    teammember = list(teammember)
    permitido = False
    is_scrum = False
    us_proyecto = []
    rol_usuario = []
    try:
        for i in range(len(teammember)):
            try:
                team = Team.objects.get(id_team_member=teammember[i].id)
            except:
                continue
            if team.id_proyecto_id == proyecto.id:
                if teammember[i].id_rol.nombre == 'scrum':
                    is_scrum = True
                    permitido = True
    except:
        return HttpResponseForbidden()
    if permitido:
        if request.method == 'POST' and 'iniciar' in request.POST:
            form = IniciarSprintForm(request.POST)
            teammem_seleccionados = request.POST.getlist('checks[]')
            if len(teammem_seleccionados) == 0:
                messages.error(request,
                               'Seleccione al menos un Team Member para el Sprint.')
                return redirect('/sprint/iniciar/' + str(proyecto_id))
            if form.is_valid():
                obj = form.save(commit=False)
                if obj.fecha_fin_est == None:
                    messages.error(request, 'Campo Fecha de Finalización Estimada es obligatorio.')
                    return redirect('/sprint/iniciar/' + str(proyecto_id))
                if obj.fecha_fin_est < date.today():
                    messages.error(request, 'La fecha de finaización estimada debe ser mayor o igual a la fecha de inicio (hoy)')
                    return redirect('/sprint/iniciar/' + str(proyecto_id))
                obj.proyecto = Proyecto.objects.get(id=proyecto_id)
                obj.fecha_ini = date.today()
                obj.save()
                i=0
                for tm in teammem_seleccionados:
                    aux='cantidad'+tm
                    hs_trab = request.POST[aux]

                    hs = Horas.objects.create(sprint=obj, teammember=TeamMember.objects.get(id=tm),horas_trabajadas=hs_trab)
                    hs.save()
                    i=i+1
                messages.success(request, ('Se inició correctamente el Sprint.'))
                return redirect('/sprint/iniciar2/'+str(proyecto_id))
        elif request.method == 'POST' and 'importar' in request.POST:
            form = IniciarSprintForm(request.POST)
            teammem_seleccionados = request.POST.getlist('checks[]')
            if len(teammem_seleccionados) == 0:
                messages.error(request,
                               'Seleccione al menos un Team Member para el Sprint.')
                return redirect('/sprint/iniciar/' + str(proyecto_id))
            if form.is_valid():
                obj = form.save(commit=False)
                if obj.fecha_fin_est == None:
                    messages.error(request, 'Campo Fecha de Finalización Estimada es obligatorio.')
                    return redirect('/sprint/iniciar/' + str(proyecto_id))
                if obj.fecha_fin_est < date.today():
                    messages.error(request,
                                   'La fecha de finaización estimada debe ser mayor o igual a la fecha de inicio (hoy)')
                    return redirect('/sprint/iniciar/' + str(proyecto_id))
                obj.proyecto = Proyecto.objects.get(id=proyecto_id)
                obj.fecha_ini = date.today()

                obj.save()
                i = 0
                for tm in teammem_seleccionados:
                    aux = 'cantidad' + tm
                    hs_trab = request.POST[aux]

                    hs = Horas.objects.create(sprint=obj, teammember=TeamMember.objects.get(id=tm),
                                              horas_trabajadas=hs_trab)
                    hs.save()
                    i = i + 1
                messages.success(request, ('Se inició correctamente el Sprint.'))
                return redirect('/sprint/elegir_para_importar/' + str(obj.id))
        else:
            form = IniciarSprintForm()
        return render(request,'sprint/iniciar.html',{'form':form ,'usuarios':usuarios, 'proyecto':proyecto})
    else:
        return HttpResponseForbidden()

def iniciar2(request, proyecto_id):
    """
        Vista para la segunda pagina de inicio de Sprint
        :param request:
        :param proyecto_id: id del proyecto dentro del cual se iniciara el spri
    """
    proyecto = Proyecto.objects.get(id=proyecto_id)

    teammember = TeamMember.objects.filter(id_usuario=request.user.usuario.id)
    teammember = list(teammember)
    permitido = False
    is_scrum = False
    us_proyecto = []
    rol_usuario = []
    try:
        for i in range(len(teammember)):
            try:
                team = Team.objects.get(id_team_member=teammember[i].id)
            except:
                continue
            if team.id_proyecto_id == proyecto.id:
                if teammember[i].id_rol.nombre == 'scrum':
                    is_scrum = True
                    permitido = True
    except:
        return HttpResponseForbidden()
    if permitido:

        sprint = Sprint.objects.get(proyecto=proyecto,estado='ACT')
        uss = Us.objects.filter(proyecto=proyecto, sprint=sprint)
        uss = list(uss)
        long = len(uss)
        return render(request,'sprint/iniciar_2.html',{'proyecto':proyecto, 'long':long})
    else:
        return HttpResponseForbidden()

@login_required
@transaction.atomic
def horas_disponibles(request, teammember_id, proyecto_id):
    """
    Vista en la que se calculan las horas disponibles para un usuario del sprint
    :param request:
    :param teammember_id: id del team member del cual se quiere verificar las horas
    :param proyecto_id: id del proyecto dentro del cual se iniciara el sprint
    """
    teammember = TeamMember.objects.get(id = teammember_id)
    proyecto = Proyecto.objects.get(id = proyecto_id)

    usuario = teammember.id_usuario
    horas_ocupadas = Horas.objects.filter(sprint__estado='ACT',teammember__id_usuario=usuario)
    horas_ocupadas = list(horas_ocupadas)
    horas_laborales = usuario.horas_laborales
    disponibles = horas_laborales
    for hora in horas_ocupadas:
        disponibles = disponibles - hora.horas_trabajadas
    mensaje = ''
    if (disponibles > 0):
        mensaje = 'Tiene ' + str(disponibles) + ' horas disponibles.'
    elif (disponibles < 0):
        mensaje = 'El usuario ya trabaja ' + str(disponibles * -1) + ' más de las que tiene disponible'
    else:
        mensaje = 'El usuario ya trabaja exactamente todas sus horas laborales diarias.'
    return render(request,'sprint/horas_disponibles.html',{'mensaje':mensaje, 'proyecto':proyecto})

@login_required
@transaction.atomic
def addReunion(request, sprint_id):
    """
    Vista en la cual se añade una Reunion retrospectiva a un sprint
    :param request:
    :param sprint_id: id del sprint al cual se añadira la reunion
    """
    sprint = Sprint.objects.get(id=sprint_id)
    proyecto = Proyecto.objects.get(sprint=sprint)

    teammember = TeamMember.objects.filter(id_usuario=request.user.usuario.id)
    teammember = list(teammember)
    permitido = False
    is_scrum = False
    us_proyecto = []
    rol_usuario = []
    try:
        for i in range(len(teammember)):
            try:
                team = Team.objects.get(id_team_member=teammember[i].id)
            except:
                continue
            if team.id_proyecto_id == proyecto.id:
                if teammember[i].id_rol.nombre == 'scrum':
                    is_scrum = True
                    permitido = True
    except:
        return HttpResponseForbidden()
    if permitido:

        if request.method == "POST":
            form = ReunionForm()
            reunion = request.POST['reunion']
            if reunion is not "":
                obj = form.save(commit=False)
                obj.reunion = reunion
                obj.sprint = sprint
                obj.save()
                messages.success(request, ('Se adjuntó correctamente la reunión retrospectiva'))
                return redirect('/sprint/consultar/'+str(sprint_id))
        else:
            form = ReunionForm()
        return render(request,'sprint/reunion.html',{'form':form})
    else:
        return HttpResponseForbidden()

@login_required
@transaction.atomic
def verReuniones(request,sprint_id):
    """
    Vista en la que se visualizan las reuniones de un sprint
    :param request:
    :param sprint_id: id del sprint del cual se quieren ver las reuniones
    """
    sprint = Sprint.objects.get(id=sprint_id)
    reuniones = Reunion.objects.filter(sprint=sprint)
    return render(request,'sprint/ver_reuniones.html',{'reuniones':reuniones})

@login_required
@transaction.atomic
def finalizar(request, sprint_id):
    """
    Vista para finalizar un sprint
    :param request:
    :param sprint_id: id del sprint a finalizar
    """
    sprint = Sprint.objects.get(id=sprint_id)
    proyecto = sprint.proyecto

    teammember = TeamMember.objects.filter(id_usuario=request.user.usuario.id)
    teammember = list(teammember)
    permitido = False
    is_scrum = False
    us_proyecto = []
    rol_usuario = []
    try:
        for i in range(len(teammember)):
            try:
                team = Team.objects.get(id_team_member=teammember[i].id)
            except:
                continue
            if team.id_proyecto_id == proyecto.id:
                if teammember[i].id_rol.nombre == 'scrum':
                    is_scrum = True
                    permitido = True
    except:
        return HttpResponseForbidden()
    if permitido:
        if request.method == "POST":
            form = ReunionForm()
            reunion = request.POST['reunion']
            if reunion is not "":
                obj = form.save(commit=False)
                obj.reunion = reunion
                obj.sprint = sprint
                obj.save()
                messages.success(request, ('Se adjuntó correctamente la reunión retrospectiva.'))
            else:
                obj = form.save(commit=False)
                obj.reunion = "No se cargó la reunión retrospectiva"
                obj.sprint = sprint
                obj.save()
                messages.success(request, ('Sprint finalizado con exito!'))
            sprint.estado = 'TER'
            sprint.fecha_fin = date.today()
            sprint.save()
            return redirect('/proyecto/trabajo/'+str(proyecto.id))
        return render(request,'sprint/finalizar.html',{'sprint':sprint, 'proyecto':proyecto})
    else:
        return HttpResponseForbidden()

@login_required
@transaction.atomic
def control_calidad(request, sprint_id):
    """
    Vista para realizar el control de calidad de un sprint
    :param request:
    :param sprint_id: id del sprint del cual se realizara el control de calidad
    """
    sprint = Sprint.objects.get(id=sprint_id)
    proyecto = sprint.proyecto

    teammember = TeamMember.objects.filter(id_usuario=request.user.usuario.id)
    teammember = list(teammember)
    permitido = False
    is_scrum = False
    us_proyecto = []
    rol_usuario = []
    try:
        for i in range(len(teammember)):
            try:
                team = Team.objects.get(id_team_member=teammember[i].id)
            except:
                continue
            if team.id_proyecto_id == proyecto.id:
                if teammember[i].id_rol.nombre == 'scrum':
                    is_scrum = True
                    permitido = True
    except:
        return HttpResponseForbidden()
    if permitido:

        tableros = Tablero.objects.filter(proyecto=proyecto)
        tableros = list(tableros)
        uss = []
        if len(tableros) > 0:
            for tablero in tableros:
                fases = Fase.objects.filter(tablero=tablero)
                fases = list(fases)
                ultima = len(fases)

                #los que no llegaron al control de calidad
                for ordenn in range(1,ultima):
                    fasee = Fase.objects.get(tablero=tablero, orden=ordenn)
                    for est in range(1,4):
                        u_story = Us.objects.filter(tablero=tablero, fase=fasee, estado=est, sprint=sprint)
                        u_story=list(u_story)
                        for u_storyy in u_story:
                            u_storyy.aprobado=3
                            u_storyy.save()


                fase = Fase.objects.get(tablero=tablero,orden=ultima)
                for est in range(1,3):
                    u_story=Us.objects.filter(tablero=tablero, fase=fase, estado=est, sprint=sprint)
                    u_story=list(u_story)
                    for u_storyy in u_story:
                        u_storyy.aprobado = 3
                        u_storyy.save()


                #los que llegaron a control de calidad (ultimo estado de la ultima fase)
                us = Us.objects.filter(tablero = tablero,fase=fase, estado=3, sprint=sprint)
                us = list(us)
                for u in us:
                    uss.append(u)
        if request.method == "POST":
            i = 0
            for u in uss:
                aprueba = request.POST.getlist('checks' + str(u.id) + '[]')
                a = aprueba[0]
                if a == 'si':
                    u.aprobado = 1
                elif a == 'no':
                    u.aprobado = 2
                u.save()
                i = i + 1
            messages.success(request, 'Se ha realizado correctamente el control de calidad.')
            return redirect('/sprint/control_calidad_mover/'+str(sprint_id))
        return render(request, 'sprint/control_calidad.html', {'uss':uss, 'proyecto':proyecto,'sprint':sprint})
    else:
        return HttpResponseForbidden()

@login_required
@transaction.atomic
def control_calidad_mover(request, sprint_id):
    """
    Vista que realiza las acciones necesarias al reprobar o aprobar un sprint
    :param request:
    :param sprint_id: id del sprint al cual se realizara el control de calidad
    """
    sprint = Sprint.objects.get(id=sprint_id)
    proyecto = sprint.proyecto

    teammember = TeamMember.objects.filter(id_usuario=request.user.usuario.id)
    teammember = list(teammember)
    permitido = False
    is_scrum = False
    us_proyecto = []
    rol_usuario = []
    try:
        for i in range(len(teammember)):
            try:
                team = Team.objects.get(id_team_member=teammember[i].id)
            except:
                continue
            if team.id_proyecto_id == proyecto.id:
                if teammember[i].id_rol.nombre == 'scrum':
                    is_scrum = True
                    permitido = True
    except:
        return HttpResponseForbidden()
    if permitido:
        uss = Us.objects.filter(sprint=sprint,aprobado=2)
        uss = list(uss)
        long = len(uss)
        if request.method == 'POST':
            return redirect('/sprint/finalizar/'+str(sprint_id))
        return render(request, 'sprint/control_calidad_mover.html', {'uss': uss, 'proyecto': proyecto, 'long':long})
    else:
        return HttpResponseForbidden()

@login_required
@transaction.atomic
def importar_sprint(request, sprint_origen, sprint_destino):
    """
    Importa sprint utilizando el campo para_importar del modelo Sprint, que sirve para
    guardar un historial de los US asociados al sprint en el momento de su creación.
    :param request
    :param sprint_origen: el sprint del cual se importa
    :param sprint_destino: el sprint al que se importa
    """
    sprint = Sprint.objects.get(id=sprint_origen)
    sprints = sprint.para_importar
    destino = Sprint.objects.get(id=sprint_destino)
    proyecto = destino.proyecto

    teammember = TeamMember.objects.filter(id_usuario=request.user.usuario.id)
    teammember = list(teammember)
    permitido = False
    is_scrum = False
    us_proyecto = []
    rol_usuario = []
    try:
        for i in range(len(teammember)):
            try:
                team = Team.objects.get(id_team_member=teammember[i].id)
            except:
                continue
            if team.id_proyecto_id == proyecto.id:
                if teammember[i].id_rol.nombre == 'scrum':
                    is_scrum = True
                    permitido = True
    except:
        return HttpResponseForbidden()
    if permitido:

        a = sprints.split(":")
        long = len(a)
        b=[]
        for i in range(0, long-1):
            b.append(a[i].split(";"))
        for i in range(0, long - 1):
            us_nuevo = Us(descripcion_breve=b[i][0], descripcion_completa=b[i][1],
                          tiempo_finalizacion=b[i][2],
                          valor_negocio=b[i][3], prioridad=b[i][4], valor_tecnico=b[i][5],
                          prioridad_promedio=b[i][6],
                          fecha_creacion=date.today(), proyecto=proyecto, sprint=destino)
            us_nuevo.save()
        return render(request,'sprint/importar.html', {'proyecto':proyecto})
    else:
        return HttpResponseForbidden()

@login_required
@transaction.atomic
def elegir_para_importar(request, sprint_destino):
    """
    Se elige el Sprint a importar en el momento en el que se crea un nuevo sprint
    :param request
    :param sprint_destino: el sprint al que se importa
    """
    destino = Sprint.objects.get(id=sprint_destino)
    proyecto = destino.proyecto

    teammember = TeamMember.objects.filter(id_usuario=request.user.usuario.id)
    teammember = list(teammember)
    permitido = False
    is_scrum = False
    us_proyecto = []
    rol_usuario = []
    try:
        for i in range(len(teammember)):
            try:
                team = Team.objects.get(id_team_member=teammember[i].id)
            except:
                continue
            if team.id_proyecto_id == proyecto.id:
                if teammember[i].id_rol.nombre == 'scrum':
                    is_scrum = True
                    permitido = True
    except:
        return HttpResponseForbidden()
    if permitido:

        sprints = Sprint.objects.filter(~Q(id=sprint_destino))
        mensajes=[]
        for sprint in sprints:
            a = sprint.para_importar.split(":")
            long = len(a)
            b = []
            for i in range(0, long - 1):
                b.append(a[i].split(";"))
            mensaje = 'User Stories: '
            for i in range(0, long - 1):
                mensaje = mensaje+'- '+b[i][0]+' '
            mensajes.append(mensaje)
        sumas = zip(sprints, mensajes)
        return render(request, 'sprint/elegir_para_importar.html', {'destino':destino,'proyecto':proyecto,'sprints':sumas})
    else:
        return HttpResponseForbidden()

@login_required
@transaction.atomic
def sprints_anteriores(request, id_proyecto):
    """
       Se elige el Sprint anterior para modificaciones
       :param request
       :param id_proyecto:id del proyecto que se selecciona
       """
    proyecto = Proyecto.objects.get(id=id_proyecto)
    teammember = TeamMember.objects.filter(id_usuario=request.user.usuario.id)
    teammember = list(teammember)
    permitido = False
    is_scrum = False
    us_proyecto = []
    rol_usuario = []
    try:
        for i in range(len(teammember)):
            try:
                team = Team.objects.get(id_team_member=teammember[i].id)
            except:
                continue
            if team.id_proyecto_id == proyecto.id:
                permitido = True
                if teammember[i].id_rol.nombre == 'scrum':
                    is_scrum = True

    except:
        return HttpResponseForbidden()
    if permitido:
        sprints = Sprint.objects.filter(proyecto=proyecto, estado='TER').order_by('fecha_ini')
        mensajes = []
        for sprint in sprints:
            a = sprint.para_importar.split(":")
            long = len(a)
            b = []
            for i in range(0, long - 1):
                b.append(a[i].split(";"))
            mensaje = ''
            for i in range(0, long - 1):
                mensaje = mensaje + ' ' + b[i][0] + ' '
            mensajes.append(mensaje)
        sumas = zip(sprints, mensajes)
        return render(request, 'sprint/sprints_anteriores.html', {'sprints':sumas})
    else:
        return HttpResponseForbidden()
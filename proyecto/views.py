from datetime import date, datetime, timedelta

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.db import transaction
from django.db.models import Q
from django.shortcuts import render, redirect
from django.test.utils import isolate_apps

from Rol.models import Rol
from sprint.models import Sprint, Horas
from tablero.models import Tablero, Fase
from us.models import Us, Daily
from us.views import daterange
from usuario.models import Usuario
from .models import Proyecto
from proyecto.forms import CreateProyectoForm
from proyecto.forms import UpdateProyectoForm
from proyecto.forms import DisableProyectoForm
from proyecto.forms import BuscarProyecto
from datetime import date, datetime
from team.models import Team, TeamMember

from django.http import HttpResponse
from django.http import HttpResponseForbidden
from django.core.mail import EmailMessage


@login_required
@transaction.atomic
def index(request):
    return HttpResponse("<h1> PROYECTOS</h1>")

@login_required
@transaction.atomic
def crear_proyecto(request):
    """
    Vista para crear un proyecto
    :param request:
    """
    if request.user.is_superuser:
        users = User.objects.all().order_by('username')
        rols = Rol.objects.all()
        if request.method == "POST":
            form = CreateProyectoForm(request.POST)
            if form.is_valid():
                nombre = form.cleaned_data['nombre']
                proyectoNvo = Proyecto(nombre=nombre)
                usuarios = request.POST.getlist('checks[]')
                teammembers = []
                rol = Rol.objects.get(nombre='scrum')
                i = 0
                for usuario in usuarios:
                    usua=Usuario.objects.get(user_id=usuario)
                    teammembers.append(TeamMember.objects.create(id_usuario=usua, id_rol=rol))
                    i = i + 1
                proyectoNvo.save()
                team = Team.objects.create(
                    id_proyecto=proyectoNvo)
                for teammember in teammembers:
                    team.id_team_member.add(teammember)
                    try:
                        email = EmailMessage('Se le asignó un Proyecto',
                                         'Buenas '+teammember.id_usuario.user.username+',\nSe le asignó como Scrum Master en el Proyecto ' + proyectoNvo.nombre + '.\nSaludos. ',
                                         to=[teammember.id_usuario.user.email])
                        email.send()
                    except:
                        pass
                team.save()
                messages.success(request, ('Se creó el proyecto seleccionado!'))
                return redirect('/menu')
        else:
            form = CreateProyectoForm()
        return render(request, 'proyecto/create_proyecto.html', {'form': form, 'users': users, 'rols':rols})
    else:
        return HttpResponseForbidden()

@login_required
@transaction.atomic
def listar_proyectos(request):
    """
       Buscar un Proyecto por nombre
       :param request:

       """
    if request.method == 'POST':
        busqueda = request.POST['buscalo']
        """
        Filtra de acuerdo a la palabra que se indico 
        :param request:
        """
        if busqueda:
            match = Proyecto.objects.filter(Q(nombre__contains=busqueda))
            if match:
                return render(request, 'proyecto/listar_proyectos.html', {'proyectos': match})
            else:
                messages.error(request, 'No existe un proyecto con ese nombre')
        else:
            matchh = Proyecto.objects.filter(Q(nombre__contains=busqueda))
            return render(request, 'proyecto/listar_proyectos.html', {'proyectos': matchh.all()})
    else:
        proyectos = Proyecto.objects.all()
        return render(request, 'proyecto/listar_proyectos.html', {'proyectos': proyectos})

    return render(request, 'proyecto/listar_proyectos.html')

@login_required
@transaction.atomic
def modificar_proyecto(request, id_proyecto):
    """
    Esta view permite modificar un proyecto
    :param request:
    """
    teammember = TeamMember.objects.filter(id_usuario=request.user.usuario.id)
    teammember = list(teammember)
    permitido = False
    proyecto = Proyecto.objects.get(id=id_proyecto)
    try:
        for i in range(len(teammember)):
            try:
                team = Team.objects.get(id_team_member=teammember[i].id)
            except:
                continue
            if team.id_proyecto_id == proyecto.id:
                permisos = teammember[i].id_rol.list_permisos()
                for j in range(len(permisos)):
                    if permisos[j].orden == 6:
                        permitido = True
    except:
        if not request.user.is_superuser:
            return HttpResponseForbidden()

    correo = "Buenas,\nEl proyecto "+proyecto.nombre+" fue modificado.\nUsuarios que conforman el team:\n"

    if permitido:
        abc = 0
        teammember = TeamMember.objects.all()
        teammember = list(teammember)
        usuarios = []
        roless = []
        checkeado_rol = {}
        proyecto = Proyecto.objects.get(id=id_proyecto)
        for i in range(len(teammember)):
            try:
                team = Team.objects.get(id_team_member=teammember[i].id)
            except:
                continue
            if team.id_proyecto_id == proyecto.id:
                usuarios.append(teammember[i].id_usuario)
                u = teammember[i].id_usuario
                roless.append(teammember[i].id_rol)
                checkeado_rol[u] = teammember[i].id_rol.id

        users = User.objects.all()
        rols = Rol.objects.all()

        checkeado = []
        chr = []
        for user in users:
            checkeado.append(-1)
            chr.append("-1")
        indice = 0
        for user in users:
            for usuario in usuarios:
                if user.id == usuario.user_id:
                    checkeado[indice] = user.id
                    chr[indice] = checkeado_rol[usuario]
            indice = indice + 1

        proyectoNvo = Proyecto.objects.get(id=id_proyecto)
        if request.method == 'POST' and 'save' in request.POST:
            # formulario enviado
            proyecto_form = UpdateProyectoForm(request.POST, instance=proyecto)
            if proyecto_form.is_valid():
                # formulario validado correctamente
                inicio = proyecto_form.cleaned_data['fecha_ini_est']
                fin = proyecto_form.cleaned_data['fecha_fin_est']
                obs = proyecto_form.cleaned_data['observaciones']
                proyectoNvo = Proyecto.objects.get(id=id_proyecto)
                if inicio is None:
                    messages.error(request,'Complete el campo de Fecha de inicio')
                    return redirect('/proyecto/update/'+str(proyectoNvo.id))
                if fin is None:
                    messages.error(request,'Complete el campo de Fecha de finalización')
                    return redirect('/proyecto/update/' + str(proyectoNvo.id))
                if obs is "":
                    messages.error(request,'Complete el campo de Observaciones')
                    return redirect('/proyecto/update/' + str(proyectoNvo.id))
                if inicio >= fin:
                    messages.error(request, 'La fecha de fin estimada debe ser mayor a la fecha de inicio estimada')
                    return redirect('/proyecto/update/' + str(proyectoNvo.id))
                proyecto_form.save()
                proyectoNvo = Proyecto.objects.get(id=id_proyecto)
                usuarios = request.POST.getlist('checks[]')
                roles = []
                teammembers = []
                i = 0

                hayScrum=False
                cont=0
                for usuario in usuarios:
                    roles.append(request.POST.getlist('roles' + usuario + '[]'))
                    usua = Usuario.objects.get(user_id=usuario)
                    rol = Rol.objects.get(id=roles[cont][0])
                    if rol.nombre=='scrum':
                        hayScrum=True
                    cont=cont+1

                if hayScrum==True:
                    for usuario in usuarios:
                        roles.append(request.POST.getlist('roles' + usuario + '[]'))
                        usua = Usuario.objects.get(user_id=usuario)
                        rol = Rol.objects.get(id=roles[i][0])

                        teamm = TeamMember.objects.filter(team__id_proyecto=proyectoNvo)
                        teamm.delete()

                        teammembers.append(TeamMember.objects.create(id_usuario=usua, id_rol=rol))
                        correo += usua.user.username + ' con el rol ' + rol.nombre + '\n'
                        i = i + 1
                    proyectoNvo.save()
                    team = Team.objects.get(id_proyecto=proyectoNvo)
                    team.delete()
                    team = Team.objects.create(id_proyecto=proyectoNvo)
                    for teammember in teammembers:
                        team.id_team_member.add(teammember)
                        try:
                            email = EmailMessage('Modificación en proyecto al que pertenece',
                                                 correo,
                                                 to=[teammember.id_usuario.user.email])
                            email.send()
                        except:
                            pass
                    team.save()
                    messages.success(request, ('Se actualizó el proyecto.'))
                    return redirect('/proyecto/trabajo/' + str(id_proyecto))
                else:
                    messages.error(request, 'Debe existir un scrum en el proyecto')
                    return redirect('/proyecto/update/' + str(proyectoNvo.id))
        else:
            # formulario inicial
            proyecto_form = UpdateProyectoForm(instance=proyecto)
        return render(request, 'proyecto/update_proyecto.html', {'proyecto_form': proyecto_form, 'users': users,
                                                                 'rols': rols, 'checkeado': checkeado,
                                                                 'checkeado_rol': checkeado_rol, 'chr': chr,
                                                                 'val': "a", 'proyecto':proyectoNvo})
    else:
        return HttpResponseForbidden()

@login_required
@transaction.atomic
def eliminado (request,pk):
    """
    Página que confirma que un proceso fue realizado con éxito
    :param request:
    :param pk:
    """
    return render(request, 'proyecto/eliminado.html')

@login_required
@transaction.atomic
def eliminado2 (request):
    """
    Página que confirma que un proceso fue realizado con éxito
    :param request:
    """
    return render(request, 'proyecto/eliminado.html')


@login_required
@transaction.atomic
def buscar_proyecto(request):
    """
    Buscar un Proyecto por nombre
    :param request:

    """
    if request.method == 'POST':
        busqueda = request.POST['buscalo']
        """
        Filtra de acuerdo a la palabra que se indico 
        :param request:
        """
        if busqueda:
            match = Proyecto.objects.filter(Q(nombre__contains=busqueda))
            if match:
                return render(request, 'proyecto/buscar_proyecto.html', {'sr': match})
            else:
                messages.error(request, 'No existe un proyecto con ese nombre')
        else:
            matchh = Proyecto.objects.filter(Q(nombre__contains=busqueda))
            return render(request, 'proyecto/buscar_proyecto.html', {'sr': matchh.all()})
    else:
        proyectos = Proyecto.objects.all()
        return render(request, 'proyecto/buscar_proyecto.html', {'sr': proyectos})

    return render(request, 'proyecto/buscar_proyecto.html')



@login_required
@transaction.atomic
def eliminar_proyecto(request, id_proyecto):
    """
    Esta view permite borrar un proyecto
    :param request:
    """
    proyecto = Proyecto.objects.get(id=id_proyecto)
    if request.method == 'POST':
        proyecto.delete()
        messages.success(request, ('Se elimino el proyecto seleccionado!'))
        return redirect('confirmado.html')
    return render(request, 'proyecto/delete_proyecto.html', {'proyecto': proyecto})

@login_required
@transaction.atomic
def confirmado (request,pk):
    """
    Página que confirma que un proceso fue realizado con éxito
    :param request:
    :param pk:
    """
    return render(request, 'proyecto/confirmado.html')

@login_required
@transaction.atomic
def confirmado2 (request):
    """
    Página que confirma que un proceso fue realizado con éxito
    :param request:
    """
    return render(request, 'proyecto/confirmado.html')


@login_required
@transaction.atomic
def list_proyecto(request, id_proyecto):
    """
    Esta view lista los datos del proyecto de acuerdo al id seleccionado
    :param request:
    """
    teammember = TeamMember.objects.filter(id_usuario=request.user.usuario.id)
    teammember = list(teammember)
    permitido = False
    us_proyecto = []
    rol_usuario=[]
    proyecto = Proyecto.objects.get(id=id_proyecto)
    try:
        for i in range(len(teammember)):
            try:
                team = Team.objects.get(id_team_member=teammember[i].id)
            except:
                continue
            if team.id_proyecto_id == proyecto.id:
                permisos = teammember[i].id_rol.list_permisos()
                for j in range(len(permisos)):
                    if permisos[j].orden == 8:
                        permitido = True
    except:
        if not request.user.is_superuser:
            return HttpResponseForbidden()

    teammember2 = TeamMember.objects.all()
    teammember2 = list(teammember2)

    for i in range(len(teammember2)):
        try:
            team = Team.objects.get(id_team_member=teammember2[i].id)
        except:
            continue
        if team.id_proyecto_id == proyecto.id:
            us_proyecto.append(teammember2[i].id_usuario)
            rol_usuario.append(teammember2[i].id_rol)

    if request.user.is_superuser or permitido:
        proyecto = Proyecto.objects.filter(id=id_proyecto)
        contexto = {'proyectos': proyecto, 'us_proyecto': us_proyecto, 'rol_usuario': rol_usuario }
        return render(request, 'proyecto/visualizar_proyecto.html', contexto)
    else:
        return HttpResponseForbidden()

@login_required
@transaction.atomic
def trabajo(request, pk):
    """
    Vista para visualizar el area de trabajo de un proyecto, es la pagina principal para trabajar en un proyecto
    :param request:
    :param pk: id del proyecto
    """
    teammember = TeamMember.objects.filter(id_usuario=request.user.usuario.id)
    teammember = list(teammember)
    permitido = False
    is_scrum = False
    is_owner = False
    us_proyecto = []
    rol_usuario=[]
    proyecto = Proyecto.objects.get(id=pk)
    us1 = Us.objects.filter(proyecto=proyecto,aprobado=0)
    us2 = Us.objects.filter(proyecto=proyecto, aprobado=2)
    us3 = Us.objects.filter(proyecto=proyecto, aprobado=3)
    puede_terminar = False
    if len(list(us1))+len(list(us2))+len(list(us3)) == 0:
        puede_terminar = True
    try:
        for i in range(len(teammember)):
            try:
                team = Team.objects.get(id_team_member=teammember[i].id)
            except:
                continue
            if team.id_proyecto_id == proyecto.id:
                #permisos = teammember[i].id_rol.list_permisos()
                #for j in range(len(permisos)):
                #    if permisos[j].orden == 8:
                #        permitido = True
                if teammember[i].id_rol.nombre == 'scrum':
                    is_scrum = True
                if teammember[i].id_rol.nombre == 'owner':
                    is_owner = True
                permitido = True
    except:
        if not request.user.is_superuser:
            return HttpResponseForbidden()

    super = False
    if request.user.is_superuser:
        super=True

    teammember2 = TeamMember.objects.all()
    teammember2 = list(teammember2)

    for i in range(len(teammember2)):
        try:
            team = Team.objects.get(id_team_member=teammember2[i].id)
        except:
            continue
        if team.id_proyecto_id == proyecto.id:
            us_proyecto.append(teammember2[i].id_usuario)
            rol_usuario.append(teammember2[i].id_rol)

    if permitido:
        proyecto = Proyecto.objects.filter(id=pk)
        tablero = Tablero.objects.filter(proyecto=proyecto[0])
        activo = Sprint.objects.filter(proyecto=proyecto[0], estado='ACT')
        activo = list(activo)
        hay_activo = False
        if len(activo) > 0:
            hay_activo = True
            activo = activo[0]
        contexto = {'proyectos': proyecto, 'is_scrum':is_scrum, 'super':super, 'us_proyecto': us_proyecto, 'rol_usuario': rol_usuario,'tablero':tablero,'hay_activo':hay_activo,'activo':activo, 'puede_terminar':puede_terminar, 'is_owner':is_owner}
        return render(request, 'proyecto/trabajo.html', contexto)
    else:
        return HttpResponseForbidden()

@login_required
@transaction.atomic
def inactivar_proyecto(request, id_proyecto):
    """
    Esta view inactiva un proyecto seleccionado
    :param request:
    """
    teammember = TeamMember.objects.filter(id_usuario=request.user.usuario.id)
    teammember = list(teammember)
    permitido = False
    proyecto = Proyecto.objects.get(id=id_proyecto)
    c = 0
    try:
        for i in range(len(teammember)):
            try:
                team = Team.objects.get(id_team_member=teammember[i].id)
            except:
                continue
            if team.id_proyecto_id == proyecto.id:
                permisos = teammember[i].id_rol.list_permisos()
                for j in range(len(permisos)):
                    if permisos[j].orden == 7:
                        permitido = True
    except:
        return HttpResponseForbidden('c='+str(c))
    super = False
    if request.user.is_superuser:
        super=True

    if permitido:
        proyecto = Proyecto.objects.get(id=id_proyecto)
        if request.method == 'POST':
            proyecto.estado = 'INA'
            proyecto.fecha_fin = date.today()
            proyecto.save()
            messages.success(request, ('Se inactivo el proyecto seleccionado!'))
            #return redirect('confirmado.html')
            return redirect('/proyecto/trabajo/'+ str(proyecto.id))
        return render(request, 'proyecto/inactivar_proyecto.html', {'proyecto': proyecto, 'super':super})
    else:
        return HttpResponseForbidden()


@login_required
@transaction.atomic
def activar_proyecto(request, id_proyecto):
    """
    Esta view permite activar un proyecto, por el scrum
    :param request:
    """
    #Falta permisos - debe ser scrum
    proyecto = Proyecto.objects.get(id=id_proyecto)
    if request.method == 'POST':
            proyecto.estado = 'ACT'
            if proyecto.fecha_ini is None:
                proyecto.fecha_ini = date.today()
            proyecto.save()
            messages.success(request, ('Se activó el proyecto seleccionado.'))
            return redirect('/proyecto/trabajo/'+str(proyecto.id))
    return render(request, 'proyecto/activar_proyecto.html', {'proyecto': proyecto, 'super':super})

@login_required
@transaction.atomic
def cancelar_proyecto(request, id_proyecto):
    """
    Esta view permite cancelar el proyecto seleccionado
    :param request:
    """
    #Falta permisos - debe ser scrum
    proyecto = Proyecto.objects.get(id=id_proyecto)
    if request.method == 'POST':
            proyecto.estado = 'CAN'
            proyecto.save()
            messages.success(request, ('Se canceló el proyecto seleccionado.'))
            return redirect('/proyecto/trabajo/'+str(proyecto.id))
    return render(request, 'proyecto/cancelar_proyecto.html', {'proyecto': proyecto, 'super':super})

@login_required
@transaction.atomic
def terminar_proyecto(request, id_proyecto):
    """
    Esta view permite terminar proyecto seleccionado
    :param request:
    """
    #Falta permisos - debe ser scrum
    proyecto = Proyecto.objects.get(id=id_proyecto)
    if request.method == 'POST':
            proyecto.estado = 'TER'
            proyecto.fecha_fin = date.today()
            proyecto.save()
            messages.success(request, ('Se terminó el proyecto seleccionado.'))
            return redirect('/proyecto/trabajo/'+str(proyecto.id))
    return render(request, 'proyecto/terminar_proyecto.html', {'proyecto': proyecto, 'super':super})

@login_required
@transaction.atomic
def burndown(request, id_sprint):
    """
    Esta view genera el reporte del burndown chart
    :param request:
    """
    sprint= Sprint.objects.get(id=id_sprint)
    proyecto = sprint.proyecto
    if sprint.fecha_fin != None:
        fecha_fin = sprint.fecha_fin
    else:
        fecha_fin = sprint.fecha_fin_est
    fecha_inicio = sprint.fecha_ini
    cant_dias = fecha_fin - fecha_inicio + timedelta(days=1)
    fecha_fin_aux = sprint.fecha_fin_est
    fecha_fin = fecha_fin + timedelta(days=1)
    fechas = []
    horas_trabajadas = []
    """fechas de feriados del anho 2018"""
    feriados = [datetime.strptime('2018-01-01', '%Y-%m-%d').date(),
                datetime.strptime('2018-02-26', '%Y-%m-%d').date(),
                datetime.strptime('2018-03-29', '%Y-%m-%d').date(),
                datetime.strptime('2018-03-30', '%Y-%m-%d').date(),
                datetime.strptime('2018-05-01', '%Y-%m-%d').date(),
                datetime.strptime('2018-05-14', '%Y-%m-%d').date(),
                datetime.strptime('2018-05-15', '%Y-%m-%d').date(),
                datetime.strptime('2018-06-11', '%Y-%m-%d').date(),
                datetime.strptime('2018-08-15', '%Y-%m-%d').date(),
                datetime.strptime('2018-09-29', '%Y-%m-%d').date(),
                datetime.strptime('2018-12-08', '%Y-%m-%d').date(),
                datetime.strptime('2018-12-25', '%Y-%m-%d').date()]
    if date.today() < fecha_fin  + timedelta(days=1):
        fecha_fin = date.today()  + timedelta(days=1)
    for fecha in daterange(fecha_inicio, fecha_fin):
        if fecha.weekday() == 6 or fecha in feriados:
            """ No se toma en consideracion los domingos y feriados"""
            cant_dias = cant_dias - timedelta(days=1)
            """ No se toma en consideracion aquellos Us que no se encuentran aun incluidos en el Sprint"""
        horas_a_restar = 0
        try:
            dailys = Daily.objects.filter(fecha__startswith=fecha)
            dailys = list(dailys)
            for daily in dailys:
                if daily.sprint == sprint:
                    horas_a_restar = horas_a_restar + daily.tiempo
            horas_trabajadas.append(horas_a_restar)
        except:
            pass
        fechas.append(fecha)
    if (cant_dias >= timedelta(days=1)):
        cant_dias = str(cant_dias).split('d')[0]
        cant_dias = int(cant_dias)
    """ Se obtiene las horas trabajadas del Sprint al cual pertenece el proyecto"""
    horas = Horas.objects.filter(sprint=sprint)
    cant_horas = 0
    for hora in horas:
        cant_horas = cant_horas + hora.horas_trabajadas
    cant_horas = cant_horas * cant_dias
    cant_horas2 = cant_horas
    hs = []
    for i in range(0, len(horas_trabajadas)):
        cant_horas2 = cant_horas2 - horas_trabajadas[i]
        hs.append(cant_horas2)
#for h in horas_trabajadas:
#        if h != 0:
#           cant_horas2 = cant_horas2 - h
#          hs.append(cant_horas2)
    return render(request, 'proyecto/burndown.html', {'fechas':fechas,'horas':hs,'fecha_inicio':fecha_inicio,'fecha_fin':fecha_fin_aux,'cant_horas':cant_horas, 'sprint':sprint})

@login_required
@transaction.atomic
def elegirburndown(request, id_proyecto):
    """
    Esta view lista los sprints para generar burndown chart
    :param request:
    """
    proyecto = Proyecto.objects.get(id=id_proyecto)
    sprints = Sprint.objects.filter(proyecto=proyecto).order_by('fecha_ini')
    mensajes = []
    for sprint in sprints:
        a = sprint.para_importar.split(":")
        long = len(a)
        b = []
        for i in range(0, long - 1):
            b.append(a[i].split(";"))
        mensaje = ''
        for i in range(0, long - 1):
            mensaje = mensaje + ' ' + b[i][0] + ' '
        mensajes.append(mensaje)
    sumas = zip(sprints, mensajes)
    return render(request, 'proyecto/elegirburndown.html', {'sprints':sumas, 'proyecto':proyecto})

@login_required
@transaction.atomic
def elegirusuario(request, id_proyecto):
    """
    Esta view lista los team members con su rol, del proyecto para generar reporte con sus us correspondiente al sprint activo
    :param request: proyecto id
    """
    proyecto = Proyecto.objects.get(id=id_proyecto)
    us_proyecto = []
    rol_usuario = []
    teammember = TeamMember.objects.all()
    teammember = list(teammember)

    for i in range(len(teammember)):
        try:
            team = Team.objects.get(id_team_member=teammember[i].id)
        except:
            continue
        if team.id_proyecto_id == proyecto.id:
            us_proyecto.append(teammember[i].id_usuario)
            rol_usuario.append(teammember[i].id_rol)
        sumas = zip(us_proyecto, rol_usuario)
    return render(request, 'proyecto/elegirusuario.html', {'usuarios':sumas, 'proyecto':proyecto})

@login_required
@transaction.atomic
def reportetm(request, id_usuario, id_proyecto):
    """
    Esta view se utiliza para realizar el reporte de US por Team Members
    :param request:
    """
    user = User.objects.get(id = id_usuario)
    proyecto = Proyecto.objects.get(id = id_proyecto)
    sprint = Sprint.objects.get(proyecto = proyecto, estado = 'ACT')
    uss = Us.objects.filter(usuario=user, sprint=sprint)
    cant_us = len(list(uss))
    return render(request, 'proyecto/reportetm.html', {'uss':uss, 'cant_us':cant_us,'user':user, 'proyecto':proyecto})

@login_required
@transaction.atomic
def reporte_proyecto(request, id_proyecto):
    """
    Esta view genera un reporte del proyecto
    :param request:
    """
    #FALTA PONER PERMISOS
    proyecto = Proyecto.objects.get(id=id_proyecto)

    #lista de team members del proyecto
    us_proyecto = []
    rol_usuario = []
    teammember = TeamMember.objects.all()
    teammember = list(teammember)

    for i in range(len(teammember)):
        try:
            team = Team.objects.get(id_team_member=teammember[i].id)
        except:
            continue
        if team.id_proyecto_id == proyecto.id:
            us_proyecto.append(teammember[i].id_usuario)
            rol_usuario.append(teammember[i].id_rol)

    #lista de sprints del proyecto
    sprint_activo = Sprint.objects.filter(proyecto=proyecto, estado='ACT')
    sprint_activo = list(sprint_activo)
    if sprint_activo:
        sprint_activo=sprint_activo[0]

    sprints_anteriores=Sprint.objects.filter(proyecto=proyecto, estado='TER')
    sprints_anteriores = list(sprints_anteriores)

    tablero = Tablero.objects.filter(proyecto=proyecto)
    tablero = list(tablero)
    fases=[]
    for tb in tablero:
        fs=Fase.objects.filter(tablero=tb)
        fs=list(fs)
        fases.append(fs)


    return render(request, 'proyecto/reporte_proyecto.html' ,{'proyecto':proyecto, 'us_proyecto':us_proyecto,
                                                              'rol_usuario':rol_usuario, 'sprint_activo':sprint_activo,
                                                              'sprints_anteriores':sprints_anteriores, 'tablero':tablero,
                                                              'fases':fases})


@login_required
@transaction.atomic
def elegirfecha(request, id_proyecto):
    """
    Esta view permite elegir la fecha de los dailys a visualizar
    """
    proyecto = Proyecto.objects.get(id=id_proyecto)
    us = Us.objects.filter(proyecto=proyecto)
    dailys = Daily.objects.filter(us=us)

    if request.method == 'POST':

        busqueda = request.POST['buscalo']
        busqueda2 = datetime.strptime(busqueda, "%Y-%m-%d")

       # busqueda2 = datetime.strptime(bs, "%Y-%m-%d %H:%M:%S.%f")
        """
        Filtra de acuerdo a la palabra que se indico 
        :param request:
        """
        if busqueda2:
            #match = dailys.objects.filter(Q(fecha__day=busqueda2.day))
            dailys = Daily.objects.filter(Q(fecha__startswith=busqueda))
            dailys = list(dailys)
            match = []
            for daily in dailys:
                if daily.us.sprint.proyecto == proyecto:
                    match.append(daily)
            if match:
                return render(request, 'proyecto/elegirfecha.html', {'sr': match, 'proyecto': proyecto,'busqueda':busqueda})
            else:
                messages.error(request, 'No se ha trabajado en esa fecha')
        else:
            #matchh = dailys.objects.filter(Q(fecha__exact=busqueda2))
            #return render(request, 'proyecto/elegirfecha.html', {'sr': matchh.all()})
            return redirect('/proyecto/elegirfecha/' + str(proyecto.id))
    else:
        #dailys = dailys.objects.all()
        return render(request, 'proyecto/elegirfecha.html',{'proyecto':proyecto})

    return render(request, 'proyecto/elegirfecha.html',{'match':match, 'proyecto':proyecto})

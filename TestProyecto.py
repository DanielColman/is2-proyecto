from unittest import TestCase
import django
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "GestorP.base")
django.setup()
from proyecto.models import Proyecto
from django.test import Client

"""
Prubas unitarias para la app proyecto
"""

class Project(TestCase):
    # def test_pagina(self):
    #     """
    #     Carga la página y verifica que encuentra
    #     """
    #     client = Client()
    #     response = client.get("/proyecto/")
    #     #HTTP_302_FOUND
    #     self.assertEqual(response.status_code, 200)

    def test_create_proyecto(self):
        """
        Verifica que un proyecto se crea
        """
        try:
            nombre = "prueba"
            fecha_ini = '2001-01-01'
            fecha_fin = '2002-01-01'
            observaciones = 'obs'
            proyectoNvo = Proyecto(nombre=nombre, fecha_ini=fecha_ini, fecha_fin=fecha_fin, observaciones=observaciones)
            proyectoNvo.save()
            proyectoNvo.delete()
        except:
            proyectoNvo.delete()
            raise

    def test_create_proyecto2(self):
        """
        Verifica que un proyecto se crea
        """
        try:
            nombre = "prueba"
            fecha_ini = '2001-01-01'
            fecha_fin = '2002-01-01'
            observaciones = 'obs'
            proyectoNvo = Proyecto(nombre=nombre, fecha_ini=fecha_ini, fecha_fin=fecha_fin, observaciones=observaciones)
            proyectoNvo.save()
            proyectoNvo = Proyecto(nombre=nombre, fecha_ini=fecha_ini, fecha_fin=fecha_fin, observaciones=observaciones)
            proyectoNvo.delete()
        except:
            proyectoNvo.delete()
            raise

    def test_create_proyecto3(self):
        """
        Verifica que un proyecto se crea
        """
        try:
            nombre = "prueba_w"
            fecha_ini = '2019-01-01'
            fecha_fin = '2020-01-01'
            observaciones = 'obs'
            proyectoNvo = Proyecto(nombre=nombre, fecha_ini=fecha_ini, fecha_fin=fecha_fin, observaciones=observaciones)
            proyectoNvo.save()
            proyectoNvo.delete()
        except:
            proyectoNvo.delete()
            raise

    def test_create_proyecto4(self):
        """
        Verifica que un proyecto se crea
        """
        try:
            nombre = "prueba_w"
            fecha_ini = '2019-01-01'
            fecha_fin = '2020-01-01'
            observaciones = 'obs'
            proyectoNvo = Proyecto(nombre=nombre, fecha_ini=fecha_ini, fecha_fin=fecha_fin, observaciones=observaciones)
            proyectoNvo.save()
            proyectoNvo.delete()
        except:
            proyectoNvo.delete()
            raise

    def test_create_proyecto5(self):
        """
        Verifica que un proyecto se crea
        """
        try:
            nombre = "Goliat"
            fecha_ini = '2019-01-01'
            fecha_fin = '2020-01-01'
            observaciones = 'obs'
            proyectoNvo = Proyecto(nombre=nombre, fecha_ini=fecha_ini, fecha_fin=fecha_fin, observaciones=observaciones)
            proyectoNvo.save()
            proyectoNvo.delete()
        except:
            proyectoNvo.delete()
            raise

    def test_create_proyecto6(self):
        """
        Verifica que un proyecto se crea
        """
        try:
            nombre = "Pegaso"
            fecha_ini = '2019-01-01'
            fecha_fin = '2020-01-01'
            observaciones = 'obs'
            proyectoNvo = Proyecto(nombre=nombre, fecha_ini=fecha_ini, fecha_fin=fecha_fin, observaciones=observaciones)
            proyectoNvo.save()
            proyectoNvo.delete()
        except:
            proyectoNvo.delete()
            raise

    def test_update_proyeto(self):
         """
         Verifica que un proyecto se modifica
         """
         try:
             nombre = "prueba"
             fecha_ini = '2001-01-01'
             fecha_fin = '2002-01-01'
             observaciones = 'obs'
             proyectoNvo = Proyecto(nombre=nombre, fecha_ini=fecha_ini, fecha_fin=fecha_fin, observaciones=observaciones)
             proyectoNvo.save()
             proyectoNvo2 = Proyecto(nombre="prueba2", fecha_ini=fecha_ini, fecha_fin=fecha_fin, observaciones=observaciones)
             proyectoNvo.save()
             proyectoNvo2.save()
             proyectoNvo2.nombre = 'prueba3'
             proyectoNvo2.delete()
             proyectoNvo.delete()
         except:
             proyectoNvo.delete()
             proyectoNvo2.delete()
             raise
    def test_update_proyeto2(self):
         """
         Verifica que un proyecto se modifica
         """
         try:
             nombre = "prueba"
             fecha_ini = '2001-01-01'
             fecha_fin = '2002-01-01'
             observaciones = 'obs'
             proyectoNvo = Proyecto(nombre=nombre, fecha_ini=fecha_ini, fecha_fin=fecha_fin, observaciones=observaciones)
             proyectoNvo.save()
             proyectoNvo2 = Proyecto(nombre="prueba2", fecha_ini=fecha_ini, fecha_fin=fecha_fin, observaciones=observaciones)
             proyectoNvo.save()
             proyectoNvo2.save()
             #No falla (siguiente linea):
             proyectoNvo2.nombre = 'prueba3'
             proyectoNvo2.nombre = 'prueba'
             proyectoNvo2.save()
             proyectoNvo2.delete()
             proyectoNvo.delete()
         except:
             proyectoNvo.delete()
             proyectoNvo2.delete()
             raise

    def test_delete_proyecto(self):
        """
        Verifica que un proyecto se elimina sin problemas
        """
        try:
            nombre = "prueba"
            fecha_ini = '2001-01-01'
            fecha_fin = '2002-01-01'
            observaciones = 'obs'
            proyectoNvo = Proyecto(nombre=nombre, fecha_ini=fecha_ini, fecha_fin=fecha_fin, observaciones=observaciones)
            proyectoNvo.save()
            proyectoNvo.delete()
        except:
            raise

    def test_delete_proyecto2(self):
        """
        Verifica que un proyecto se elimina sin problemas
        """
        try:
            nombre = "prueba"
            fecha_ini = '2001-01-01'
            fecha_fin = '2002-01-01'
            observaciones = 'obs'
            proyectoNvo = Proyecto(nombre=nombre, fecha_ini=fecha_ini, fecha_fin=fecha_fin, observaciones=observaciones)
            proyectoNvo.save()
            proyectoNvo.delete()
            proyectoNvo.delete()
        except:
            raise

    def test_delete_proyecto3(self):
        """
        Verifica que un proyecto se elimina sin problemas
        """
        try:
            nombre = "Worst"
            fecha_ini = '2019-01-01'
            fecha_fin = '2020-01-01'
            observaciones = 'obs'
            proyectoNvo = Proyecto(nombre=nombre, fecha_ini=fecha_ini, fecha_fin=fecha_fin, observaciones=observaciones)
            proyectoNvo.save()
            proyectoNvo.delete()
        except:
            raise
from django import forms
from .models import Sprint, Reunion, Horas

class IniciarSprintForm(forms.ModelForm):
    """
    Formulario para la creación de un nuevo Sprint
    """
    class Meta:
        model = Sprint
        fields = ('fecha_fin_est',)


class ReunionForm(forms.ModelForm):
    """
    Formulario para la creación de Reuniones
    """
    class Meta:
        model = Reunion
        fields = ('reunion',)
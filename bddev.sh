#!/bin/bash
echo "---Base de datos bddev para entorno de desarrollo---"
echo "Borrando base de datos bddev existente..."
#dropdb -i --if-exists -U admin bddev
dropdb -i --if-exists bddev
if [ "$?" -ne 0 ]
then
    echo -e "No se pudo borrar la base de datos,  verifique que no esté siendo usada."
    exit 1
fi
echo "Se ha borrado la base de datos bddev."
echo "Creando la base de datos bddev..."
#createdb -U admin bddev
createdb bddev
if [ "$?" -ne 0 ]
then
    echo -e "No se pudo crear bddev"
    exit 2
fi
echo "Se ha creado bddev"

source .env/bin/activate
#python manage.py makemigrations
#python manage.py migrate
#python bddev_populate.py
cd /home/daniel
#Cambiar por el usuario correspondiente linux
PGPASSWORD="postgres" pg_restore -h localhost -p 5432 -U postgres -d bddev -v "bddevbu"



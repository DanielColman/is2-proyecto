#!/bin/bash
echo "¡Bienvenido!"
echo "Seleccione uno de los siguientes entornos de despliegue:"
PS3='Por favor, ingrese una opción:' 
options=("Desarrollo" "Producción" "Salir")
select opt in "${options[@]}"
do
    case $opt in
        "Desarrollo")
            echo "Elegió desplegar desarrollo..."
            echo
            #echo "Elija uno de los siguientes tags:"
            #git ls-remote --tags origin
            echo "Ingrese en nombre del tag:"
            read tg
            git clone https://gitlab.com/DanielColman/is2-proyecto.git --branch $tg --single-branch
            cd is2-proyecto
            echo "Se descargan los archivos de GitLab.com"
            while [ "$?" -ne 0 ]; do
                 echo
                 echo "No existe tag con el nombre proveído. Vuelva a intentar..."
                 echo
                 echo "Elija uno de los siguientes tags:"
                 git tag
                 read -p "Ingrese en nombre del tag: " tg
                 git clone git@gitlab.com:DanielColman/is2-proyecto.git --branch $tg --single-branch
                 cd is2-proyecto

            done
            echo
            python3 -m venv .env
            source .env/bin/activate
            pip install -r requirements.txt
            echo "Poblando Corriendo Script de Poblacion"
            chmod +x bddev.sh
            sudo -u postgres ./bddev.sh
            chmod +x echo.sh
            sudo ./echo.sh
	    echo "Generando documentacion..."
	    source .env/bin/activate
   	    pycco *.py
  	    pycco accounts/*.py
	    pycco GestorP/*.py
  	    pycco menu/*.py
	    pycco proyecto/*.py
	    pycco Rol/*.py
	    pycco sprint/*.py
	    pycco tablero/*.py
	    pycco team/*.py
	    pycco us/*.py
  	    pycco usuario/*.py
        echo "Corriendo UnitTests..."
            python -m unittest TestInicioSesion.py
            python -m unittest TestProyecto.py
            python -m unittest TestRoles.py
            python -m unittest TestSprint.py
            python -m unittest TestTipoUserStory.py
            python -m unittest TestUsuario.py
            echo "Corriendo Servidor..."
            python3 manage.py runserver
            break
            ;;
        "Producción")

            echo "Configurando servidor httpd..."
            echo "Elegió desplegar Produccion..."
            echo
            #echo "Elija uno de los siguientes tags:"
            #git ls-remote --tags origin
            echo "Ingrese en nombre del tag:"
            read tg
            git clone https://gitlab.com/DanielColman/is2-proyecto.git --branch $tg --single-branch
            cd is2-proyecto
            echo "Se descargan los archivos de GitLab.com"
            while [ "$?" -ne 0 ]; do
                 echo
                 echo "No existe tag con el nombre proveído. Vuelva a intentar..."
                 echo
                 echo "Elija uno de los siguientes tags:"
                 git tag
                 read -p "Ingrese en nombre del tag: " tg
                 git clone git@gitlab.com:DanielColman/is2-proyecto.git --branch $tg --single-branch
                 cd is2-proyecto

            done
            echo
            python3 -m venv .env
            source .env/bin/activate
            pip install -r requirements.txt
            echo "Poblando Corriendo Script de Poblacion"
            chmod +x bddev.sh
            sudo -u postgres ./bdpro.sh
            chmod +x echo.sh
            sudo ./echo.sh
	    echo "Generando documentacion..."
	    source .env/bin/activate
   	    pycco *.py
  	    pycco accounts/*.py
	    pycco GestorP/*.py
  	    pycco menu/*.py
	    pycco proyecto/*.py
	    pycco Rol/*.py
	    pycco sprint/*.py
	    pycco tablero/*.py
	    pycco team/*.py
	    pycco us/*.py
  	    pycco usuario/*.py
        echo "Corriendo UnitTests..."
            python -m unittest TestInicioSesion.py
            python -m unittest TestProyecto.py
            python -m unittest TestRoles.py
            python -m unittest TestSprint.py
            python -m unittest TestTipoUserStory.py
            python -m unittest TestUsuario.py
            echo "Corriendo Servidor..."
            python3 manage.py collectstatic
            echo "Reiniciando httpd para correr el programa."
            /etc/init.d/apache2 restart
            break
            ;;
        "Salir")
            break
            ;;
        *) echo "Opción inválida";;
    esac
done

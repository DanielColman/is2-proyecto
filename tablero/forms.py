from django import forms
from .models import Tablero, Fase

# === Clase Tipo Formulario ===
class TableroForm(forms.ModelForm):
    """
    Campos de Clase Tipo Formulario:

    1. ** model **: Tablero o Flujo
    2. ** fields**: Se solicita el nombre y la descripcion como campo

    """
    class Meta:
        model = Tablero
        fields = ('nombre','descripcion',)

# === Clase Fase Formulario ===
class FaseForm(forms.ModelForm):
    """
    Campos de Clase Tipo Formulario:

    1. ** model **: Tablero o Flujo
    2. ** fields **: Se solicita el nombre y la descripcion como campo

    """
    class Meta:
        model = Fase
        fields = ('nombre','descripcion',)

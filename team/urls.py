from django.conf.urls import include, url
from . import views

"""
Las url para la administración de team
"""

urlpatterns = [
        url(r'^$', views.menu_team, name='menu_team'),
        url(r'^update_team/(?P<pk>[0-9]+)/$', views.update_team, name='update_team'),
        url(r'^update_team_member/(?P<pk>[0-9]+)/$', views.update_team_member, name='update_team_member'),
        url(r'^create/$', views.crear_teamMember, name='create_team_member'),
        url(r'^create_team/$', views.crear_team, name='create_team'),
]
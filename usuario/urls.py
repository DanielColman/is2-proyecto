from django.conf.urls import include, url
from . import views

"""
Las url para la administración de usuarios
"""

urlpatterns = [
        url(r'^$', views.list_usuario),
        url(r'^create/$', views.crear_usuario, name='create_usuario'),
        url(r'^update/(?P<pk>[0-9]+)/$', views.update_usuario, name='update_usuario'),
        url(r'^update/(?P<pk>[0-9]+)/confirmado.html$', views.confirmado, name='confirmado'),
        url(r'^create/confirmado.html$', views.confirmado2, name='confirmado'),
        url(r'^buscar_username/$', views.buscar_username, name='buscar_username'),
        url(r'^searchci/$', views.buscar_ci, name='buscar_ci'),
        url(r'^cambiar_passw.html$', views.cambiar_password, name='cambiar_password'),
        url(r'^confirmado.html$', views.confirmado2, name='confirmado'),
        url(r'^confirmado_usuario$', views.confirmado_usuario, name='confirmado_usuario'),
        url(r'^delete/(?P<pk>\d+)/$', views.eliminar_usuario, name='delete_usuario'),
        url(r'^delete/(?P<pk>[0-9]+)/confirmado.html$', views.confirmado, name='confirmado'),
        url(r'^delete/confirmado.html$', views.confirmado2, name='confirmado'),
        url(r'^visualizar/(?P<pk>[0-9]+)/$', views.list_usuario2, name='list_usuario2'),
        url(r'^visualizar_mio/$', views.visualisar_mio, name='visualizar_mio'),
]

from django.conf.urls import url
from . import views

"""
Lista de URLs del tablero
"""
urlpatterns = [
    url(r'^crear/(?P<pk>[0-9]+)/$', views.crear),
    url(r'^crearfase/(?P<pk>[0-9]+)/$', views.crear_fase, name='crear_fase'),
    url(r'^crearfasefinal/(?P<pk>[0-9]+)/$', views.crearfasefinal, name='crear_fase_final'),
    url(r'^continuar/(?P<pk>[0-9]+)/$', views.crear_fase2),
    url(r'^ver/(?P<pk>[0-9]+)/$', views.ver_tablero, name="tablero"),
    url(r'^modificar_fase/(?P<pk>[0-9]+)/$', views.modificar_fase, name='modificar_fase'),
    url(r'^modificar_tablero/(?P<pk>[0-9]+)/$', views.modificar_tablero, name='modificar_tablero'),
    url(r'^listo', views.listo, name='listo'),
    url(r'^importar/(?P<pk>[0-9]+)/$', views.importar),
    url(r'^importar2/(?P<pk>[0-9]+)/(?P<origen>[0-9]+)/$', views.importar2),
    url(r'^importado', views.importado, name='importado'),
]
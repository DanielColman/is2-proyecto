from django.conf.urls import include, url
from . import views





"""
Las url para proyecto
"""


urlpatterns = [
    url(r'^reporte_proyecto/(?P<id_proyecto>[0-9]+)/$', views.reporte_proyecto, name='reporte_proyecto'),
    url(r'^$', views.listar_proyectos),
    url(r'^create/$', views.crear_proyecto, name='create_proyecto'),
    url(r'^create/confirmado.html$', views.confirmado2, name='confirmado'),
    url(r'^update/(?P<id_proyecto>[0-9]+)/$', views.modificar_proyecto, name='update_proyecto'),
    url(r'^update/(?P<pk>[0-9]+)/confirmado.html$', views.confirmado, name='confirmado'),
    url(r'^update/(?P<pk>[0-9]+)/eliminado.html$', views.eliminado, name='eliminado'),
    url(r'^delete/(?P<id_proyecto>\d+)/$', views.eliminar_proyecto, name='delete_proyecto'),
    url(r'^delete/(?P<pk>[0-9]+)/confirmado.html$', views.confirmado, name='confirmado'),
    url(r'^eliminado.html$', views.eliminado2, name='delete'),
    url(r'^delete/confirmado.html$', views.confirmado2, name='confirmado'),
    url(r'^search/$', views.buscar_proyecto, name='buscar_proyecto'),
    url(r'^visualizar/(?P<id_proyecto>[0-9]+)/$', views.list_proyecto, name='list_proyecto'),
    url(r'^inactivar/(?P<id_proyecto>[0-9]+)/$', views.inactivar_proyecto, name='inactivar_proyecto'),
    url(r'^activar/(?P<id_proyecto>[0-9]+)/$', views.activar_proyecto, name='activar_proyecto'),
    url(r'^cancelar/(?P<id_proyecto>[0-9]+)/$', views.cancelar_proyecto, name='cancelar_proyecto'),
    url(r'^terminar/(?P<id_proyecto>[0-9]+)/$', views.terminar_proyecto, name='terminar_proyecto'),
    url(r'^inactivar/(?P<pk>[0-9]+)/confirmado.html$', views.confirmado, name='confirmado'),
    url(r'^trabajo/(?P<pk>[0-9]+)', views.trabajo, name='trabajo'),
    url(r'^burndown/(?P<id_sprint>[0-9]+)/$', views.burndown, name='burndown'),
    url(r'^elegirburndown/(?P<id_proyecto>[0-9]+)/$', views.elegirburndown, name='elegirburndown'),
    url(r'^elegirusuario/(?P<id_proyecto>[0-9]+)/$', views.elegirusuario, name='elegirusuario'),
    url(r'^reportetm/(?P<id_usuario>[0-9]+)/(?P<id_proyecto>[0-9]+)/$', views.reportetm, name='reportetm'),
    url(r'^elegirfecha/(?P<id_proyecto>[0-9]+)/$', views.elegirfecha, name='elegirfecha'),
]



#!/usr/bin/env bash

echo "add_usuario('daniel', 'danielcolman@outlook.com', 'proyecto', 'Daniel', 'Colman', '12345678', '021751438', 'Developer',
                'Mariano Roque Alonso', 8)"
echo "add_usuario('miriam', 'miriam@gmail.com', 'proyecto', 'Miriam', 'Godoy', '4841518', '021751439', 'Developer',
                'Asuncion', 7)"
echo "add_usuario('blas', 'blasmiranda@gmail.com', 'proyecto', 'Blas', 'Miranda', '4841519', '021751440', 'Developer',
                'Fernando de la Mora', 9)"
echo "add_usuario('lucia', 'lucia@gmail.com', 'proyecto', 'Lucia', 'Salinas', '4841617', '021752438', 'Developer',
                'San Lorenzo', 5)"
echo "add_usuario('noemi', 'noemi@gmail.com', 'proyecto', 'Noemi', 'Gauto', '690745', '0971180176', 'Developer',
                'Benjamin Aceval', 10)"
echo "add_usuario('jazmin', 'jazmin@gmail.com', 'proyecto', 'Jazmin', 'Insfran', '4288835', '021640692', 'Developer',
                'America 163', 15)"
echo "add_usuario('joel', 'joel@gmail.com', 'proyecto', 'Joel', 'Acosta', '4258632', '02168953', 'Developer',
                'Tuyutti', 20)"
echo "add_usuario('lucas', 'lucas@gmail.com', 'proyecto', 'Lucas', 'Gamarra', '848851', '0971790393', 'Developer',
                'las mercedes', 20)"
echo "add_usuario('emilio', 'emilio@gmail.com', 'proyecto', 'Emilio', 'Paiva', '4288832', '0978962104', 'Developer',
                'los laureles', 8)"
echo "add_usuario('arami', 'arami@gmail.com', 'proyecto', 'Arami', 'Leguizamon', '759883', '02185632', 'Developer',
                'carmelitas', 30)"


echo "add_proyecto('a iniciar', None, None, None, None, None, 'PEN', [('daniel','scrum')])
    add_us('s1', 'User Story 1', 50, 8, 7, 9, 8, '2019-05-23 15:40:39', None, None, 0, None, 'a iniciar', 1,
           0)"
echo "add_us('s2', 'User Story 2', 30, 3, 4, 5, 2, '2019-05-23 15:40:39', None, None, 0, None, 'a iniciar', 1,
           0)"
echo "add_us('s3', 'User Story 3', 60, 3, 4, 5, 2, '2019-05-23 15:40:39', None, None, 0, None, 'a iniciar', 1,
           0)"

  echo " add_permiso(1,\"Consultar Usuarios del Proyecto\")"
  echo "add_permiso(2,\"Crear Rol\")" 
  echo "add_permiso(3,\"Modificar Rol\")" 
  echo "add_permiso(4,\"Buscar Rol\")" 
  echo "add_permiso(5,\"Consultar Rol\")"  
  echo "add_permiso(6,\"Modificar Proyecto\")"  
  echo "add_permiso(7,\"Inactivar Proyecto\")"  
  echo " add_permiso(8,\"Consultar Proyecto\")" 


echo "bddev se cargó exitosamente."
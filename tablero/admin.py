from django.contrib import admin
from .models import Fase, Tablero, Estado

admin.site.register(Tablero)
admin.site.register(Fase)
admin.site.register(Estado)
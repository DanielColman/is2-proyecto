from django.db import models

# Create your models here.
ESTADOS = (
    ('PEN', 'Pendiente'), # cuando se crea
    ('ACT', 'Activo'), # cuando se inicia
    ('CAN','Cancelado'), # cuando se cancela
    ('TER', 'Terminado'), # cuando se aprueba uno finalizado
)

"""Definicion del modelo Proyecto"""
class Proyecto(models.Model):
    nombre = models.CharField(max_length=50, verbose_name='nombre del proyecto', unique=True)
    fecha_ini = models.DateField(verbose_name='fecha de inicio')
    fecha_fin = models.DateField(verbose_name='fecha de finalizacion')
    observaciones = models.TextField(verbose_name='observaciones')
    estado = models.CharField(max_length=3, choices=ESTADOS, default='PEN')

    def __str__(self):
        return self.nombre
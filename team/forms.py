from team.models import *
from django import forms
#from django.forms import CharField, Form

"""
  Formulario para anhadir datos de un team member
  """
class TeamMemberForm(forms.ModelForm):
    class Meta:
        model = TeamMember
        fields = [
            'id_usuario',
            'id_rol',
        ]
        labels = {
            'id_usuario': 'Usuario',
            'id_rol': 'Rol',
        }
        widgets = {
        'id_usuario': forms.RadioSelect(),
        'id_rol': forms.RadioSelect(),
        }



class TeamForm(forms.ModelForm):
    class Meta:
        model = Team
        fields = [
            'id_proyecto',
            'id_team_member',
        ]
        labels = {
            'id_proyecto': 'Proyecto',
            'id_team_member': 'Team',
        }
        widgets = {
        'id_proyecto': forms.RadioSelect(),
        'id_team_member': forms.CheckboxSelectMultiple(),
        }
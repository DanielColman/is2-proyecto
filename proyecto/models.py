from django.db import models


ESTADOS = (
    ('PEN', 'Pendiente'), # cuando se crea
    ('ACT', 'Activo'), # cuando se inicia
    ('CAN','Cancelado'), # cuando se cancela
    ('TER', 'Terminado'), # cuando se aprueba uno finalizado
    ('INA', 'Inactivo'), # cuando se inactiva el proyecto
)

"""Definicion del modelo Proyecto"""
class Proyecto(models.Model):
    nombre = models.CharField(max_length=50, verbose_name='nombre del proyecto', unique=True)
    fecha_ini = models.DateField(verbose_name='fecha de inicio', null=True, blank=True)
    fecha_ini_est = models.DateField(verbose_name='fecha de inicio estimada', null=True, blank=True)
    fecha_fin = models.DateField(verbose_name='fecha de finalizacion', null=True, blank=True)
    fecha_fin_est = models.DateField(verbose_name='fecha de finalizacion estimada', null=True, blank=True)
    observaciones = models.TextField(verbose_name='observaciones', null=True, blank=True)
    estado = models.CharField(max_length=3, choices=ESTADOS, default='PEN')

    def __str__(self):
        return self.nombre
from unittest import TestCase
import django
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "GestorP.base")
django.setup()
from us.models import Us
from django.test import Client

"""
Prubas unitarias para la app us
"""

class UserStroy(TestCase):
    def test_pagina(self):
        """
        Carga la página y verifica que encuentra
        """
        client = Client()
        response = client.get("/us/")
        #HTTP_302_FOUND
        self.assertEqual(response.status_code, 404)

    def test_create_us(self):
        """
        Verifica que un us se crea
        """
    try:
        descripcion_breve = 'descripcion_bree'
        descripcion_completa = 'descripcion_completa'
        tiempo_finalizacion = 1
        valor_negocio = 1
        prioridad = 1
        valor_tecnico = 1
        fecha_creacion = '2018-01-01'
        #valor_tecnico = 'cadena'
        us_nuevo = Us(descripcion_breve=descripcion_breve, descripcion_completa=descripcion_completa,tiempo_finalizacion=tiempo_finalizacion,
                      valor_negocio=valor_negocio, prioridad=prioridad,valor_tecnico=valor_tecnico,
                      fecha_creacion=fecha_creacion)
        us_nuevo.save()

    except:
        us_nuevo.delete()
        raise

    def test_create_us2(self):
        """
        Verifica que un us se crea
        """
    try:
        descripcion_breve = 'descripcion_bree'
        descripcion_completa = 'descripcion_completa'
        tiempo_finalizacion = 1
        valor_negocio = 1
        prioridad = 1
        valor_tecnico = 1
        fecha_creacion = '2018-01-01'
        #valor_tecnico = 'cadena'
        us_nuevo = Us(descripcion_breve=descripcion_breve, descripcion_completa=descripcion_completa,tiempo_finalizacion=tiempo_finalizacion,
                      valor_negocio=valor_negocio, prioridad=prioridad,valor_tecnico=valor_tecnico,
                      fecha_creacion=fecha_creacion)
        us_nuevo.save()

    except:
        us_nuevo.delete()
        raise

    def test_create_us3(self):
        """
        Verifica que un us se crea
        """
    try:
        descripcion_breve = 'descripcion_bree'
        descripcion_completa = 'descripcion_completa'
        tiempo_finalizacion = 1
        valor_negocio = 1
        prioridad = 1
        valor_tecnico = 1
        fecha_creacion = '2018-01-01'
        #valor_tecnico = 'cadena'
        us_nuevo = Us(descripcion_breve=descripcion_breve, descripcion_completa=descripcion_completa,tiempo_finalizacion=tiempo_finalizacion,
                      valor_negocio=valor_negocio, prioridad=prioridad,valor_tecnico=valor_tecnico,
                      fecha_creacion=fecha_creacion)
        us_nuevo.save()

    except:
        us_nuevo.delete()
        raise

    def test_update_us(self):
        """
        Verifica que un us se modifica
        """
    try:
        descripcion_breve = 'descripcion_breve'
        descripcion_completa = 'descripcion_completa'
        tiempo_finalizacion = 1
        valor_negocio = 2
        prioridad = 2
        valor_tecnico = 2
        us_nuevo = Us(descripcion_breve=descripcion_breve, descripcion_completa=descripcion_completa,
                      tiempo_finalizacion=tiempo_finalizacion,
                      valor_negocio=valor_negocio, prioridad=prioridad, valor_tecnico=valor_tecnico)
        us_nuevo.save()
        us_nuevo2 = Us(descripcion_breve='descripcion_breve2', descripcion_completa='descripcion_completa2',
                       tiempo_finalizacion=2,
                       valor_negocio=3, prioridad=4, valor_tecnico=5)
        us_nuevo.save()
        us_nuevo2.save()
        # No falla (siguiente 7 lineas):
        us_nuevo2.descripcion_breve = 'descripcion_breve3'
        us_nuevo2.descripcion_completa= 'descripcion_completa3'
        us_nuevo2.tiempo_finalizacion= 3
        us_nuevo2.valor_negocio=4
        us_nuevo2.prioridad = 5
        us_nuevo2.valor_tecnico =6
        #us_nuevo2.valor_tecnico = 'cadena'
        us_nuevo2.save()
    except:
        us_nuevo.delete()
        us_nuevo2.delete()
        raise

    def test_delete_us(self):
        """
        Verifica que un us se elimina sin problemas
        """
        try:
            descripcion_breve = 'descripcion_breve'
            descripcion_completa = 'descripcion_completa'
            tiempo_finalizacion = 1
            valor_negocio = 1
            prioridad = 1
            valor_tecnico = 1
            fecha_creacion = '2018-01-01'
            us_nuevo =Us(descripcion_breve=descripcion_breve, descripcion_completa=descripcion_completa,tiempo_finalizacion=tiempo_finalizacion,
                      valor_negocio=valor_negocio, prioridad=prioridad,valor_tecnico=valor_tecnico,
                      fecha_creacion=fecha_creacion)
            us_nuevo.save()
            us_nuevo.delete()
            #Falla(intenta borrar algo que no existe,(descomentar linea de abajo)
            #us_nuevo.delete()
        except:
            raise

    def test_delete_us2(self):
            """
            Verifica que un us se elimina sin problemas
            """
            try:
                descripcion_breve = 'descripcion_breve'
                descripcion_completa = 'descripcion_completa'
                tiempo_finalizacion = 1
                valor_negocio = 1
                prioridad = 1
                valor_tecnico = 1
                fecha_creacion = '2018-01-01'
                us_nuevo = Us(descripcion_breve=descripcion_breve, descripcion_completa=descripcion_completa,
                              tiempo_finalizacion=tiempo_finalizacion,
                              valor_negocio=valor_negocio, prioridad=prioridad, valor_tecnico=valor_tecnico,
                              fecha_creacion=fecha_creacion)
                us_nuevo.save()
                us_nuevo.delete()
                us_nuevo.delete()
            except:
                raise

    def test_delete_us3(self):
            """
            Verifica que un us se elimina sin problemas
            """
            try:
                descripcion_breve = 'descripcion_breve'
                descripcion_completa = 'descripcion_completa'
                tiempo_finalizacion = 1
                valor_negocio = 1
                prioridad = 1
                valor_tecnico = 1
                fecha_creacion = '2018-01-01'
                us_nuevo = Us(descripcion_breve=descripcion_breve, descripcion_completa=descripcion_completa,
                              tiempo_finalizacion=tiempo_finalizacion,
                              valor_negocio=valor_negocio, prioridad=prioridad, valor_tecnico=valor_tecnico,
                              fecha_creacion=fecha_creacion)
                us_nuevo.save()
                us_nuevo.delete()
                us_nuevo.delete()
            except:
                raise

    def test_delete_us4(self):
        """
        Verifica que un us se elimina sin problemas
        """
        try:
            descripcion_breve = 'descripcion_breve'
            descripcion_completa = 'descripcion_completa'
            tiempo_finalizacion = 1
            valor_negocio = 1
            prioridad = 1
            valor_tecnico = 1
            fecha_creacion = '2018-01-01'
            us_nuevo =Us(descripcion_breve=descripcion_breve, descripcion_completa=descripcion_completa,tiempo_finalizacion=tiempo_finalizacion,
                      valor_negocio=valor_negocio, prioridad=prioridad,valor_tecnico=valor_tecnico,
                      fecha_creacion=fecha_creacion)
            us_nuevo.save()
            us_nuevo.delete()
        except:
            raise

    def test_delete_us5(self):
            """
            Verifica que un us se elimina sin problemas
            """
            try:
                descripcion_breve = 'descripcion_breve'
                descripcion_completa = 'descripcion_completa'
                tiempo_finalizacion = 1
                valor_negocio = 1
                prioridad = 1
                valor_tecnico = 1
                fecha_creacion = '2018-01-01'
                us_nuevo = Us(descripcion_breve=descripcion_breve, descripcion_completa=descripcion_completa,
                              tiempo_finalizacion=tiempo_finalizacion,
                              valor_negocio=valor_negocio, prioridad=prioridad, valor_tecnico=valor_tecnico,
                              fecha_creacion=fecha_creacion)
                us_nuevo.save()
                us_nuevo.delete()
                us_nuevo.delete()
            except:
                raise

    def test_update_us2(self):
        """
        Verifica que un us se modifica
        """
    try:
        descripcion_breve = 'descripcion_breve'
        descripcion_completa = 'descripcion_completa'
        tiempo_finalizacion = 1
        valor_negocio = 2
        prioridad = 2
        valor_tecnico = 2
        us_nuevo = Us(descripcion_breve=descripcion_breve, descripcion_completa=descripcion_completa,
                      tiempo_finalizacion=tiempo_finalizacion,
                      valor_negocio=valor_negocio, prioridad=prioridad, valor_tecnico=valor_tecnico)
        us_nuevo.save()
        us_nuevo2 = Us(descripcion_breve='descripcion_breve2', descripcion_completa='descripcion_completa2',
                       tiempo_finalizacion=2,
                       valor_negocio=3, prioridad=4, valor_tecnico=5)
        us_nuevo.save()
        us_nuevo2.save()
        # No falla (siguiente 7 lineas):
        us_nuevo2.descripcion_breve = 'descripcion_breve3'
        us_nuevo2.descripcion_completa= 'descripcion_completa3'
        us_nuevo2.tiempo_finalizacion= 3
        us_nuevo2.valor_negocio=4
        us_nuevo2.prioridad = 5
        us_nuevo2.valor_tecnico =6
        #us_nuevo2.valor_tecnico = 'cadena'
        us_nuevo2.save()
    except:
        us_nuevo.delete()
        us_nuevo2.delete()
        raise


    def test_update_us3(self):
        """
        Verifica que un us se modifica
        """
    try:
        descripcion_breve = 'descripcion_breve'
        descripcion_completa = 'descripcion_completa'
        tiempo_finalizacion = 1
        valor_negocio = 2
        prioridad = 2
        valor_tecnico = 2
        us_nuevo = Us(descripcion_breve=descripcion_breve, descripcion_completa=descripcion_completa,
                      tiempo_finalizacion=tiempo_finalizacion,
                      valor_negocio=valor_negocio, prioridad=prioridad, valor_tecnico=valor_tecnico)
        us_nuevo.save()
        us_nuevo2 = Us(descripcion_breve='descripcion_breve2', descripcion_completa='descripcion_completa2',
                       tiempo_finalizacion=2,
                       valor_negocio=3, prioridad=4, valor_tecnico=5)
        us_nuevo.save()
        us_nuevo2.save()
        # No falla (siguiente 7 lineas):
        us_nuevo2.descripcion_breve = 'descripcion_breve3'
        us_nuevo2.descripcion_completa= 'descripcion_completa3'
        us_nuevo2.tiempo_finalizacion= 3
        us_nuevo2.valor_negocio=4
        us_nuevo2.prioridad = 5
        us_nuevo2.valor_tecnico =6
        #us_nuevo2.valor_tecnico = 'cadena'
        us_nuevo2.save()
    except:
        us_nuevo.delete()
        us_nuevo2.delete()
        raise

    def test_update_us4(self):
        """
        Verifica que un us se modifica
        """
    try:
        descripcion_breve = 'descripcion_breve'
        descripcion_completa = 'descripcion_completa'
        tiempo_finalizacion = 1
        valor_negocio = 2
        prioridad = 2
        valor_tecnico = 2
        us_nuevo = Us(descripcion_breve=descripcion_breve, descripcion_completa=descripcion_completa,
                      tiempo_finalizacion=tiempo_finalizacion,
                      valor_negocio=valor_negocio, prioridad=prioridad, valor_tecnico=valor_tecnico)
        us_nuevo.save()
        us_nuevo2 = Us(descripcion_breve='descripcion_breve2', descripcion_completa='descripcion_completa2',
                       tiempo_finalizacion=2,
                       valor_negocio=3, prioridad=4, valor_tecnico=5)
        us_nuevo.save()
        us_nuevo2.save()
        # No falla (siguiente 7 lineas):
        us_nuevo2.descripcion_breve = 'descripcion_breve3'
        us_nuevo2.descripcion_completa= 'descripcion_completa3'
        us_nuevo2.tiempo_finalizacion= 3
        us_nuevo2.valor_negocio=4
        us_nuevo2.prioridad = 5
        us_nuevo2.valor_tecnico =6
        #us_nuevo2.valor_tecnico = 'cadena'
        us_nuevo2.save()
    except:
        us_nuevo.delete()
        us_nuevo2.delete()
        raise

    def test_update_us5(self):
        """
        Verifica que un us se modifica
        """
    try:
        descripcion_breve = 'descripcion_breve'
        descripcion_completa = 'descripcion_completa'
        tiempo_finalizacion = 1
        valor_negocio = 2
        prioridad = 2
        valor_tecnico = 2
        us_nuevo = Us(descripcion_breve=descripcion_breve, descripcion_completa=descripcion_completa,
                      tiempo_finalizacion=tiempo_finalizacion,
                      valor_negocio=valor_negocio, prioridad=prioridad, valor_tecnico=valor_tecnico)
        us_nuevo.save()
        us_nuevo2 = Us(descripcion_breve='descripcion_breve2', descripcion_completa='descripcion_completa2',
                       tiempo_finalizacion=2,
                       valor_negocio=3, prioridad=4, valor_tecnico=5)
        us_nuevo.save()
        us_nuevo2.save()
        # No falla (siguiente 7 lineas):
        us_nuevo2.descripcion_breve = 'descripcion_breve3'
        us_nuevo2.descripcion_completa= 'descripcion_completa3'
        us_nuevo2.tiempo_finalizacion= 3
        us_nuevo2.valor_negocio=4
        us_nuevo2.prioridad = 5
        us_nuevo2.valor_tecnico =6
        #us_nuevo2.valor_tecnico = 'cadena'
        us_nuevo2.save()
    except:
        us_nuevo.delete()
        us_nuevo2.delete()
        raise
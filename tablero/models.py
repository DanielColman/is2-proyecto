from django.db import models
from proyecto.models import Proyecto

# === Modelos de Tipos ===
class Tablero(models.Model):

    """
    Campos del modelo:

    1. ** nombre **:describe el nombre del Tipo de US
    2. ** descripcion **: Se solicita una descripcion al Tipo de US
    3. ** proyecto **: Clave foranea, establece el 1 a muchos, y restriccion de eliminacion Nunca!

    """
    nombre = models.CharField(max_length=50)
    descripcion = models.TextField()
    proyecto = models.ForeignKey(Proyecto, on_delete=models.DO_NOTHING, null=True)


    def __str__(self):
        return self.nombre

# === Modelos de Fases ===
class Fase(models.Model):

    """
    Campos del modelo:

    1. ** nombre **: describe el nombre de la Fase
    2. ** descripcion **: Se solicita una descripcion a la Fase
    3. ** tipo **: Clave foranea y restriccion de eliminacion en cascada

    """
    nombre = models.CharField(max_length=50)
    descripcion = models.TextField()
    tablero = models.ForeignKey(Tablero, on_delete=models.CASCADE, null=True)
    orden = models.IntegerField(default=0)

    def __str__(self):
        return self.nombre

class Estado(models.Model):
    """
     Campos del modelo:

     1. ** nombre **: describe el nombre del Tipo de US
     2. ** descripcion **: Se solicita una descripcion del Tipo de US

     """


    nombre = models.CharField(max_length=50)
    descripcion = models.TextField()
    orden = models.IntegerField(default=0)

    def __str__(self):
        return self.nombre




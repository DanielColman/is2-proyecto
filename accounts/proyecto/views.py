from django.contrib import messages
from django.contrib.auth.decorators import login_required

from django.db import transaction
from django.db.models import Q
from django.shortcuts import render, redirect
from .models import Proyecto
from proyecto.forms import CreateProyectoForm
from proyecto.forms import UpdateProyectoForm
from proyecto.forms import DisableProyectoForm
from proyecto.forms import BuscarProyecto


from django.http import HttpResponse

# Create your views here.



def index(request):
    return HttpResponse("<h1> PROYECTOS</h1>")

#@login_required
@transaction.atomic
def crear_proyecto(request):
    """
    Esta view permite crear un User
    :param request:
    """
    if request.method == "POST":
        form = CreateProyectoForm(request.POST)
        if form.is_valid():
            nombre = form.cleaned_data['nombre']
            fecha_ini = form.cleaned_data['fecha_ini']
            fecha_fin = form.cleaned_data['fecha_fin']
            observaciones = form.cleaned_data['observaciones']
            proyectoNvo = Proyecto(nombre=nombre, fecha_ini=fecha_ini, fecha_fin=fecha_fin, observaciones=observaciones)
            proyectoNvo.save()
            messages.success(request, ('Se creó el proyecto seleccionado!'))

    else:
        form = CreateProyectoForm()
    return render(request, 'proyecto/create_proyecto.html', {'form': form})


def listar_proyectos(request):
    """
    Esta view permite listar los Proyectos
    :param request:
    """
    proyectos = Proyecto.objects.all()
    return render(request, 'proyecto/listar_proyectos.html', {'proyectos': proyectos})


@transaction.atomic
def modificar_proyecto(request, id_proyecto):
    """
    Esta view permite modificar un proyecto
    :param request:
    """
    proyecto = Proyecto.objects.get(id=id_proyecto)
    if request.method == 'POST' and 'save' in request.POST:
        # formulario enviado
        proyecto_form = UpdateProyectoForm(request.POST, instance=proyecto)
        if proyecto_form.is_valid():
            # formulario validado correctamente
            proyecto_form.save()
            if proyecto_form.has_changed():
                messages.success(request, ('Se modifico el proyecto seleccionado!'))
            if proyecto.estado == 'CAN' or proyecto.estado == 'TER':
                proyecto_form = DisableProyectoForm(instance=proyecto)
    elif request.method == 'POST' and 'eliminar' in request.POST:
        # borrar proyecto
        proyecto_form = UpdateProyectoForm(request.POST, instance=proyecto)
        proyecto.delete()
        return redirect('eliminado.html')
    else:
        # formulario inicial
        if proyecto.estado == 'CAN' or proyecto.estado == 'TER':
            proyecto_form = DisableProyectoForm(instance=proyecto)

        else:
            proyecto_form = UpdateProyectoForm(instance=proyecto)
    return render(request, 'proyecto/update_proyecto.html', {'proyecto_form': proyecto_form})

@login_required
@transaction.atomic
def eliminado (request,pk):
    """
    Página que confirma que un proceso fue realizado con éxito
    :param request:
    :param pk:
    """
    return render(request, 'proyecto/eliminado.html')

@login_required
@transaction.atomic
def eliminado2 (request):
    """
    Página que confirma que un proceso fue realizado con éxito
    :param request:
    """
    return render(request, 'proyecto/eliminado.html')

@login_required
@transaction.atomic
def buscar_proyecto(request):
    """
    Buscar un Proyecto por nombre
    :param request:
    """
    if request.method == 'POST':
        busqueda = request.POST['buscalo']
        """
        Filtra de acuerdo a la palabra que se indico 
        :param request:
        """
        if busqueda:
            match = Proyecto.objects.filter(Q(nombre__contains=busqueda))
            if match:
                return render(request, 'proyecto/buscar_proyecto.html', {'sr': match})
            else:
                messages.error(request, 'No existe un proyecto con ese nombre')
        else:
            return redirect("/proyecto/buscar_proyecto/")

    return render(request, 'proyecto/buscar_proyecto.html')

@login_required
@transaction.atomic
def confirmado (request,pk):
    """
    Página que confirma que un proceso fue realizado con éxito
    :param request:
    :param pk:
    """
    return render(request, 'proyecto/confirmado.html')



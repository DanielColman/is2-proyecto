import os
import django
import unittest

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "GestorP.base")
django.setup()

from django.test import Client
from django.contrib.auth.models import User

class Prueba(unittest.TestCase):
    def test_pagina(self):
        client = Client()
        response = client.get("/accounts/login/")
        self.assertEqual(response.status_code, 200)

    def test_login(self):
        user = User.objects.create(username='test')
        user.set_password('prueba')
        user.save()
        client = Client()
        try:
            self.assertEqual(client.login(username='test', password='prueba'), True)
            #self.assertEqual(client.login(username='ejemplo', password='no'), True)
            user.delete()
        except AssertionError:
            user.delete()
            raise

# Generated by Django 2.0.4 on 2018-05-10 13:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('proyecto', '0001_initial'),
        ('team', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Horas',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('horas_trabajadas', models.IntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Reunion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('reunion', models.TextField()),
            ],
            options={
                'verbose_name_plural': 'Reuniones',
            },
        ),
        migrations.CreateModel(
            name='Sprint',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('estado', models.CharField(choices=[('ACT', 'Activo'), ('TER', 'Terminado')], default='ACT', max_length=3)),
                ('fecha_ini', models.DateField(blank=True, null=True, verbose_name='fecha de inicio')),
                ('fecha_fin', models.DateField(blank=True, null=True, verbose_name='fecha de finalizacion')),
                ('fecha_fin_est', models.DateField(blank=True, null=True, verbose_name='fecha de finalizacion estimada')),
                ('proyecto', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='proyecto.Proyecto')),
                ('team_sprint', models.ManyToManyField(through='sprint.Horas', to='team.TeamMember')),
            ],
        ),
        migrations.AddField(
            model_name='reunion',
            name='sprint',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='sprint.Sprint'),
        ),
        migrations.AddField(
            model_name='horas',
            name='sprint',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='sprint.Sprint'),
        ),
        migrations.AddField(
            model_name='horas',
            name='teammember',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='team.TeamMember'),
        ),
    ]

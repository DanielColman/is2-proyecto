from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponseRedirect
from django.contrib.auth import login, forms
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.contrib import messages
from django.contrib.auth import logout
from django.shortcuts import redirect
from django.urls import reverse


"""
Vista del Login
"""

def home(request):
    """
    Vista del login
    :param request:
    """
    return render(request, 'accounts/login.html')


def post(request):
        """
        Autenticacion del user con su password, y logueo
        :param request:
        """
        user = User.objects.get(username__exact=request.user.username)
        #if user is None:
        #    messages.error(request, 'Nombre de usuario o contraseña incorrectos')
        #    return redirect('/accounts/login')
        # if user.is_superuser:
        #     return redirect('/menu/admin')
        # else:
        #     return redirect('/menu/usuario')
        return redirect('/menu')

        #username = request.POST['username']
        #password = request.POST['password']
        #user = authenticate(request, username=username, password=password)
        #if user is not None:
            #login(request, user)
            #if User.objects.get(username__exact=request.user.username).is_superuser():
                #return redirect('accounts:profile')
             #   return redirect('/menu/menu_admin.html')
            #else:
            #    return redirect('/menu/menu_usuario.html')

        #else:
            #return HttpResponse("NO SE HA LOGUEADO")


def user_logout(request):
        logout(request)
        return redirect('/accounts/login')

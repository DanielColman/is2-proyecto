from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User
from usuario.models import Usuario

"""
Modifica Admin de Users de Django
"""

class UsuarioInline(admin.StackedInline):
    """
    Definimos cual sera el modelo que vamos a añadir a User
    """
    model = Usuario
    can_delete = False
    verbose_name_plural = 'usuarios'

class UserAdmin(BaseUserAdmin):
    """
    Modificamos el administrador de usuarios de Django para que
    use los nuevos campos del modelo "Usuario"
    """
    inlines = (UsuarioInline,)

admin.site.unregister(User)
admin.site.register(User, UserAdmin)

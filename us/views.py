from datetime import date, datetime, timedelta

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.db import transaction
from django.db.models import Q
from django.shortcuts import render, redirect
from django.core.mail import EmailMessage
from django.http import HttpResponseForbidden

from proyecto.models import Proyecto
from sprint.models import Sprint, Horas
from tablero.models import Tablero, Fase
from team.models import Team, TeamMember
from us.models import Us, Nota, Archivo, Daily
from .forms import ArchivoForm, UsForm, ModificarUsForm, NotaForm, DailyForm
from django.contrib import messages

"""Las views para la administración de User Stories
Un User Story(US) es la unidad mínima de tarea de un Proyecto.
"""
@login_required
@transaction.atomic
def product_backlog(request, pk):
    """
    El Product Backlog es donde se encuentran los User Stories que aún no
    han sido asignados dentro del tablero.
    Esta View permite visualizar el Product Backlog con sus User Stories dentro.
    :param request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    :param pk: es el ID del proyecto en donde se encuentra el Product Backlog
    :return: el template donde se visualiza el Product Backlog
    """

    """User Stories ordenados por prioridad"""
    proyecto = Proyecto.objects.get(id=pk)
    uss = Us.objects.filter(fase=None, proyecto=proyecto).order_by('-prioridad_promedio')
    uss = list(uss)
    #Completar uss con los que están en un sprint y no aprobaron el control de calidad
    us_no_aprob = Us.objects.filter(aprobado=2, proyecto=proyecto)
    us_no_aprob = list(us_no_aprob)
    if us_no_aprob:
        for us_no in us_no_aprob:
            us_no.valor_negocio=10
            us_no.prioridad=10
            us_no.valor_tecnico=10
            us_no.prioridad_promedio=10
            us_no.save()

    us_no_llegaron = Us.objects.filter(aprobado=3, proyecto=proyecto)
    us_no_llegaron = list(us_no_llegaron)
    if us_no_llegaron:
        for us_no_l in us_no_llegaron:
            us_no_l.valor_negocio = 10
            us_no_l.prioridad = 10
            us_no_l.valor_tecnico = 10
            us_no_l.prioridad_promedio = 10
            us_no_l.save()
    teammember = TeamMember.objects.filter(id_usuario=request.user.usuario.id)
    teammember = list(teammember)
    permitido = False
    is_scrum = False
    us_proyecto = []
    rol_usuario = []
    try:
        for i in range(len(teammember)):
            try:
                team = Team.objects.get(id_team_member=teammember[i].id)
            except:
                continue
            if team.id_proyecto_id == proyecto.id:
                permitido = True
                if teammember[i].id_rol.nombre == 'scrum':
                    is_scrum = True
    except:
        pass

    super = False
    if request.user.is_superuser:
        super = True
        permitido = True

    if permitido:
        return render(request, 'us/product_backlog.html',{'uss':uss, 'is_scrum':is_scrum, 'proyecto':proyecto, 'us_no_aprob':us_no_aprob, 'us_no_llegaron':us_no_llegaron} )
    else:
        return HttpResponseForbidden()

@login_required
@transaction.atomic
def product_backlog_para_elegir(request, pk):
    """
    El Product Backlog es donde se encuentran los User Stories que aún no
    han sido asignados dentro del tablero.
    Esta View permite visualizar el Product Backlog con sus User Stories dentro.
    :param request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    :param pk: es el ID del proyecto en donde se encuentra el Product Backlog
    :return: el template donde se visualiza el Product Backlog
    """

    """User Stories ordenados por prioridad"""
    proyecto = Proyecto.objects.get(id=pk)
    uss = Us.objects.filter(fase=None, proyecto=proyecto).order_by('-prioridad_promedio')
    uss = list(uss)
    #Completar uss con los que están en un sprint y no aprobaron el control de calidad
    us_no_aprob = Us.objects.filter(aprobado=2, proyecto=proyecto)
    us_no_aprob = list(us_no_aprob)
    if us_no_aprob:
        for us_no in us_no_aprob:
            us_no.valor_negocio = 10
            us_no.prioridad = 10
            us_no.valor_tecnico = 10
            us_no.prioridad_promedio = 10
            us_no.save()

    us_no_llegaron = Us.objects.filter(aprobado=3, proyecto=proyecto)
    us_no_llegaron = list(us_no_llegaron)
    if us_no_llegaron:
        for us_no_l in us_no_llegaron:
            us_no_l.valor_negocio = 10
            us_no_l.prioridad = 10
            us_no_l.valor_tecnico = 10
            us_no_l.prioridad_promedio = 10
            us_no_l.save()

    teammember = TeamMember.objects.filter(id_usuario=request.user.usuario.id)
    teammember = list(teammember)
    permitido = False
    is_scrum = False
    us_proyecto = []
    rol_usuario = []
    try:
        for i in range(len(teammember)):
            try:
                team = Team.objects.get(id_team_member=teammember[i].id)
            except:
                continue
            if team.id_proyecto_id == proyecto.id:
                permitido = True
                if teammember[i].id_rol.nombre == 'scrum':
                    is_scrum = True
    except:
        pass

    super = False
    if request.user.is_superuser:
        super = True
        permitido = True

    if permitido:
        return render(request, 'us/product_backlog_para_elegir.html',{'uss':uss, 'is_scrum':is_scrum, 'proyecto':proyecto, 'us_no_aprob':us_no_aprob, 'us_no_llegaron':us_no_llegaron} )
        return HttpResponseForbidden()

def daterange(start_date, end_date):
    """
        Este método cuenta la cantidad de días entre dos fechas
        :param start_date: fecha inicial
        :param end_date: fecha final
    """
    for n in range(int ((end_date - start_date).days)):
        yield start_date + timedelta(n)

@login_required
@transaction.atomic
def sprint_backlog_para_elegir(request, pk):
    """
    Esta View permite visualizar los US seleccionados para que formen parte del Sprint
    al momento de iniciar el Sprint
    :param request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    :param pk: es el ID del proyecto en donde se encuentra el Product Backlog
    :return: el template donde se visualiza el Sprint
    """
    proyecto = Proyecto.objects.get(id=pk)

    teammember = TeamMember.objects.filter(id_usuario=request.user.usuario.id)
    teammember = list(teammember)
    permitido = False
    is_scrum = False
    us_proyecto = []
    rol_usuario = []
    try:
        for i in range(len(teammember)):
            try:
                team = Team.objects.get(id_team_member=teammember[i].id)
            except:
                continue
            if team.id_proyecto_id == proyecto.id:
                if teammember[i].id_rol.nombre == 'scrum':
                    is_scrum = True
                    permitido = True
    except:
        return HttpResponseForbidden()
    if permitido:
        sprint = Sprint.objects.get(estado='ACT', proyecto=proyecto)
        usa = Us.objects.filter(proyecto=proyecto, sprint=sprint)
        us_lista = list(usa)

        fecha_fin = sprint.fecha_fin_est
        fecha_hoy = date.today()
        cant_dias = fecha_fin - fecha_hoy + timedelta(days=1)
        fecha_fin = fecha_fin + timedelta(days=1)
        """fechas de feriados del anho 2018"""
        feriados = [datetime.strptime('2018-01-01', '%Y-%m-%d').date(),
                    datetime.strptime('2018-02-26', '%Y-%m-%d').date(),
                    datetime.strptime('2018-03-29', '%Y-%m-%d').date(),
                    datetime.strptime('2018-03-30', '%Y-%m-%d').date(),
                    datetime.strptime('2018-05-01', '%Y-%m-%d').date(),
                    datetime.strptime('2018-05-14', '%Y-%m-%d').date(),
                    datetime.strptime('2018-05-15', '%Y-%m-%d').date(),
                    datetime.strptime('2018-06-11', '%Y-%m-%d').date(),
                    datetime.strptime('2018-08-15', '%Y-%m-%d').date(),
                    datetime.strptime('2018-09-29', '%Y-%m-%d').date(),
                    datetime.strptime('2018-12-08', '%Y-%m-%d').date(),
                    datetime.strptime('2018-12-25', '%Y-%m-%d').date()]
        for fecha in daterange(fecha_hoy, fecha_fin):
            if fecha.weekday()==6 or fecha in feriados:
                """ No se toma en consideracion los domingos y feriados"""
                cant_dias = cant_dias - timedelta(days=1)
                """ No se toma en consideracion aquellos Us que no se encuentran aun incluidos en el Sprint"""
        if (cant_dias >= timedelta(days=1)):
            cant_dias = str(cant_dias).split('d')[0]
            cant_dias = int(cant_dias)
        """ Se obtiene las horas trabajadas del Sprint al cual pertenece el proyecto"""
        horas = Horas.objects.filter(sprint = sprint)
        cant_horas = 0
        for hora in horas:
            cant_horas = cant_horas + hora.horas_trabajadas
        cant_horas = cant_horas * cant_dias
        cant_horas_gastadas = cant_horas
        for us_lista in us_lista:
                cant_horas_gastadas = cant_horas_gastadas - us_lista.tiempo_finalizacion
        cant_horas_restantes = cant_horas - cant_horas_gastadas
        uss = Us.objects.filter(sprint=sprint)
        """Mensaje para imprimir la capacidad el Sprint"""
        if (cant_horas > cant_horas_restantes):
            mensaje = str(cant_horas - cant_horas_restantes) + ' horas.'
        elif (cant_horas < cant_horas_restantes):
            mensaje = str(cant_horas_restantes - cant_horas) + ' horas más de las que tiene disponible'
        else:
            mensaje = 'Ya no quedan horas disponibles. Gastó exactamente la capacidad del Sprint.'

        return render(request,'us/sprint_backlog_para_elegir.html', {'uss':uss, 'mensaje':mensaje, 'cant_horas':cant_horas, 'dias':cant_dias, 'proyecto':proyecto})
    else:
        return HttpResponseForbidden()

@login_required
@transaction.atomic
def sprint_backlog(request, pk):
    """
    Esta View permite visualizar el Sprint Backlog con los Us del cual pertenece
    :param request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    :param pk: es el ID del proyecto en donde se encuentra el Product Backlog
    :return: el template donde se visualiza el Sprint
    """
    proyecto = Proyecto.objects.get(id=pk)

    teammember = TeamMember.objects.filter(id_usuario=request.user.usuario.id)
    teammember = list(teammember)
    permitido = False
    is_scrum = False
    us_proyecto = []
    rol_usuario = []
    try:
        for i in range(len(teammember)):
            try:
                team = Team.objects.get(id_team_member=teammember[i].id)
            except:
                continue
            if team.id_proyecto_id == proyecto.id:
                permitido = True
                if teammember[i].id_rol.nombre == 'scrum':
                    is_scrum = True

    except:
        return HttpResponseForbidden()
    if permitido:
        sprint = Sprint.objects.get(estado='ACT', proyecto=proyecto)
        usa = Us.objects.filter(proyecto=proyecto, sprint=sprint)
        us_lista = list(usa)
        fecha_fin = sprint.fecha_fin_est
        fecha_hoy = date.today()
        cant_dias = fecha_fin - fecha_hoy + timedelta(days=1)
        fecha_fin = fecha_fin + timedelta(days=1)
        """fechas de feriados del anho 2019"""
        feriados = [datetime.strptime('2019-01-01', '%Y-%m-%d').date(),
                    datetime.strptime('2019-03-01', '%Y-%m-%d').date(),
                    datetime.strptime('2019-04-18', '%Y-%m-%d').date(),
                    datetime.strptime('2019-04-19', '%Y-%m-%d').date(),
                    datetime.strptime('2019-05-01', '%Y-%m-%d').date(),
                    datetime.strptime('2019-05-14', '%Y-%m-%d').date(),
                    datetime.strptime('2019-05-15', '%Y-%m-%d').date(),
                    datetime.strptime('2019-06-12', '%Y-%m-%d').date(),
                    datetime.strptime('2019-08-15', '%Y-%m-%d').date(),
                    datetime.strptime('2019-09-29', '%Y-%m-%d').date(),
                    datetime.strptime('2019-12-08', '%Y-%m-%d').date(),
                    datetime.strptime('2019-12-25', '%Y-%m-%d').date()]
        for fecha in daterange(fecha_hoy, fecha_fin):
            if fecha.weekday()==6 or fecha in feriados:
                """ No se toma en consideracion los domingos y feriados"""
                cant_dias = cant_dias - timedelta(days=1)
                """ No se toma en consideracion aquellos Us que no se encuentran aun incluidos en el Sprint"""
        cant_dias = str(cant_dias).split('d')[0]
        cant_dias = int(cant_dias)
        """ Se obtiene las horas trabajadas del Sprint al cual pertenece el proyecto"""
        horas = Horas.objects.filter(sprint = sprint)
        cant_horas = 0
        for hora in horas:
            cant_horas = cant_horas + hora.horas_trabajadas
        cant_horas = cant_horas * cant_dias
        cant_horas_gastadas = cant_horas
        for us_lista in us_lista:
                cant_horas_gastadas = cant_horas_gastadas - us_lista.tiempo_finalizacion
        cant_horas_restantes = cant_horas - cant_horas_gastadas
        uss = Us.objects.filter(sprint=sprint)
        """Mensaje para imprimir la capacidad el Sprint"""
        if (cant_horas > cant_horas_restantes):
            mensaje = str(cant_horas - cant_horas_restantes) + ' horas.'
        elif (cant_horas < cant_horas_restantes):
            mensaje = str(cant_horas_restantes - cant_horas) + ' horas más de las que tiene disponible'
        else:
            mensaje = 'Ya no quedan horas disponibles. Gastó exactamente la capacidad del Sprint.'

        return render(request,'us/sprint_backlog.html', {'uss':uss, 'mensaje':mensaje, 'cant_horas':cant_horas, 'dias':cant_dias, 'proyecto':proyecto})
    else:
        return HttpResponseForbidden()

@login_required
@transaction.atomic
def a_sprint(request, proyecto_id, us_id):
    """
        View en el cual se muestra y se trabaja para mover un user story al sprint backlog
        :param request: objeto HttpRequest que representa la metadata de la solicitud HTTP
        :param proyecto_id: es el ID del proyecto en donde se encuentra el Sprint Backlog
        :param us_id: es el ID del user story con el cual se esta trabajando
        :return: el template donde se visualiza la pagina
    """
    us = Us.objects.get(id = us_id)
    proyecto = Proyecto.objects.get(id = proyecto_id)

    teammember = TeamMember.objects.filter(id_usuario=request.user.usuario.id)
    teammember = list(teammember)
    permitido = False
    is_scrum = False
    us_proyecto = []
    rol_usuario = []
    try:
        for i in range(len(teammember)):
            try:
                team = Team.objects.get(id_team_member=teammember[i].id)
            except:
                continue
            if team.id_proyecto_id == proyecto.id:
                if teammember[i].id_rol.nombre == 'scrum':
                    is_scrum = True
                    permitido = True
    except:
        return HttpResponseForbidden()
    if permitido:
        sprint = Sprint.objects.get(estado='ACT',proyecto=proyecto)

        fecha_fin = sprint.fecha_fin_est
        fecha_hoy = date.today()
        cant_dias = fecha_fin - fecha_hoy + timedelta(days=1)
        fecha_fin = fecha_fin + timedelta(days=1)
        """fechas de feriados del anho 2019"""
        feriados = [datetime.strptime('2019-01-01', '%Y-%m-%d').date(),
                    datetime.strptime('2019-03-01', '%Y-%m-%d').date(),
                    datetime.strptime('2019-04-18', '%Y-%m-%d').date(),
                    datetime.strptime('2019-04-19', '%Y-%m-%d').date(),
                    datetime.strptime('2019-05-01', '%Y-%m-%d').date(),
                    datetime.strptime('2019-05-14', '%Y-%m-%d').date(),
                    datetime.strptime('2019-05-15', '%Y-%m-%d').date(),
                    datetime.strptime('2019-06-12', '%Y-%m-%d').date(),
                    datetime.strptime('2019-08-15', '%Y-%m-%d').date(),
                    datetime.strptime('2019-09-29', '%Y-%m-%d').date(),
                    datetime.strptime('2019-12-08', '%Y-%m-%d').date(),
                    datetime.strptime('2019-12-25', '%Y-%m-%d').date()]
        for fecha in daterange(fecha_hoy, fecha_fin):
            if fecha.weekday() == 6 or fecha in feriados:
                """ No se toma en consideracion los domingos y feriados"""
                cant_dias = cant_dias - timedelta(days=1)
                """ No se toma en consideracion aquellos Us que no se encuentran aun incluidos en el Sprint"""
        cant_dias = str(cant_dias).split('d')[0]
        cant_dias = int(cant_dias)

        usuarios = TeamMember.objects.filter(sprint= sprint)
        sumas = []
        """Se obtienen los usuario del sprint para verificar las horas disponibles de cada uno"""
        for usuario in usuarios:
            sum = Horas.objects.get(teammember__id_usuario=usuario.id_usuario, sprint=sprint)
            suma = sum.horas_trabajadas
            uss_usuario =Us.objects.filter(usuario=usuario.id_usuario.user, sprint=sprint)
            uss_usuario = list(uss_usuario)
            if len(uss_usuario) > 0:
                for uss in uss_usuario:
                    suma = suma - uss.tiempo_finalizacion//cant_dias
            sumas.append(suma)
        sumas = zip(usuarios, sumas)
        tableros = Tablero.objects.filter(proyecto=proyecto)
        if sprint.para_importar != None:
            para_importar = str(sprint.para_importar)
        else:
            para_importar = ''
        if request.method == "POST":
            us.usuario = User.objects.get(username = request.POST.get('usuario',False))
            us.tablero = Tablero.objects.get(id=request.POST.get('tablero', False))
            us.sprint = sprint
            if us.aprobado == 0:
                us.fase = Fase.objects.get(tablero=us.tablero,orden=1)
                us.estado = 1
            elif us.aprobado == 2 or us.aprobado == 3:
                us.aprobado=0

            us.save()
            try:
                email = EmailMessage('SGPA (Sistema de Gestion de Proyectos Agiles)',
                                 'Querido '+ us.usuario.username + ':\nSe le asignó el user story ' + us.descripcion_breve + ' en el proyecto ' + us.proyecto.nombre + '.\nSaludos',
                                 to=[us.usuario.email])
                email.send()
            except:
                pass
            sprint.para_importar = para_importar + us.descripcion_breve + ';' + us.descripcion_completa + ';' + str(us.tiempo_finalizacion) + ';' + str(us.valor_negocio) + ';' + str(us.prioridad) + ';' + str(us.valor_tecnico) + ';' + str(us.prioridad_promedio) + ':'
            sprint.save()
            return redirect('/sprint/iniciar2/'+str(proyecto.id))
        return render(request, 'us/a_sprint.html', {'proyecto':proyecto, 'usuarios':usuarios, 'tableros':tableros, 'sumas':sumas, 'us':us, })
    else:
        return HttpResponseForbidden()

@login_required
@transaction.atomic
def a_product(request, proyecto_id, us_id):
    """
        View en el cual se muestra y se trabaja para mover un user story al prodcut backlog
        :param request: objeto HttpRequest que representa la metadata de la solicitud HTTP
        :param proyecto_id: es el ID del proyecto en donde se encuentra el Product Backlog
        :param us_id: es el ID del user story con el cual se esta trabajando
        :return: el template donde se visualiza la pagina
    """
    us = Us.objects.get(id = us_id)
    proyecto = Proyecto.objects.get(id = proyecto_id)

    teammember = TeamMember.objects.filter(id_usuario=request.user.usuario.id)
    teammember = list(teammember)
    permitido = False
    is_scrum = False
    us_proyecto = []
    rol_usuario = []
    try:
        for i in range(len(teammember)):
            try:
                team = Team.objects.get(id_team_member=teammember[i].id)
            except:
                continue
            if team.id_proyecto_id == proyecto.id:
                if teammember[i].id_rol.nombre == 'scrum':
                    is_scrum = True
                    permitido = True
    except:
        return HttpResponseForbidden()
    if permitido:

        us.usuario = None
        us.tablero = None
        us.sprint = None
        us.fase = None
        us.estado=0

        us.save()
        return redirect('/sprint/iniciar2/'+str(proyecto.id))
        return render(request, 'us/a_product.html', {'proyecto':proyecto})
    else:
        return HttpResponseForbidden()

@login_required
@transaction.atomic
def add_tablero(request, pk):
    """
    Esta view añade un User Story(US) que se encuentra en el Product Backlog al tablero
    :param request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    :param pk: El ID del User Story que queremos añadir al tablero
    :return:
    """
    us = Us.objects.get(pk=pk)
    teammember = TeamMember.objects.filter(id_usuario=request.user.usuario.id)
    teammember = list(teammember)
    is_scrum = False
    us_proyecto = []
    rol_usuario = []
    proyecto = us.tablero.proyecto
    try:
        for i in range(len(teammember)):
            try:
                team = Team.objects.get(id_team_member=teammember[i].id)
            except:
                continue
            if team.id_proyecto_id == proyecto.id:
                if teammember[i].id_rol.nombre == 'scrum':
                    is_scrum = True
    except:
        pass

    if is_scrum:
        """Un US al añadirse al tablero por defecto se encuentra en el estado=1 de la fase=1 """
        us.estado = 1
        tablero = us.tablero
        fase = Fase.objects.get(tablero=tablero, orden=1)
        us.fase=fase
        us.save()
        """Se añade el US al tablero de su proyecto"""
        proyecto = tablero.proyecto.id
        messages.success(request, 'Se añadió correctamente al tablero')
        return redirect('/us/'+str(proyecto))
        return render(request, 'us/add.html')
    else:
        return HttpResponseForbidden()

@login_required
@transaction.atomic
def crear(request, pk):
    """
    Crea un User Story.
    :param request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    :param pk: el ID del proyecto
    :return: el formulario para crear el US
    """
    teammember = TeamMember.objects.filter(id_usuario=request.user.usuario.id)
    teammember = list(teammember)
    is_scrum = False
    us_proyecto = []
    rol_usuario = []
    proyecto = Proyecto.objects.get(id=pk)
    try:
        for i in range(len(teammember)):
            try:
                team = Team.objects.get(id_team_member=teammember[i].id)
            except:
                continue
            if team.id_proyecto_id == proyecto.id:
                if teammember[i].id_rol.nombre == 'scrum':
                    is_scrum = True
    except:
        pass

    if is_scrum:
        """Obtiene el proyecto al cual pertenece el US, su tablero y su equipo"""
        proyecto = Proyecto.objects.get(id=pk)
        usuarios = TeamMember.objects.filter(team__id_proyecto=proyecto)
        """Se carga el formulario de creación de US"""
        if request.method == "POST":
            form = UsForm(request.POST)
            if form.is_valid():
                obj = form.save(commit=False)
                """carga el proyecto"""
                obj.proyecto = proyecto
                """calcula la prioridad del US y lo carga en el form"""
                obj.prioridad_promedio = obj.get_prioridad_promedio()
                """crea el US"""
                obj.save()
                messages.success(request, 'Se creó correctamente el User Story')
                return redirect('/us/'+str(proyecto.id))
        else:
            #formulario inicial
            form = UsForm()
        return render(request, 'us/crear.html', {'form':form, 'usuarios':usuarios,'proyecto':proyecto })
    else:
        return HttpResponseForbidden()


@login_required
@transaction.atomic
def crear2(request, pk):
    """
    Crea un User Story.
    :param request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    :param pk: el ID del proyecto
    :return: el formulario para crear el US
    """
    teammember = TeamMember.objects.filter(id_usuario=request.user.usuario.id)
    teammember = list(teammember)
    is_scrum = False
    us_proyecto = []
    rol_usuario = []
    proyecto = Proyecto.objects.get(id=pk)
    try:
        for i in range(len(teammember)):
            try:
                team = Team.objects.get(id_team_member=teammember[i].id)
            except:
                continue
            if team.id_proyecto_id == proyecto.id:
                if teammember[i].id_rol.nombre == 'scrum':
                    is_scrum = True
    except:
        pass

    if is_scrum:
        """Obtiene el proyecto al cual pertenece el US, su tablero y su equipo"""
        proyecto = Proyecto.objects.get(id=pk)
        usuarios = TeamMember.objects.filter(team__id_proyecto=proyecto)
        """Se carga el formulario de creación de US"""
        if request.method == "POST":
            form = UsForm(request.POST)
            if form.is_valid():
                obj = form.save(commit=False)
                """carga el proyecto"""
                obj.proyecto = proyecto
                """calcula la prioridad del US y lo carga en el form"""
                obj.prioridad_promedio = obj.get_prioridad_promedio()
                """crea el US"""
                obj.save()
                messages.success(request, 'Se creó correctamente el User Story')
                return redirect('/us/elegir/'+str(proyecto.id))
        else:
            #formulario inicial
            form = UsForm()
        return render(request, 'us/crear.html', {'form':form, 'usuarios':usuarios,'proyecto':proyecto })
    else:
        return HttpResponseForbidden()


@login_required
@transaction.atomic
def modificar(request, pk):
    """
    Permite modificar los datos de un US
    :param request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    :param pk: El ID del US que se desea modificar
    :return: El formulario para la modificación
    """
    us = Us.objects.get(pk=pk)
    proyecto = Proyecto.objects.get(id=us.proyecto.id)
    usuarios = TeamMember.objects.filter(team__id_proyecto=proyecto)
    teammember = TeamMember.objects.filter(id_usuario=request.user.usuario.id)
    teammember = list(teammember)
    is_scrum = False
    us_proyecto = []
    rol_usuario = []
    try:
        for i in range(len(teammember)):
            try:
                team = Team.objects.get(id_team_member=teammember[i].id)
            except:
                continue
            if team.id_proyecto_id == proyecto.id:
                if teammember[i].id_rol.nombre == 'scrum':
                    is_scrum = True
    except:
        pass

    if is_scrum:
        if request.method == 'POST' and 'save' in request.POST:
            modificar_us_form = ModificarUsForm(request.POST, instance=us)
            if modificar_us_form.is_valid():
                """Modifica el US"""
                obj = modificar_us_form.save(commit=False)
                """calcula la prioridad del US y lo carga en el form"""
                obj.prioridad_promedio = obj.get_prioridad_promedio()
                """crea el US"""
                obj.save()
                messages.success(request, ('Se actualizo el User Story Seleccionado!'))
                return redirect('/tablero/listo')
            else:
                messages.error(request, ('Uno o mas datos ingresados no son validos'))
        else:
            #formulario inicial
            modificar_us_form = ModificarUsForm(instance=us)
        return render(request, 'us/modificar.html', {
            'modificar_us_form': modificar_us_form,
            'us': us,'usuarios':usuarios,
        })
    else:
        return HttpResponseForbidden()

@login_required
@transaction.atomic
def eliminar(request, pk):
    """
    Elimina un US
    :param request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    :param pk: El ID del US que se desea eliminar
    :return: el formulario de eliminación
    """

    """Obtiene el US a eliminar a traves de su ID"""
    us = Us.objects.get(pk=pk)
    proyecto = Proyecto.objects.get(pk=us .proyecto.id)
    teammember = TeamMember.objects.filter(id_usuario=request.user.usuario.id)
    teammember = list(teammember)
    is_scrum = False
    us_proyecto = []
    rol_usuario = []
    try:
        for i in range(len(teammember)):
            try:
                team = Team.objects.get(id_team_member=teammember[i].id)
            except:
                continue
            if team.id_proyecto_id == proyecto.id:
                if teammember[i].id_rol.nombre == 'scrum':
                    is_scrum = True
    except:
        pass

    if is_scrum:
        if request.method == 'POST':
            """Elimina el US"""
            us.delete()
            messages.success(request, ('Se elimino el User Story Seleccionado!'))
            return redirect('/tablero/listo')
        return render(request, 'us/eliminar.html', {'us': us})
    else:
        return HttpResponseForbidden()

@login_required
@transaction.atomic
def avanzar(request,pk):
    """
    Permite que un US avance un estado en el Tablero Kanban del Proyecto.
    :param request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    :param pk: El ID del User Story que se desea que avance en el Tablero Kanban
    :return:
    """
    us = Us.objects.get(pk=pk)
    proyecto = us.tablero.proyecto
    los_scrum = TeamMember.objects.filter(id_rol__nombre='scrum', team__id_proyecto=proyecto)
    teammember = TeamMember.objects.filter(id_usuario=request.user.usuario.id)
    teammember = list(teammember)
    is_scrum = False
    us_proyecto = []
    rol_usuario = []
    try:
        for i in range(len(teammember)):
            try:
                team = Team.objects.get(id_team_member=teammember[i].id)
            except:
                continue
            if team.id_proyecto_id == proyecto.id:
                if teammember[i].id_rol.nombre == 'scrum':
                    is_scrum = True
    except:
        pass

    if is_scrum or request.user.id == us.usuario.id:
        tablero = us.tablero
        fases = Fase.objects.filter(tablero=tablero)
        fases = list(fases)
        cant_fases = len(fases)

        if us.estado < 3:
            """ Cambio de estado"""
            us.estado = us.estado + 1
        elif us.estado == 3:
            if us.fase.orden != cant_fases:
                nva_fase = Fase.objects.get(orden=us.fase.orden+1,tablero=tablero)
                us.fase = nva_fase
                """ Cambio de estado"""
                us.estado=1
        if us.fase.orden == cant_fases and us.estado == 3:
            try:
                for scrum in list(los_scrum):
                    email = EmailMessage('SGPA (Sistema de Gestion de Proyectos Agiles)',
                                            'Querido scrum master ' + scrum.id_usuario.user.username + ':\nEl user story ' + us.descripcion_breve + ' en el proyecto ' + us.proyecto.nombre + ' ha llegado a la última fase '+ us.fase.nombre+' en el Tipo de User Story '+us.fase.tablero.nombre+'.\nCuando el Sprint actual finalice, deberá realizar el control de calidad de dicho User Story, excepto si el usuario encargado del User Story lo vuelva a retroceder en el Tipo de User Story.\nSaludos',
                                            to=[us.usuario.email])
                    email.send()
            except:
                pass
        us.save()
        return redirect('/tablero/ver/'+str(tablero.id))
    else:
        return HttpResponseForbidden()

    return render(request, '/us/avanzar.html')

@login_required
@transaction.atomic
def retroceder(request, pk):
    """
    Permite que un US retroceda un estado en el Tablero Kanban del Proyecto.
    :param request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    :param pk: El ID del User Story que se desea que retroceder en el Tablero Kanban
    :return:
    """
    us = Us.objects.get(pk=pk)
    proyecto = us.tablero.proyecto
    teammember = TeamMember.objects.filter(id_usuario=request.user.usuario.id)
    teammember = list(teammember)
    is_scrum = False
    us_proyecto = []
    rol_usuario = []
    try:
        for i in range(len(teammember)):
            try:
                team = Team.objects.get(id_team_member=teammember[i].id)
            except:
                continue
            if team.id_proyecto_id == proyecto.id:
                if teammember[i].id_rol.nombre == 'scrum':
                    is_scrum = True
    except:
        pass

    if is_scrum or request.user.id == us.usuario.id:
        tablero = Tablero.objects.get(pk=us.tablero.pk)
        fase = Fase.objects.get(pk=us.fase.pk, tablero=tablero)


        if fase.orden != 1:
            """ Si la fase no es del primer orden """
            if us.estado == 1:
                n_fase = Fase.objects.get(orden=fase.orden-1, tablero=tablero)
                us.fase = n_fase
                """ Cambio de estado"""
                us.estado = 3
            elif us.estado == 3:
                """ Cambio de estado"""
                us.estado = 2
            elif us.estado == 2:
                """ Cambio de estado"""
                us.estado = 1
            us.save()
            return redirect('/tablero/ver/'+str(tablero.pk))
        if fase.orden == 1:
            if us.estado == 3:
                """ Cambio de estado"""
                us.estado = 2
            else:
                """ Cambio de estado"""
                us.estado = 1
            us.save()
            return redirect('/tablero/ver/'+str(tablero.pk))
    else:
        return HttpResponseForbidden()

@login_required
@transaction.atomic
def adjuntar(request, pk):
        """
       Permite adjuntar un US ya sea nota o un archivo
       :param request: objeto HttpRequest que representa la metadata de la solicitud HTTP
       :return:
       """

        uss = Us.objects.get(pk=pk)
        us = Us.objects.get(pk=pk)
        proyecto = us.tablero.proyecto

        tiempo = 0
        try:
            dailys = Daily.objects.filter(us=us).order_by('fecha')
            dailys = list(dailys)
            for daily in dailys:
                tiempo = tiempo + daily.tiempo
        except:
            pass

        teammember = TeamMember.objects.filter(id_usuario=request.user.usuario.id)
        teammember = list(teammember)
        is_scrum = False
        us_proyecto = []
        rol_usuario = []
        try:
            for i in range(len(teammember)):
                try:
                    team = Team.objects.get(id_team_member=teammember[i].id)
                except:
                    continue
                if team.id_proyecto_id == proyecto.id:
                    if teammember[i].id_rol.nombre == 'scrum':
                        is_scrum = True
        except:
            pass

        if is_scrum or request.user.id == us.usuario.id:
            archivos = Archivo.objects.filter(us=uss).order_by('fecha')
            notas = Nota.objects.filter(us=uss).order_by('fecha')
            usuario = request.user
            if request.method == 'POST':
                form = ArchivoForm(request.POST, request.FILES)
                form2 = NotaForm(request.POST)
                if 'archivo' in request.POST:
                     if form.is_valid():
                             """Guarda el archivo"""
                             #form.save()
                             obj = form.save(commit=False)
                             obj.us = Us.objects.get(id=pk)
                             obj.usuario = request.user
                             obj.save()
                             messages.success(request, ('Se adjuntó correctamente el archivo.'))
                             return redirect('/us/adjuntar/'+str(uss.id))
                if 'nota' in request.POST:
                    if form2.is_valid():
                        #form2.save()
                        obj = form2.save(commit=False)
                        obj.us = Us.objects.get(id=pk)
                        obj.usuario = request.user
                        obj.save()
                        """Guarda la nota"""
                        messages.success(request, ('Se adjuntó correctamente la nota.'))
                        return redirect('/us/adjuntar/'+str(uss.id))
            else:
                form = ArchivoForm()
                form2 = NotaForm()

            return render(request, 'us/subir_archivo.html', {
            'us': uss,'form2': form2, 'form': form,'archivos':archivos, 'notas':notas, 'usuario':usuario, 'dailys':dailys, 'tiempo':tiempo
            })
        else:
            return HttpResponseForbidden()

@login_required
@transaction.atomic
def buscarus(request, pk):
    """
            Permite adjuntar un US ya sea nota o un archivo
          :param request: objeto HttpRequest que representa la metadata de la solicitud HTTP
          :return:
          """

    if request.method == 'POST':
        busqueda = request.POST['buscalo']
        proyecto = Proyecto.objects.get(id=pk)
        teammember = TeamMember.objects.filter(id_usuario=request.user.usuario.id)
        teammember = list(teammember)
        permitido = False
        us_proyecto = []
        rol_usuario = []
        try:
            for i in range(len(teammember)):
                try:
                    team = Team.objects.get(id_team_member=teammember[i].id)
                except:
                    continue
                if team.id_proyecto_id == proyecto.id:
                    permitido = True
        except:
            pass

        if permitido:
            tablero = Tablero.objects.get(proyecto=proyecto)
            """
            Filtra de acuerdo a la palabra que se indico
            :param request:
            """
            if busqueda:
                match = Us.objects.filter(Q(descripcion_breve__contains=busqueda), tablero=tablero).order_by('prioridad_promedio')
                if match:
                    return render(request, 'us/buscarus.html', {'sr': match})
                else:
                    messages.error(request, 'No existe un US con esa descripción')
            else:
                matchh = Us.objects.filter(Q(descripcion_breve__contains=busqueda), tablero=tablero).order_by('prioridad_promedio')
                return render(request, 'us/buscarus.html', {'sr': matchh.all()})

        else:
            return HttpResponseForbidden()
    return render(request, 'us/buscarus.html')

def addDaily(request, us_id, adelante):
    """
        View en el se añade un daily al realizar un cambio de fase o estado a un US
        :param request: objeto HttpRequest que representa la metadata de la solicitud HTTP
        :param us_id: es el ID del user story con el cual se esta trabajando
        :param adelante: valor que nos dice si el tablero avanzo o retrocedio de estado
        :return: el template donde se visualiza la pagina
    """
    us = Us.objects.get(id = us_id)
    proyecto = us.proyecto
    permitido = False
    try:
        if us.usuario == request.user:
            permitido = True

    except:
        return HttpResponseForbidden()
    if permitido:
        tab_id=us.tablero.id
        if request.method == 'POST':
            daily = request.POST['d']
            form = DailyForm(request.POST)
            if form.is_valid():
                obj = form.save(commit=False)
                obj.us = us
                obj.usuario = request.user
                obj.fase = us.fase
                obj.estado = us.estado
                if adelante == '1':
                    obj.avanza = True
                else:
                    obj.avanza = False
                if daily is not "":
                    obj.daily = daily
                else:
                    obj.daily = "No se cargo correctamente"
                obj.sprint = us.sprint
                obj.save()
                messages.success(request, ('Se guardó correctamente.'))
                if obj.avanza:
                    return redirect('/us/avanzar/'+str(us_id))
                else:
                    return redirect('/us/retroceder/'+str(us_id))
        else:
            form = DailyForm()
        return render(request, 'us/daily.html',{'form':form, 'tab_id':tab_id})
    else:
        return HttpResponseForbidden()

def listar_archivos_notas(request, us_id):
    """
        View para listar los archivos y notas de un US
        :param request: objeto HttpRequest que representa la metadata de la solicitud HTTP
        :param us_id: es el ID del user story con el cual se esta trabajando
        :return: el template donde se visualiza la pagina
    """
    us = Us.objects.get(id=us_id)
    proyecto = us.proyecto

    teammember = TeamMember.objects.filter(id_usuario=request.user.usuario.id)
    teammember = list(teammember)
    permitido = False
    is_scrum = False
    us_proyecto = []
    rol_usuario = []
    try:
        for i in range(len(teammember)):
            try:
                team = Team.objects.get(id_team_member=teammember[i].id)
            except:
                continue
            if team.id_proyecto_id == proyecto.id:
                if teammember[i].id_rol.nombre == 'scrum':
                    is_scrum = True
                    permitido = True
    except:
        return HttpResponseForbidden()
    if permitido:

        archivos = Archivo.objects.filter(us=us).order_by('fecha')
        notas = Nota.objects.filter(us=us).order_by('fecha')
        dailys = Daily.objects.filter(us=us).order_by('fecha')
        tiempo = 0
        dailys = list(dailys)
        for daily in dailys:
            tiempo = tiempo + daily.tiempo
        return render(request, 'us/listar_archivos_notas.html', {'archivos':archivos,'notas':notas,'dailys':dailys, 'tiempo':tiempo})
    else:
        return HttpResponseForbidden()

def calidad(request, us_id):
    """
        View para el formulario del control de calidad para cada US
        :param request: objeto HttpRequest que representa la metadata de la solicitud HTTP
        :param us_id: es el ID del user story con el cual se esta trabajando
        :return: el template donde se visualiza la pagina
    """
    us = Us.objects.get(id=us_id)
    proyecto = us.proyecto
    tiempo = 0
    try:
        dailys = Daily.objects.filter(us=us).order_by('fecha')
        dailys = list(dailys)
        for daily in dailys:
            tiempo = tiempo + daily.tiempo
    except:
        pass
    teammember = TeamMember.objects.filter(id_usuario=request.user.usuario.id)
    teammember = list(teammember)
    permitido = False
    is_scrum = False
    us_proyecto = []
    rol_usuario = []
    try:
        for i in range(len(teammember)):
            try:
                team = Team.objects.get(id_team_member=teammember[i].id)
            except:
                continue
            if team.id_proyecto_id == proyecto.id:
                if teammember[i].id_rol.nombre == 'scrum':
                    is_scrum = True
                    permitido = True
    except:
        return HttpResponseForbidden()
    if permitido:
        tablero = us.tablero
        fases = Fase.objects.filter(tablero=tablero)
        if request.method == 'POST':
            fase = Fase.objects.get(id = request.POST.get('fase',False))
            us.fase = fase
            us.estado = request.POST.get('estado',False)
            us.tiempo_finalizacion = request.POST.get('tiempo',False)
            us.save()
            return redirect('/us/calidad_listo/'+str(us_id))
        return render(request, 'us/calidad.html', {'us': us, 'tablero':tablero, 'fases': fases, 'tiempo':tiempo})
    else:
        return HttpResponseForbidden()

def calidad_listo(request, us_id):
    """
        View en el cual finaliza el control de calidad para cada US
        :param us_id: es el ID del user story con el cual se esta trabajando
        :return: el template donde se visualiza la pagina
    """
    us = Us.objects.get(id=us_id)
    proyecto = us.proyecto

    teammember = TeamMember.objects.filter(id_usuario=request.user.usuario.id)
    teammember = list(teammember)
    permitido = False
    is_scrum = False
    us_proyecto = []
    rol_usuario = []
    try:
        for i in range(len(teammember)):
            try:
                team = Team.objects.get(id_team_member=teammember[i].id)
            except:
                continue
            if team.id_proyecto_id == proyecto.id:
                if teammember[i].id_rol.nombre == 'scrum':
                    is_scrum = True
                    permitido = True
    except:
        return HttpResponseForbidden()
    if permitido:
        return render(request, 'us/calidad_listo.html', {'us': us,})
    else:
        return HttpResponseForbidden()

@login_required
@transaction.atomic
def finalizados(request, pk):
    """
    Lista los US finalizados del Proyecto. Estos son los US con atributo aprobado==1
    :param request:
    :param pk: es el primary key del Proyecto
    :return: el template donde se visualiza la pagina
    """
    proyecto = Proyecto.objects.get(id=pk)

    teammember = TeamMember.objects.filter(id_usuario=request.user.usuario.id)
    teammember = list(teammember)
    permitido = False
    is_scrum = False
    us_proyecto = []
    rol_usuario = []
    try:
        for i in range(len(teammember)):
            try:
                team = Team.objects.get(id_team_member=teammember[i].id)
            except:
                continue
            if team.id_proyecto_id == proyecto.id:
                permitido = True
                if teammember[i].id_rol.nombre == 'scrum':
                    is_scrum = True

    except:
        return HttpResponseForbidden()
    if permitido:
        uss = Us.objects.filter(proyecto=proyecto,aprobado=1)
        tiempos = []
        for us in uss:
            dailys = Daily.objects.filter(us=us).order_by('fecha')
            tiempo = 0
            dailys = list(dailys)
            for daily in dailys:
                tiempo = tiempo + daily.tiempo
            tiempos.append(tiempo)
        sumas = zip(uss, tiempos)
        return render(request, 'us/finalizados.html', {'uss': sumas})
    else:
        return HttpResponseForbidden()

@login_required
@transaction.atomic
def horas(request, us_id):
    us = Us.objects.get(pk=us_id)
    tiempo = 0
    try:
        dailys = Daily.objects.filter(us=us).order_by('fecha')
        dailys = list(dailys)
        for daily in dailys:
            tiempo = tiempo + daily.tiempo
    except:
        pass
    return render(request, 'us/horas.html', {'tiempo':tiempo})

@login_required
@transaction.atomic
def reportebacklog(request, id_proyecto):
    """Genera reporte sobre el product backlog"""
    #SPRINT BACKLOG
    proyecto = Proyecto.objects.get(id = id_proyecto)
    uss = None
    mensaje = None
    hay_sprint = False
    cant_horas = None
    cant_dias = None
    try:
        sprint = Sprint.objects.get(estado='ACT', proyecto=proyecto)
        usa = Us.objects.filter(proyecto=proyecto, sprint=sprint)
        us_lista = list(usa)
        fecha_fin = sprint.fecha_fin_est
        fecha_hoy = date.today()
        cant_dias = fecha_fin - fecha_hoy + timedelta(days=1)
        fecha_fin = fecha_fin + timedelta(days=1)
        """fechas de feriados del anho 2019"""
        feriados = [datetime.strptime('2019-01-01', '%Y-%m-%d').date(),
                    datetime.strptime('2019-03-01', '%Y-%m-%d').date(),
                    datetime.strptime('2019-04-18', '%Y-%m-%d').date(),
                    datetime.strptime('2019-04-19', '%Y-%m-%d').date(),
                    datetime.strptime('2019-05-01', '%Y-%m-%d').date(),
                    datetime.strptime('2019-05-14', '%Y-%m-%d').date(),
                    datetime.strptime('2019-05-15', '%Y-%m-%d').date(),
                    datetime.strptime('2019-06-12', '%Y-%m-%d').date(),
                    datetime.strptime('2019-08-15', '%Y-%m-%d').date(),
                    datetime.strptime('2019-09-29', '%Y-%m-%d').date(),
                    datetime.strptime('2019-12-08', '%Y-%m-%d').date(),
                    datetime.strptime('2019-12-25', '%Y-%m-%d').date()]
        for fecha in daterange(fecha_hoy, fecha_fin):
            if fecha.weekday() == 6 or fecha in feriados:
                """ No se toma en consideracion los domingos y feriados"""
                cant_dias = cant_dias - timedelta(days=1)
                """ No se toma en consideracion aquellos Us que no se encuentran aun incluidos en el Sprint"""
        cant_dias = str(cant_dias).split('d')[0]
        cant_dias = int(cant_dias)
        """ Se obtiene las horas trabajadas del Sprint al cual pertenece el proyecto"""
        horas = Horas.objects.filter(sprint=sprint)
        cant_horas = 0
        for hora in horas:
            cant_horas = cant_horas + hora.horas_trabajadas
        cant_horas = cant_horas * cant_dias
        cant_horas_gastadas = cant_horas
        for us_lista in us_lista:
            cant_horas_gastadas = cant_horas_gastadas - us_lista.tiempo_finalizacion
        cant_horas_restantes = cant_horas - cant_horas_gastadas
        uss = Us.objects.filter(sprint=sprint)
        """Mensaje para imprimir la capacidad el Sprint"""
        if (cant_horas > cant_horas_restantes):
            mensaje = str(cant_horas - cant_horas_restantes) + ' horas.'
        elif (cant_horas < cant_horas_restantes):
            mensaje = str(cant_horas_restantes - cant_horas) + ' horas más de las que tiene disponible'
        else:
            mensaje = 'Ya no quedan horas disponibles. Gastó exactamente la capacidad del Sprint.'
        hay_sprint = True
    except:
        pass

    #PRODUCT BACKLOG
    uss2 = Us.objects.filter(fase=None, proyecto=proyecto).order_by('-prioridad_promedio')
    uss2 = list(uss2)
    # Completar uss con los que están en un sprint y no aprobaron el control de calidad
    us_no_aprob = Us.objects.filter(aprobado=2, proyecto=proyecto)
    us_no_aprob = list(us_no_aprob)
    if us_no_aprob:
        for us_no in us_no_aprob:
            us_no.valor_negocio = 10
            us_no.prioridad = 10
            us_no.valor_tecnico = 10
            us_no.prioridad_promedio = 10
            us_no.save()

    us_no_llegaron = Us.objects.filter(aprobado=3, proyecto=proyecto)
    us_no_llegaron = list(us_no_llegaron)
    if us_no_llegaron:
        for us_no_l in us_no_llegaron:
            us_no_l.valor_negocio = 10
            us_no_l.prioridad = 10
            us_no_l.valor_tecnico = 10
            us_no_l.prioridad_promedio = 10
            us_no_l.save()

    uss3 = Us.objects.filter(proyecto=proyecto, aprobado=1)
    tiempos = []
    for us in uss3:
        dailys = Daily.objects.filter(us=us).order_by('fecha')
        tiempo = 0
        dailys = list(dailys)
        for daily in dailys:
            tiempo = tiempo + daily.tiempo
        tiempos.append(tiempo)
    sumas = zip(uss3, tiempos)
    long_uss3 = len(uss3)


    return render(request, 'us/reportebacklog.html',
                  {'uss': uss,'uss2':uss2, 'us_no_aprob': us_no_aprob, 'uss3':sumas,
                       'us_no_llegaron': us_no_llegaron, 'mensaje': mensaje, 'hay_sprint':hay_sprint,
                   'cant_horas': cant_horas, 'dias': cant_dias, 'proyecto': proyecto, 'long_3':long_uss3})

@login_required
@transaction.atomic
def reporteprioridad(request, pk):
    """
    Esta View permite visualizar el reporte con los Us de prioridad maxima
    """
    proyecto = Proyecto.objects.get(id=pk)
    sprint = Sprint.objects.get(estado='ACT', proyecto=proyecto)
    usa = Us.objects.filter(proyecto=proyecto, sprint=sprint)
    us_lista = list(usa)
    fecha_fin = sprint.fecha_fin_est
    fecha_hoy = date.today()
    cant_dias = fecha_fin - fecha_hoy + timedelta(days=1)
    fecha_fin = fecha_fin + timedelta(days=1)
    """fechas de feriados del anho 2019"""
    feriados = [datetime.strptime('2019-01-01', '%Y-%m-%d').date(),
                datetime.strptime('2019-03-01', '%Y-%m-%d').date(),
                datetime.strptime('2019-04-18', '%Y-%m-%d').date(),
                datetime.strptime('2019-04-19', '%Y-%m-%d').date(),
                datetime.strptime('2019-05-01', '%Y-%m-%d').date(),
                datetime.strptime('2019-05-14', '%Y-%m-%d').date(),
                datetime.strptime('2019-05-15', '%Y-%m-%d').date(),
                datetime.strptime('2019-06-12', '%Y-%m-%d').date(),
                datetime.strptime('2019-08-15', '%Y-%m-%d').date(),
                datetime.strptime('2019-09-29', '%Y-%m-%d').date(),
                datetime.strptime('2019-12-08', '%Y-%m-%d').date(),
                datetime.strptime('2019-12-25', '%Y-%m-%d').date()]
    for fecha in daterange(fecha_hoy, fecha_fin):
        if fecha.weekday()==6 or fecha in feriados:
                """ No se toma en consideracion los domingos y feriados"""
                cant_dias = cant_dias - timedelta(days=1)
                """ No se toma en consideracion aquellos Us que no se encuentran aun incluidos en el Sprint"""
    cant_dias = str(cant_dias).split('d')[0]
    cant_dias = int(cant_dias)
    """ Se obtiene las horas trabajadas del Sprint al cual pertenece el proyecto"""
    horas = Horas.objects.filter(sprint = sprint)
    cant_horas = 0
    for hora in horas:
        cant_horas = cant_horas + hora.horas_trabajadas
    cant_horas = cant_horas * cant_dias
    cant_horas_gastadas = cant_horas
    for us_lista in us_lista:
            cant_horas_gastadas = cant_horas_gastadas - us_lista.tiempo_finalizacion
    cant_horas_restantes = cant_horas - cant_horas_gastadas
    uss = Us.objects.filter(sprint=sprint)
    """Mensaje para imprimir la capacidad el Sprint"""
    if (cant_horas > cant_horas_restantes):
        mensaje = str(cant_horas - cant_horas_restantes) + ' horas.'
    elif (cant_horas < cant_horas_restantes):
        mensaje = str(cant_horas_restantes - cant_horas) + ' horas más de las que tiene disponible'
    else:
        mensaje = 'Ya no quedan horas disponibles. Se gastó exactamente la capacidad del Sprint.'

    return render(request,'us/reporteprioridad.html', {'uss':uss, 'mensaje':mensaje, 'cant_horas':cant_horas, 'dias':cant_dias, 'proyecto':proyecto})

@login_required
@transaction.atomic
def horasreales(request, pk):
    """
    Esta View permite visualizar las horas reales trabajadas por sprint, los us y los team members
    """
    proyecto = Proyecto.objects.get(id=pk)
    #POR SPRINT
    sprints = Sprint.objects.filter(proyecto=proyecto)
    sumasprint=[]
    total = 0
    for sprint in sprints:
        d_ss = Daily.objects.filter(sprint=sprint)
        aux = 0
        for d_s in d_ss:
            aux = aux + d_s.tiempo
        sumasprint.append(aux)
        total = total + aux
    sumas = zip(sprints, sumasprint)
    #POR USER STORY
    uss = Us.objects.filter(proyecto=proyecto)
    sumasus = []
    for us in uss:
        d_us = Daily.objects.filter(us=us)
        aux = 0
        for d_u in d_us:
            aux = aux + d_u.tiempo
        sumasus.append(aux)
    sumas1 = zip(uss, sumasus)
    #POR USUARIO
    team = Team.objects.get(id_proyecto=proyecto)
    tms = TeamMember.objects.filter(team=team)
    sumastm = []
    for tm in tms:
        d_ts = Daily.objects.filter(usuario=tm.id_usuario.user)
        aux = 0
        for d_t in d_ts:
            if d_t.sprint.proyecto == proyecto:
                aux = aux + d_t.tiempo
        sumastm.append(aux)
    sumas2 = zip(tms, sumastm)
    return render(request,'us/horasreales.html', {'proyecto':proyecto,'sprints':sumas, 'uss':sumas1, 'usuarios':sumas2, 'total':total})

@login_required
@transaction.atomic
def reportesprint(request, pk):
    """
    Esta View permite visualizar el reporte por sprint
    """
    proyecto = Proyecto.objects.get(id=pk)
    sprints = Sprint.objects.filter(proyecto=proyecto).order_by('fecha_ini')
    mensajes = []
    for sprint in sprints:
        a = sprint.para_importar.split(":")
        long = len(a)
        b = []
        for i in range(0, long - 1):
            b.append(a[i].split(";"))
        mensaje = ''
        for i in range(0, long - 1):
            mensaje = mensaje + ' ' + b[i][0] + ' '
        mensajes.append(mensaje)
    sumas = zip(sprints, mensajes)
    return render(request,'us/reportesprint.html', {'proyecto':proyecto, 'sprints':sumas})

@login_required
@transaction.atomic
def reportesprint2(request, pk):
    sprint = Sprint.objects.get(pk = pk)
    proyecto = sprint.proyecto
    usa = Us.objects.filter(proyecto=sprint.proyecto, sprint=sprint)
    us_lista = list(usa)
    fecha_fin = sprint.fecha_fin_est
    fecha_hoy = date.today()
    cant_dias = fecha_fin - fecha_hoy + timedelta(days=1)
    fecha_fin = fecha_fin + timedelta(days=1)
    """fechas de feriados del anho 2018"""
    feriados = [datetime.strptime('2018-01-01', '%Y-%m-%d').date(),
                datetime.strptime('2018-02-26', '%Y-%m-%d').date(),
                datetime.strptime('2018-03-29', '%Y-%m-%d').date(),
                datetime.strptime('2018-03-30', '%Y-%m-%d').date(),
                datetime.strptime('2018-05-01', '%Y-%m-%d').date(),
                datetime.strptime('2018-05-14', '%Y-%m-%d').date(),
                datetime.strptime('2018-05-15', '%Y-%m-%d').date(),
                datetime.strptime('2018-06-11', '%Y-%m-%d').date(),
                datetime.strptime('2018-08-15', '%Y-%m-%d').date(),
                datetime.strptime('2018-09-29', '%Y-%m-%d').date(),
                datetime.strptime('2018-12-08', '%Y-%m-%d').date(),
                datetime.strptime('2018-12-25', '%Y-%m-%d').date()]
    for fecha in daterange(fecha_hoy, fecha_fin):
        if fecha.weekday() == 6 or fecha in feriados:
            """ No se toma en consideracion los domingos y feriados"""
            cant_dias = cant_dias - timedelta(days=1)
            """ No se toma en consideracion aquellos Us que no se encuentran aun incluidos en el Sprint"""
    cant_dias = str(cant_dias).split('d')[0]
    cant_dias = int(cant_dias)
    """ Se obtiene las horas trabajadas del Sprint al cual pertenece el proyecto"""
    horas = Horas.objects.filter(sprint=sprint)
    cant_horas = 0
    for hora in horas:
        cant_horas = cant_horas + hora.horas_trabajadas
    cant_horas = cant_horas * cant_dias
    cant_horas_gastadas = cant_horas
    for us_lista in us_lista:
        cant_horas_gastadas = cant_horas_gastadas - us_lista.tiempo_finalizacion
    cant_horas_restantes = cant_horas - cant_horas_gastadas
    uss = Us.objects.filter(sprint=sprint)
    """Mensaje para imprimir la capacidad el Sprint"""
    if (cant_horas > cant_horas_restantes):
        mensaje = str(cant_horas - cant_horas_restantes) + ' horas.'
    elif (cant_horas < cant_horas_restantes):
        mensaje = str(cant_horas_restantes - cant_horas) + ' horas más de las que tiene disponible'
    else:
        mensaje = 'Ya no quedan horas disponibles. Gastó exactamente la capacidad del Sprint.'

    return render(request, 'us/reportesprint2.html',
                  {'uss': uss, 'mensaje': mensaje, 'cant_horas': cant_horas, 'dias': cant_dias, 'proyecto': proyecto, 'sprint':sprint})
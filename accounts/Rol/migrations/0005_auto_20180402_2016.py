# Generated by Django 2.0.3 on 2018-04-02 20:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Rol', '0004_remove_permiso_nivel'),
    ]

    operations = [
        migrations.AlterField(
            model_name='Rol',
            name='permiso',
            field=models.ManyToManyField(blank=True, to='Rol.Permiso'),
        ),
    ]

from django import forms
from Rol.models import *
from django import forms
from django.forms import CharField, Form

class BuscarRol(Form):
    """
    Formulario para buscar Rol por nombre
    """
    Nombre = CharField()

class RolForm(forms.ModelForm):
    """
    Formulario para crear un Rol
    """
    class Meta:
        model = Rol
        fields = [
            'nombre',
            'descripcion',
            'permiso'
        ]
        labels = {
            'nombre': 'Nombre del rol',
            'descripcion': 'Descripcion ',
            'permiso': 'Permiso ',

        }
        widgets = {
        'permiso': forms.CheckboxSelectMultiple(),
        }
    def clean_permiso(self):
        permiso = self.cleaned_data['permiso']
        try:
            pr = Permiso.objects.get(permiso = permiso)
        except:
            return self.cleaned_data['permiso']
        raise forms.ValidationError('Debe seleccionar al menos uno')




class UpdateRolForm(forms.ModelForm):
    """
    Formulario para la modificacion de un rol
    """
    class Meta:
        model = Rol
        fields = [
            'nombre',
            'descripcion',
            'permiso'
        ]
        labels = {
            'nombre': 'Nombre del rol ',
            'descripcion': 'Descripcion ',
            'permiso': 'Permiso ',

        }
        widgets = {
        'permiso': forms.CheckboxSelectMultiple(),
        }

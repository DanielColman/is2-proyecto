from django import forms
from .models import Proyecto
from django.forms import CharField, PasswordInput
from usuario.models import Usuario
import django.contrib.auth.forms
from django.forms import CharField, Form, PasswordInput


class CreateProyectoForm(forms.ModelForm):
    """
    Formulario para la creación de un nuevo Proyecto
    """
    # class Meta:
    #     model = Proyecto
    #     fields = ('nombre', 'fecha_ini', 'fecha_fin', 'observaciones')

    class Meta:
        model = Proyecto
        fields = ('nombre',)

class UpdateProyectoForm(forms.ModelForm):
    """
    Formulario para la modificacion de un Proyecto
    """
    class Meta:
        model = Proyecto
        fields = ('nombre', 'fecha_ini_est', 'fecha_fin_est', 'observaciones' )

class DisableProyectoForm(forms.ModelForm):
    """
    Formulario para la modificacion de un Proyecto
    """
    nombre = forms.CharField(
        widget=forms.TextInput(attrs={'readonly': 'readonly'})
    )
    fecha_ini = forms.DateField(
        widget=forms.TextInput(attrs={'readonly': 'readonly'})
    )
    fecha_fin = forms.DateField(
        widget=forms.TextInput(attrs={'readonly': 'readonly'})
    )
    observaciones = forms.CharField(
        widget=forms.TextInput(attrs={'readonly': 'readonly'})
    )
    estado = forms.CharField(
        widget=forms.TextInput(attrs={'readonly': 'readonly'})
    )
    class Meta:
        model = Proyecto
        fields = ('nombre', 'fecha_ini', 'fecha_fin', 'observaciones', 'estado')

class BuscarProyecto(Form):
    """
    Formulario para buscar Proyecto por nombre
    """
    nombre = CharField()


from django.contrib import admin
from .models import Us, Archivo, Nota, Daily

admin.site.register(Us)
admin.site.register(Archivo)
admin.site.register(Nota)
admin.site.register(Daily)
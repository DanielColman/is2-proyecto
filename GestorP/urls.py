"""GestorP URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url
from django.conf.urls import include
from django.urls import path # new para el logout
from django.contrib import auth

from django.conf import settings
from django.conf.urls.static import static

import menu

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/', include('accounts.urls')),
    path('accounts/', include('django.contrib.auth.urls')), # new para el logout
    url(r'^usuario/', include('usuario.urls')),
    url(r'^proyecto/', include('proyecto.urls')),
    url(r'^rol/', include('Rol.urls')),
    url(r'^menu/', include('menu.urls')),
    url(r'^$', menu.views.index),
    url(r'^team/', include('team.urls')),
    url(r'^us/', include('us.urls')),
    url(r'^tablero/', include('tablero.urls')),
    url(r'^sprint/', include('sprint.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

import os

def InData():
    #CREAR PERMISOS
    add_permiso(1,"Consultar Usuarios del Proyecto")
    add_permiso(2,"Crear Rol")
    add_permiso(3,"Modificar Rol")
    add_permiso(4,"Buscar Rol")
    add_permiso(5,"Consultar Rol")
    add_permiso(6,"Modificar Proyecto")
    add_permiso(7,"Inactivar Proyecto")
    add_permiso(8,"Consultar Proyecto")


def add_permiso(orden, nombre):
    p = Permiso(orden=orden,nombre=nombre)
    p.save()
    return p


if __name__ == '__main__':
    print ("Inicio de la poblacion de la base de datos")
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'GestorP.settings.dev')
    import django
    django.setup()
    from Rol.models import Permiso
    InData()
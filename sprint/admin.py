from django.contrib import admin
from .models import Sprint, Reunion, Horas

# Register your models here.
admin.site.register(Sprint)
admin.site.register(Reunion)
admin.site.register(Horas)
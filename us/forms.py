from django import forms
from .models import Archivo, Us, Nota, Daily


class UsForm(forms.ModelForm):
    """
    Formulario para la creación de un US
    """
    class Meta:
        model = Us
        """Campos a ser ingresados"""
        fields = ('descripcion_breve', 'descripcion_completa',
                  'tiempo_finalizacion', 'valor_negocio',
                  'prioridad', 'valor_tecnico',)
        widgets = {
            'descripcion_breve': forms.TextInput(attrs={'placeholder': 'Defina el nombre del US'}),
            'valor_negocio': forms.TextInput(attrs={'placeholder': 'Rango [0 - 10] - Siendo 0: Muy Baja y 10: Muy Alta'}),
            'valor_tecnico': forms.TextInput(attrs={'placeholder': 'Rango [0 - 10] - Siendo 0: Muy Baja y 10: Muy Alta'}),
            'prioridad': forms.TextInput(attrs={'placeholder': 'Rango [0 - 10] - Siendo 0: Muy Baja y 10: Muy Alta'})
        }

class ArchivoForm(forms.ModelForm):
    """
    Formulario para subir los archivos al US
    """
    class Meta:
        model = Archivo
        """Campos a ser ingresados"""
        fields = ('titulo', 'archivo', )

class NotaForm(forms.ModelForm):
    """
    Formulario para subir notas al US
    """
    class Meta:
        model = Nota
        """Campos que deben ingresarse"""
        fields = ('nota',)

class ModificarUsForm(forms.ModelForm):
    """Formulario para modificar un US
    """
    class Meta:
        model = Us
        """Campos a ser ingresados"""
        fields = ('descripcion_breve', 'descripcion_completa',
                  'tiempo_finalizacion', 'valor_negocio',
                  'prioridad', 'valor_tecnico',)

class DailyForm(forms.ModelForm):
    """
    Formulario para subir notas al US
    """
    class Meta:
        model = Daily
        """Campos a ser ingresados"""
        fields = ('tiempo',)
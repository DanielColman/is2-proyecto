from django.db.models import Q
from django.contrib import messages
from Rol.forms import RolForm, BuscarRol, UpdateRolForm
from Rol.models import Rol, Permiso
from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
from django.db import transaction

from django.contrib import messages
from django.http import HttpResponseForbidden


# Create your views here.
from proyecto.models import Proyecto
from team.models import TeamMember, Team


@login_required
@transaction.atomic
def index(request):
        if request.user.is_superuser:
            if request.method == 'POST':
                busqueda = request.POST['buscalo']
                """
                Filtra de acuerdo a la palabra que se indico 
                :param request:
                """
                if busqueda:
                    match = Rol.objects.filter(Q(nombre__icontains=busqueda))
                    if match:
                        return render(request, 'Rol/index.html', {'roles': match})
                    else:
                        messages.error(request, 'No existe un Rol con ese nombre')
                else:
                    matchh = Rol.objects.filter(Q(nombre__icontains=busqueda))
                    return render(request, 'Rol/index.html', {'roles': matchh.all()})
            else:
                roles = Rol.objects.all()
                return render(request, 'Rol/index.html', {'roles': roles})

            return render(request, 'Rol/index.html')
        else:
            return HttpResponseForbidden()


@login_required
@transaction.atomic
def list_rol(request, id_rol):
    """
    Esta view lista los datos del rol de acuerdo al id seleccionado
    :param request:
    """
    if request.user.is_superuser:
        rol = Rol.objects.filter(id=id_rol)
        contexto = {'roles': rol}
        return render(request, 'Rol/visualizar_rol.html', contexto)
    else:
        return HttpResponseForbidden()

@login_required
@transaction.atomic
def rol_view(request):
    """
    Esta view permite crear un Rol
    :param request:
    """
    if request.user.is_superuser:
        if request.method == 'POST':
            form = RolForm(request.POST)
            if form.is_valid():
                form.save()
                messages.success(request, ('Se creó el rol seleccionado!'))
                return redirect('/rol')
        else:
            form = RolForm()
        return render(request, 'Rol/Rol_form.html', {'form': form})
    else:
        return HttpResponseForbidden()

@login_required
@transaction.atomic
def rol_view2(request):
    """
    Esta view permite crear un Rol
    :param request:
    """
    if request.user.is_superuser:
        if request.method == 'POST':
            form = RolForm(request.POST)
            if form.is_valid():
                form.save()
                messages.success(request, ('Se creó el rol seleccionado!'))
                #form = RolForm()
                return redirect('/rol')
        else:
            form = RolForm()
        return render(request, 'Rol/Rol_form2.html', {'form': form})
    else:
        return HttpResponseForbidden()


@login_required
@transaction.atomic
def rol_edit(request, id_rol):
    """
    Esta view permite modificar un Rol
    :param request:
    """
    if request.user.is_superuser:
        rol = Rol.objects.get(id=id_rol)
        if request.method == 'GET':
            form = RolForm(instance=rol)
        else:
            form = RolForm(request.POST, instance=rol)
            if form.is_valid():
                form.save()
            messages.success(request, ('Se actualizó el rol seleccionado!'))
            return redirect('/rol')
        return render(request, 'Rol/rol_editar.html', {'form': form})
    else:
        return HttpResponseForbidden()

@login_required
@transaction.atomic
def rol_eliminar(request, id_rol):
    """
    Esta view permite borrar un Rol
    :param request:
    """
    if request.user.is_superuser:
        rol = Rol.objects.get(id=id_rol)
        if request.method == 'POST':
            rol.delete()
            messages.success(request, ('Se eliminó el rol seleccionado!'))
            return redirect('/rol')
        return render(request, 'Rol/rol_eliminar.html', {'rol': rol})
    else:
        return HttpResponseForbidden()

@login_required
@transaction.atomic
def rol_buscar(request):
    """
    Buscar un Rol por nombre
    :param request:
    """
    if request.user.is_superuser:
        if request.method == 'POST':
            busqueda = request.POST['buscalo']
            """
            Filtra de acuerdo a la palabra que se indico 
            :param request:
            """
            if busqueda:
                match = Rol.objects.filter(Q(nombre__contains=busqueda))
                if match:
                    return render(request, 'Rol/rol_buscar.html', {'sr': match})
                else:
                    messages.error(request, 'No existe un Rol con ese nombre')
            else:
                matchh = Rol.objects.filter(Q(nombre__contains=busqueda))
                return render(request, 'Rol/rol_buscar.html', {'sr': matchh.all()})
        else:
            roles = Rol.objects.all()
            return render(request, 'Rol/rol_buscar.html', {'sr': roles})

        return render(request, 'Rol/rol_buscar.html')
    else:
        return HttpResponseForbidden()

@login_required
@transaction.atomic
def modificar_rol(request, id_rol):
    """
    Esta view permite modificar un Rol a partir del menu principal
    :param request:
    """
    if request.user.is_superuser:
        rol = Rol.objects.get(id=id_rol)
        if request.method == 'POST' and 'save' in request.POST:
            # formulario enviado
            rol_form = UpdateRolForm(request.POST, instance=rol)
            if rol_form.is_valid():
                # formulario validado correctamente
                rol_form.save()
                if rol_form.has_changed():
                    messages.success(request, ('Se actualizó el Rol seleccionado!'))
                    return redirect('/rol')
                else:
                    messages.error(request, ('Corrija el error de abajo.'))
        elif request.method == 'POST' and 'eliminar' in request.POST:
            # borrar proyecto
            rolform = UpdateRolForm(request.POST, instance=rol)
            rol.delete()
            messages.success(request, ('Se eliminó el Rol seleccionado!'))
            return redirect('/rol')
        else:
            rol_form = UpdateRolForm(instance=rol)
        return render(request, 'Rol/update_rol.html', {'rol_form': rol_form})
    else:
        return HttpResponseForbidden()


@login_required
@transaction.atomic
def post_list(request):
    """
    Esta view lista los roles
    :param request:
    """
    if request.user.is_superuser:
        return render(request, 'Rol/post_list.html', {})
    else:
        return HttpResponseForbidden()


@login_required
@transaction.atomic
def confirmado (request, pk):
    """
    Esta view indica que una operación se realizó con éxito
    :param request:
    """
    return render(request, 'Rol/confirmado.html')

@login_required
@transaction.atomic
def confirmado2 (request):
    """
    Página que confirma que un proceso se realizó con éxito
    :param request:
    """
    return render(request, 'Rol/confirmado.html')

@login_required
@transaction.atomic
def confirmado3 (request):
    """
    Página que confirma que un proceso se realizó con éxito
    :param request:
    """
    return render(request, 'Rol/confirmado3.html')
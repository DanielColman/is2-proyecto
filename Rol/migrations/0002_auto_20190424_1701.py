# Generated by Django 2.0.4 on 2019-04-24 17:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Rol', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rol',
            name='permiso',
            field=models.ManyToManyField(to='Rol.Permiso'),
        ),
    ]

from django.db import models
from proyecto.models import Proyecto
from usuario.models import Usuario
from Rol.models import Rol

# Create your models here.
class TeamMember(models.Model):
    """
    Definimos el modelo TeamMember que está relacionado uno a uno con Usuario, Rol y team
    """
    id_usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    id_rol = models.ForeignKey(Rol, on_delete=models.CASCADE)

    def __str__(self):
        return self.id_usuario.user.username


class Team(models.Model):
    """
    Definimos el modelo team que está relacionado uno a uno con Proyecto.
    Definimos los campos id del proyecto
    """
    id_proyecto = models.OneToOneField(Proyecto, on_delete=models.CASCADE)
    #id_team_member = models.ManyToManyField(TeamMember, null=True)
    id_team_member = models.ManyToManyField(TeamMember)

    def __str__(self):
        return self.id_proyecto.nombre
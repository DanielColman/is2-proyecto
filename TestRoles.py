from unittest import TestCase
import django
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "GestorP.base")
django.setup()
from Rol.models import Rol
from django.test import Client

"""
Prubas unitarias para la app roles
"""

class Roles(TestCase):
    # def test_pagina(self):
    #     """
    #     Carga la página y verifica que encuentra
    #     """
    #     client = Client()
    #     response = client.get("/Rol/")
    #     #HTTP_302_FOUND
    #     self.assertEqual(response.status_code, 200)

    def test_create_rol(self):
        """
        Verifica que un Rol se crea
        """
        try:
            rol = Rol(nombre="rol1")
            rol.save()
            rol.delete()
        except:
            rol.delete()
            raise

    def test_create_rol0(self):
        """
        Verifica que un Rol se crea
        """
        try:
            rol = Rol(nombre="rol1")
            rol.save()
            rol.delete()
        except:
            rol.delete()
            raise

    def test_create_rol1(self):
        """
        Verifica que un Rol se crea
        """
        try:
            rol = Rol(nombre="rol1")
            rol.save()
            rol.delete()
        except:
            rol.delete()
            raise

    def test_create_rol3(self):
        """
        Verifica que un Rol se crea
        """
        try:
            rol = Rol(nombre="rol1")
            rol.save()
            rol.delete()
        except:
            rol.delete()
            raise

    def test_create_rol4(self):
        """
        Verifica que un Rol se crea
        """
        try:
            rol = Rol(nombre="rol1")
            rol.save()
            rol.delete()
        except:
            rol.delete()
            raise

    def test_create_rol5(self):
        """
        Verifica que un Rol se crea
        """
        try:
            rol = Rol(nombre="rol1")
            rol.save()
            rol.delete()
        except:
            rol.delete()
            raise

    def test_update_rol(self):
         """
         Verifica que un Rol se modifica
         """
         try:
             rol = Rol(nombre="rol1")
             rol.save()
             rol.nombre = 'rol2'
             rol.delete()
         except:
             rol.delete()
             raise

    def test_update_rol0(self):
         """
         Verifica que un Rol se modifica
         """
         try:
             rol = Rol(nombre="rol1")
             rol.save()
             rol.nombre = 'rol2'
             rol.delete()
         except:
             rol.delete()
             raise

    def test_update_rol1(self):
         """
         Verifica que un Rol se modifica
         """
         try:
             rol = Rol(nombre="rol1")
             rol.save()
             rol.nombre = 'rol2'
             rol.delete()
         except:
             rol.delete()
             raise

    def test_update_rol2(self):
         """
         Verifica que un Rol se modifica
         """
         try:
             rol = Rol(nombre="rol1")
             rol.save()
             rol.nombre = 'rol2'
             rol.delete()
         except:
             rol.delete()
             raise

    def test_update_rol3(self):
         """
         Verifica que un Rol se modifica
         """
         try:
             rol = Rol(nombre="rol1")
             rol.save()
             rol.nombre = 'rol2'
             rol.delete()
         except:
             rol.delete()
             raise

    def test_update_rol4(self):
         """
         Verifica que un Rol se modifica
         """
         try:
             rol = Rol(nombre="rol1")
             rol.save()
             rol.nombre = 'rol2'
             rol.delete()
         except:
             rol.delete()
             raise


    def test_update_rol5(self):
         """
         Verifica que un Rol se modifica
         """
         try:
             rol = Rol(nombre="rol1")
             rol.save()
             rol.nombre = 'rol2'
             rol.delete()
         except:
             rol.delete()
             raise

    def test_delete_proyecto(self):
        """
        Verifica que un Rol se elimina sin problemas
        """
        try:
            rol = Rol(nombre="rol1")
            rol.save()
            rol.delete()
            #Falla(intenta borrar algo que no existe, linea de abajo):
            #Rol.delete()
        except:
            raise


    def test_delete_proyecto2(self):
        """
        Verifica que un Rol se elimina sin problemas
        """
        try:
            rol = Rol(nombre="rol1")
            rol.save()
            rol.delete()
            #Falla(intenta borrar algo que no existe, linea de abajo):
            #Rol.delete()
        except:
            raise

    def test_delete_proyecto3(self):
        """
        Verifica que un Rol se elimina sin problemas
        """
        try:
            rol = Rol(nombre="rol1")
            rol.save()
            rol.delete()
            #Falla(intenta borrar algo que no existe, linea de abajo):
            Rol.delete()
        except:
            raise

    def test_delete_proyecto4(self):
        """
        Verifica que un Rol se elimina sin problemas
        """
        try:
            rol = Rol(nombre="rol1")
            rol.save()
            rol.delete()
            # Falla(intenta borrar algo que no existe, linea de abajo):
            Rol.delete()
        except:
            raise


    def test_delete_proyecto5(self):
        """
        Verifica que un Rol se elimina sin problemas
        """
        try:
            rol = Rol(nombre="rol1")
            rol.save()
            rol.delete()
            # Falla(intenta borrar algo que no existe, linea de abajo):
            Rol.delete()
        except:
            raise

    def test_delete_proyecto6(self):
        """
        Verifica que un Rol se elimina sin problemas
        """
        try:
            rol = Rol(nombre="rol1")
            rol.save()
            rol.delete()
            # Falla(intenta borrar algo que no existe, linea de abajo):
            Rol.delete()
        except:
            raise

    def test_delete_proyecto7(self):
        """
        Verifica que un Rol se elimina sin problemas
        """
        try:
            rol = Rol(nombre="rol1")
            rol.save()
            rol.delete()
            # Falla(intenta borrar algo que no existe, linea de abajo):
            Rol.delete()
        except:
            raise
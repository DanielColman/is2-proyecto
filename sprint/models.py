from django.db import models

# Create your models here.
from django.db.models import PROTECT

from proyecto.models import Proyecto
from team.models import TeamMember

ESTADOS = (
    ('ACT', 'Activo'), # cuando se inicia
    ('TER', 'Terminado'), # cuando se termina
)

class Sprint(models.Model):
    """
    Modelo de la clase Sprint
    """
    estado = models.CharField(max_length=3, choices=ESTADOS, default='ACT')
    proyecto = models.ForeignKey(Proyecto, on_delete=PROTECT, null=True, blank=True)
    fecha_ini = models.DateField(verbose_name='fecha de inicio', null=True, blank=True)
    fecha_fin = models.DateField(verbose_name='fecha de finalizacion', null=True, blank=True)
    fecha_fin_est = models.DateField(verbose_name='fecha de finalizacion estimada', null=True, blank=True)
    team_sprint = models.ManyToManyField(TeamMember, through='Horas')
    para_importar = models.TextField(null=True, blank=True)

class Horas(models.Model):
    """
    Modelo de la clase Horas, la cual es una relación entre el Sprint
    y un Teammember que pertenece al Proyecto del Sprint.
    Almacena las horas que debera el usuario por Sprint.
    """
    sprint = models.ForeignKey(Sprint, on_delete=models.CASCADE)
    teammember = models.ForeignKey(TeamMember, on_delete=models.CASCADE)
    horas_trabajadas = models.IntegerField(default=0)

    def __str__(self):
        return self.teammember.id_usuario.user.username
class Reunion(models.Model):
    """
    Modelo de la clase Reuniones Retrospectivas: notas añadidas a cada Sprint
    """
    reunion = models.TextField()
    sprint = models.OneToOneField(Sprint, null=True, blank=True, on_delete=PROTECT)
    class Meta:
        verbose_name_plural = "Reuniones"

    def __str__(self):
        return self.reunion
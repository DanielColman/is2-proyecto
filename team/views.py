from django.shortcuts import render, redirect
from .models import Team,TeamMember
from .forms import TeamMemberForm, TeamForm
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.db import transaction


# Create your views here.
@login_required
@transaction.atomic
def crear_teamMember(request):
    """
    Esta view permite la creacion de un team member
    :param request:
    """
    if request.method == "POST":
        form = TeamMemberForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, ('Se creó el TeamMember seleccionado!'))
    else:
        form = TeamMemberForm()

    return render(request, 'team/create_team_member.html', {'form': form})

@login_required
@transaction.atomic
def crear_team(request):
    """
    Esta view permite la creacion de un team
    :param request:
    """
    if request.method == "POST":
        form = TeamForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, ('Se creó el Team seleccionado!'))
            return redirect('/team')
    else:
        form = TeamForm()

    return render(request, 'team/create_team.html', {'form': form})

@login_required
@transaction.atomic
def menu_team(request):
    """
    Esta view muestra el menu de los team
    :param request:
    """
    teams = Team.objects.all()
    teammembers=TeamMember.objects.all()
    return render(request, 'team/team.html', {'teams':teams, 'teammembers':teammembers})

@login_required
@transaction.atomic
def update_team(request, pk):
    """
    Esta view permite modificar un team
    :param request:
    """
    team = Team.objects.get(pk=pk)
    if request.method == 'POST' and 'save' in request.POST:
        form = TeamForm(request.POST, instance=team)
        if form.is_valid():
            form.save()
            messages.success(request, ('Se actualizo el team seleccionado!'))
            return redirect('/team')
        else:
            messages.error(request, ('Corrija el error de abajo.'))
    elif request.method == 'POST' and 'eliminar' in request.POST:
        team.delete()
        messages.success(request, ('Se elimino el team seleccionado!'))
        return redirect('/team')
    else:
        form = TeamForm(instance=team)
    return render(request, 'team/modificar_team.html', {
        'form': form,
        'team': team
    })

@login_required
@transaction.atomic
def update_team_member(request, pk):
    """
    Esta view permite realizar modificaciones a un team member
    :param request:
    """
    teammember = TeamMember.objects.get(pk=pk)
    if request.method == 'POST' and 'save' in request.POST:
        form = TeamMemberForm(request.POST, instance=teammember)
        if form.is_valid():
            form.save()
            messages.success(request, ('Se actualizo el team member seleccionado!'))
            return redirect('/team')
        else:
            messages.error(request, ('Corrija el error de abajo.'))
    elif request.method == 'POST' and 'eliminar' in request.POST:
        teammember.delete()
        messages.success(request, ('Se elimino el team member seleccionado!'))
        return redirect('/team')
    else:
        form = TeamMemberForm(instance=teammember)
    return render(request, 'team/modificar_team_member.html', {
        'form': form,
        'teammember': teammember
    })
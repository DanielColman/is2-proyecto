from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.db.models import Q
from django.shortcuts import render, redirect
from django.http import HttpResponseForbidden

from proyecto.models import Proyecto
from tablero.models import Tablero, Fase
from team.models import Team, TeamMember
from us.models import Us
from .forms import TableroForm, FaseForm

@login_required
@transaction.atomic
# === Metodo Ver Tablero ===
def ver_tablero(request, pk):
    """
    Campos del metodo:
    Permite ver un Tipo de User Story

    1. ** request **: describe el objeto HttpRequest que representa la metadata de la solicitud HTTP Response

    """

    tablero = Tablero.objects.get(pk=pk)
    proyecto = Proyecto.objects.get(pk=tablero.proyecto.pk)
    usuario = request.user
    super = False
    if request.user.is_superuser:
        super=True
    teammember = TeamMember.objects.filter(id_usuario=request.user.usuario.id)
    teammember = list(teammember)
    permitido = False
    is_scrum = False
    us_proyecto = []
    rol_usuario = []
    try:
        for i in range(len(teammember)):
            try:
                team = Team.objects.get(id_team_member=teammember[i].id)
            except:
                continue
            if team.id_proyecto_id == proyecto.id:
                if teammember[i].id_rol.nombre == 'scrum':
                    is_scrum = True
                    permitido = True
    except:
        if not request.user.is_superuser:
            return HttpResponseForbidden()
    try:
        team_pertenece = Team.objects.get(id_team_member__id_usuario=usuario.usuario, id_proyecto=proyecto)
        uss = Us.objects.filter(tablero=tablero).order_by('id')
        fases = Fase.objects.filter(tablero=tablero).order_by('orden')

        cant_fases1 = list(fases)
        cant_fases = len(cant_fases1)
        return render(request, 'tablero/ver_tablero.html',{'uss':uss, 'fases':fases,'usuario':usuario, 'proyecto':proyecto, 'tablero':tablero,'super':super, 'cant_fases':cant_fases, 'is_scrum':is_scrum})
    except:
        return HttpResponseForbidden()
@login_required
@transaction.atomic

# === Metodo Ver Tablero ===
def crear(request,pk):

    """
    Campos del metodo:
    Permite crear un Tipo de User Story

    1. ** request **: describe el objeto HttpRequest que representa la metadata de la solicitud HTTP Response

    """
    teammember = TeamMember.objects.filter(id_usuario=request.user.usuario.id)
    teammember = list(teammember)
    permitido = False
    is_scrum = False
    us_proyecto = []
    rol_usuario = []
    proyecto = Proyecto.objects.get(id=pk)
    try:
        for i in range(len(teammember)):
            try:
                team = Team.objects.get(id_team_member=teammember[i].id)
            except:
                continue
            if team.id_proyecto_id == proyecto.id:
                if teammember[i].id_rol.nombre == 'scrum':
                    is_scrum = True
                    permitido = True
    except:
        if not request.user.is_superuser:
            return HttpResponseForbidden()

    super = False
    if request.user.is_superuser:
        super = True
        permitido = True

    if permitido:
        if request.method == 'POST':
            form = TableroForm(request.POST)
            if form.is_valid():

                obj = form.save(commit=False)
                obj.proyecto = Proyecto.objects.get(id=pk)
                obj.save()
                messages.success(request, 'Se creó correctamente el Tipo de US')
                return redirect('/tablero/crearfase/'+str(obj.id))
        else:
            form = TableroForm()
        return render(request, 'tablero/crear.html', {'form':form, 'proyecto':proyecto})
    else:
        return HttpResponseForbidden()

@login_required
@transaction.atomic

# === Metodo Crear Fase ===
def crear_fase(request, pk):
    """
    Campos del metodo:
    Permite crear una fase al tipo de User Story

    1. ** request **: describe el objeto HttpRequest que representa la metadata de la solicitud HTTP Response
    2. ** pk es el id el Tipo de US **

    """
    teammember = TeamMember.objects.filter(id_usuario=request.user.usuario.id)
    teammember = list(teammember)
    permitido = False
    is_scrum = False
    us_proyecto = []
    rol_usuario = []
    tablero = Tablero.objects.get(id=pk)
    proyecto = tablero.proyecto
    try:
        for i in range(len(teammember)):
            try:
                team = Team.objects.get(id_team_member=teammember[i].id)
            except:
                continue
            if team.id_proyecto_id == proyecto.id:
                if teammember[i].id_rol.nombre == 'scrum':
                    is_scrum = True
                    permitido = True
    except:
        if not request.user.is_superuser:
            return HttpResponseForbidden()

    super = False
    if request.user.is_superuser:
        super = True
        permitido = True

    nro_fase = Fase.objects.filter(tablero=tablero)
    nro_fase = len(list(nro_fase))

    if permitido:
        if request.method == 'POST':
            form = FaseForm(request.POST)
            if form.is_valid():
                obj = form.save(commit=False)
                obj.tablero = Tablero.objects.get(id=pk)
                nro_fase = Fase.objects.filter(tablero=obj.tablero)
                nro_fase = len(list(nro_fase))

                sgte=0
                if nro_fase == 0:

                    sgte=1
                else:
                    sgte = nro_fase+1
                obj.orden = sgte
                obj.save()

                messages.success(request, 'Se creó correctamente la Fase')
                return redirect('/tablero/continuar/'+str(pk))
        else:
            form = FaseForm()
        return render(request, 'tablero/crearfase.html', {'form': form, 'tablero':tablero, 'proyecto':proyecto, 'nro_fase':nro_fase,})
    else:
        return HttpResponseForbidden()

@login_required
@transaction.atomic
def crear_fase2(request, pk):
    """
       Campos del metodo:
       Permite crear una fase al tipo de User Story

       1. ** request **: describe el objeto HttpRequest que representa la metadata de la solicitud HTTP Response
       2. ** pk es el id el Tipo de US **

       """

    teammember = TeamMember.objects.filter(id_usuario=request.user.usuario.id)
    teammember = list(teammember)
    permitido = False
    is_scrum = False
    us_proyecto = []
    rol_usuario = []
    tablero = Tablero.objects.get(id=pk)
    proyecto = tablero.proyecto
    try:
        for i in range(len(teammember)):
            try:
                team = Team.objects.get(id_team_member=teammember[i].id)
            except:
                continue
            if team.id_proyecto_id == proyecto.id:
                if teammember[i].id_rol.nombre == 'scrum':

                    is_scrum = True
                    permitido = True
    except:
        if not request.user.is_superuser:
            return HttpResponseForbidden()

    super = False
    if request.user.is_superuser:
        super = True
        permitido = True

    if permitido:

        return render(request, 'tablero/crearfase2.html',{'pk':pk, 'tablero':tablero})
    else:
        return HttpResponseForbidden()

@login_required
@transaction.atomic
def modificar_fase(request,pk):
    """
       Campos del metodo:
       Permite modificar una fase al tipo de User Story

       1. ** request **: describe el objeto HttpRequest que representa la metadata de la solicitud HTTP Response
       2. ** pk es el id del Tipo de US **

       """
    teammember = TeamMember.objects.filter(id_usuario=request.user.usuario.id)
    teammember = list(teammember)
    permitido = False
    is_scrum = False
    us_proyecto = []
    rol_usuario = []
    fase = Fase.objects.get(id=pk)
    tablero = fase.tablero
    proyecto = tablero.proyecto
    try:
        for i in range(len(teammember)):
            try:
                team = Team.objects.get(id_team_member=teammember[i].id)
            except:
                continue
            if team.id_proyecto_id == proyecto.id:
                if teammember[i].id_rol.nombre == 'scrum':
                    is_scrum = True
                    permitido = True
    except:
        return HttpResponseForbidden()

    if permitido:

        fase = Fase.objects.get(id=pk)
        if request.method == 'POST':
            form = FaseForm(request.POST, instance=fase)
            if form.is_valid():
                form.save()

                messages.success(request, 'Se modificó correctamente la Fase')
                return redirect('/tablero/listo')
        else:
            form = FaseForm(instance=fase)
        return render(request, 'tablero/modificar_fase.html', {'form':form, 'fase':fase})
    else:
        return HttpResponseForbidden()
@login_required
@transaction.atomic
def modificar_tablero(request,pk):
    """
       Campos del metodo:
       Permite modificacion del tablero al tipo de User Story

       1. ** request **: describe el objeto HttpRequest que representa la metadata de la solicitud HTTP Response
       2. ** pk es el id el Tipo de US **

       """

    teammember = TeamMember.objects.filter(id_usuario=request.user.usuario.id)
    teammember = list(teammember)
    permitido = False
    is_scrum = False
    us_proyecto = []
    rol_usuario = []
    tablero = Tablero.objects.get(id=pk)
    proyecto = tablero.proyecto
    try:
        for i in range(len(teammember)):
            try:
                team = Team.objects.get(id_team_member=teammember[i].id)
            except:
                continue
            if team.id_proyecto_id == proyecto.id:
                if teammember[i].id_rol.nombre == 'scrum':
                    is_scrum = True
                    permitido = True
    except:
        if not request.user.is_superuser:
            return HttpResponseForbidden()

    super = False
    if request.user.is_superuser:
        super = True
        permitido = True

    if permitido:

        tablero = Tablero.objects.get(id=pk)
        if request.method == 'POST':
            form = FaseForm(request.POST, instance=tablero)
            if form.is_valid():
                form.save()
                messages.success(request, 'Se modificó correctamente la Fase')
                return redirect('/tablero/listo')
        else:
            form = FaseForm(instance=tablero)
        return render(request, 'tablero/modificar_tablero.html', {'form': form, 'tablero': tablero})
    else:
        return HttpResponseForbidden()
@login_required
@transaction.atomic
def crearfasefinal(request,pk):
    """
       Campos del metodo:
       Permite la creacion de la fase final al tipo de User Story

       1. ** request **: describe el objeto HttpRequest que representa la metadata de la solicitud HTTP Response
       2. ** pk es el id el Tipo de US **

       """

    teammember = TeamMember.objects.filter(id_usuario=request.user.usuario.id)
    teammember = list(teammember)
    permitido = False
    is_scrum = False
    us_proyecto = []
    rol_usuario = []
    tablero = Tablero.objects.get(id=pk)
    proyecto = tablero.proyecto
    try:
        for i in range(len(teammember)):
            try:
                team = Team.objects.get(id_team_member=teammember[i].id)
            except:
                continue
            if team.id_proyecto_id == proyecto.id:
                if teammember[i].id_rol.nombre == 'scrum':
                    is_scrum = True
                    permitido = True
    except:
        if not request.user.is_superuser:
            return HttpResponseForbidden()

    super = False
    if request.user.is_superuser:
        super = True
        permitido = True

    if permitido:
        """Verfica que tenga el permiso requerido"""
        if request.method == 'POST':
            form = FaseForm(request.POST)
            if form.is_valid():
                obj = form.save(commit=False)
                obj.tablero = Tablero.objects.get(id=pk)
                """Se obtene el numero de la fase"""
                nro_fase = Fase.objects.filter(tablero=obj.tablero)
                nro_fase = len(list(nro_fase))
                """Se lista las fases para visaulizarlas"""
                sgte=0
                if nro_fase == 0:
                    sgte=1
                else:
                    sgte = nro_fase+1
                obj.orden = sgte
                obj.save()
                """Verifica que tenga el permiso requerido"""
                messages.success(request, 'Se creó correctamente la Fase')
                return redirect('/tablero/listo')
        else:
            form = FaseForm()
        return render(request, "tablero/crearfasefinal.html", {'form':form})
    return HttpResponseForbidden()

@login_required
@transaction.atomic
def listo(request):
    """
          Ventana que visualiza Listo
          :param request: objeto HttpRequest que representa la metadata de la solicitud HTTP
          :return:
          """
    return render(request, 'tablero/listo.html')

@login_required
@transaction.atomic
def importar(request, pk):
    """
          Permite importar un Tipo de User Story a un Tablero
          :param request: objeto HttpRequest que representa la metadata de la solicitud HTTP
          :return:
          """
    teammember = TeamMember.objects.filter(id_usuario=request.user.usuario.id)
    teammember = list(teammember)
    permitido = False
    is_scrum = False
    us_proyecto = []
    rol_usuario = []
    proyecto = Proyecto.objects.get(id=pk)
    try:
        for i in range(len(teammember)):
            try:
                team = Team.objects.get(id_team_member=teammember[i].id)
            except:
                continue
            if team.id_proyecto_id == proyecto.id:
                if teammember[i].id_rol.nombre == 'scrum':
                    is_scrum = True
                    permitido = True
    except:
        if not request.user.is_superuser:
            return HttpResponseForbidden()

    super = request.user.is_superuser

    if permitido:
        #proyectos = Proyecto.objects.filter(~Q(id = pk))
        tableros = Tablero.objects.all()
        destino = Proyecto.objects.get(id=pk)
        return render(request, 'tablero/importar.html',{'tableros':tableros, 'destino':destino, 'super':super})
    else:
        return HttpResponseForbidden()

@login_required
@transaction.atomic
def importar2(request, pk, origen):
    """
          Permite importar un Tipo de User Story a un Proyecto
          :param request: objeto HttpRequest que representa la metadata de la solicitud HTTP
          PK: es el ID del proyecto destino,
          Origen: es el ID del proyecto origen
          :return:
          """
    # permisos
    if request.user.is_superuser:
        tablero = Tablero.objects.get(id=origen)
        """Es el Tipo de User Story del cual se importara usando como parametro el id"""
        proyecto_destino = Proyecto.objects.get(id=pk)
        """Es el proyecto al cual se importara el Tipo de User Story usando como parametro el id"""
        destino = Tablero(nombre=tablero.nombre,descripcion=tablero.descripcion,proyecto=proyecto_destino)
        destino.save()
        """Es guarda correctamente la importacion"""
        fases_origen = Fase.objects.filter(tablero=tablero).order_by('orden')
        """Se listan las fases delTipo de User Story"""
        fases_origen = list(fases_origen)
        orden = 1
        """
              Al elegir un Tipo de US se copian las fase del proyecto importado
              """
        for fase in fases_origen:
            nva_fase = Fase(nombre=fase.nombre, descripcion=fase.descripcion, tablero=destino, orden=orden)
            nva_fase.save()
            orden = orden + 1
        messages.success(request,'Se importó correctamente el Tipo de User Story')
        return redirect('/proyecto/trabajo/'+str(proyecto_destino.id))
        return render(request, 'tablero/importar2.html',)
    else:
        return HttpResponseForbidden()
@login_required
@transaction.atomic
def importado(request):
    """Vista que visualiza que se importo correctamente el Tipo de User Story"""
    # permisos
    if request.user.is_superuser:
        return render(request, 'tablero/importado.html')
    else:
        return HttpResponseForbidden()

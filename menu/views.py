from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.shortcuts import render
from team.models import Team, TeamMember
from proyecto.models import Proyecto
from django.contrib.auth.models import User
from django.http import HttpResponseForbidden


@login_required
@transaction.atomic
def menu_admin(request):
    """
    Esta view muestra el menú del administrador
    """
    usuario_conectado = User.objects.get(username__exact=request.user.username)
    team_pertenece = Team.objects.filter(id_team_member__id_usuario=usuario_conectado.usuario)
    proyectos = []
    for i in range(len(team_pertenece)):
        proyectos.append(team_pertenece[i].id_proyecto)
    super = False
    if request.user.is_superuser:
        super=True
    return render(request, 'menu/menu_admin.html', {'Proyectos': proyectos, 'super':super})


# @login_required
# @transaction.atomic
# def menu_usuario(request):
#     """
#     Esta view muestra el menu de usuario
#     """
#     usuario_conectado = User.objects.get(username__exact=request.user.username)
#     team_pertenece=Team.objects.filter(id_team_member__id_usuario=usuario_conectado.usuario)
#     return render(request, 'menu/menu_usuario.html', {'Proyectos': team_pertenece})

@login_required
@transaction.atomic
def index(request):
    """
    Esta view muestra el menú inicial
    """
    return render(request, 'menu/index.html', { })


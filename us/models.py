from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import PROTECT

from proyecto.models import Proyecto
from sprint.models import Sprint
from tablero.models import Tablero, Fase
from django.contrib.auth.models import User


def rango(valor):
    """Validación de rango permitido"""
    if valor >10 or valor < 0:
        raise ValidationError('El valor debe estar en 0-10, pero %s no cumple esa condición ' % valor)

def rango_estado(valor):
    """Validación de rango de Estado permitido"""
    if valor >3 or valor < 0:
        raise ValidationError('El valor debe estar en 0-3, pero %s no cumple esa condición ' % valor)

class Us(models.Model):
    """Modelo de US"""
    """Campos:"""
    descripcion_breve = models.CharField(max_length=50)
    descripcion_completa = models.TextField()
    tiempo_finalizacion = models.PositiveIntegerField(default=0)
    valor_negocio = models.PositiveIntegerField(validators=[rango])
    prioridad = models.PositiveIntegerField(validators=[rango])
    valor_tecnico = models.PositiveIntegerField(validators=[rango])
    prioridad_promedio = models.IntegerField(default=0,null=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    """
        Relación uno a uno entre tablero y US. Un US pertenece a 
        un solo Tablero.
    """
    tablero = models.ForeignKey(Tablero, on_delete=models.CASCADE, null = True, blank=True)
    """
        Relación uno a uno entre fase y US. Un US se 
        puede encontrar en una sola Fase.
    """
    fase = models.ForeignKey(Fase, on_delete=models.CASCADE, null=True, blank=True)
    estado = models.IntegerField(default=0, validators=[rango_estado])
    """
        Relación uno a uno entre Usuario y US. Un US 
        está asignado a un solo Usuario.
    """
    usuario = models.ForeignKey(User, on_delete=PROTECT, null=True, blank=True)
    proyecto = models.ForeignKey(Proyecto, on_delete=PROTECT, null=True, blank=True)
    """
    0: sprint sin haber pasado por control de calidad (no aprobado)
    1: sprint aprobado
    2: sprint no aprobado luego de haber pasado por control de calidad
    """
    sprint = models.ForeignKey(Sprint, on_delete=PROTECT, null=True, blank=True)
    """
    0: US RECIEN CREADO sin haber pasado por control de calidad (no aprobado)
    1: US aprobado
    2: US no aprobado luego de pasar por control de calidad
    3: US que quedó por el camino ( o sea ya inicia pero no llegó a control de  calidad)
    """
    aprobado= models.IntegerField(default=0)


    def __str__(self):
        return self.descripcion_breve

    def get_prioridad_promedio(self):
        """formula para calcular la prioridad promedio de un US"""
        return (self.valor_negocio + self.prioridad + 2*self.valor_tecnico)/4

class Nota(models.Model):
    """
        Clase para adjuntar una nota a un US
    """
    """Campos:"""
    nota = models.TextField()
    """ForeignKey porque una nota puede pertenecer a un solo US"""
    us = models.ForeignKey(Us, on_delete=models.CASCADE, null = True)
    usuario = models.ForeignKey(User, on_delete=PROTECT, null=True)
    fecha = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.nota

class Archivo(models.Model):
    """
           Clase para adjuntar un archivo a un US
       """
    """Campos:"""
    titulo = models.CharField(max_length=255, blank=True)
    archivo = models.FileField(upload_to='')
    fecha = models.DateTimeField(auto_now_add=True)
    """ForeignKey porque un archivo puede pertenecer a un solo US"""
    us = models.ForeignKey(Us, on_delete=models.CASCADE, null=True)
    usuario = models.ForeignKey(User, on_delete=PROTECT, null=True)

    def __str__(self):
        return self.titulo

class Daily(models.Model):
    """
    Modelo de la clase Cambio de estado, que contendrá un reporte acerca de cada cambio de estado y fase de un US
    """
    us = models.ForeignKey(Us, on_delete=models.CASCADE, null=True)
    daily = models.TextField()
    tiempo = models.PositiveIntegerField(default=0)
    avanza = models.BooleanField(default=True)
    fecha = models.DateTimeField(auto_now_add=True)
    """Estado y fases actuales (antes de moverse en el tipo de user story)"""
    estado = models.IntegerField(default=0)
    fase = models.ForeignKey(Fase, on_delete=models.CASCADE, unique=False)
    usuario = models.ForeignKey(User, on_delete=PROTECT, null=True, blank=True, unique=False)
    sprint = models.ForeignKey(Sprint, on_delete=PROTECT, null=True, blank=True)
from django.conf.urls import include, url
from . import views

"""
Lista de url de Rol
"""

urlpatterns = [
        url(r'^$', views.index),
        url(r'^listar$', views.list_rol, name='rol_listar'),
        url(r'^confirmado$', views.confirmado, name='confirmado'),
        url(r'^nuevo$', views.rol_view, name='rol_crear'),
        url(r'^editar/(?P<id_rol>\d+)/$', views.rol_edit, name='rol_modificar'),
        url(r'^eliminar/(?P<id_rol>\d+)/$', views.rol_eliminar, name='rol_eliminar'),
        url(r'^eliminar/(?P<pk>[0-9]+)/confirmado.html$', views.confirmado, name='confirmado'),
        url(r'^buscar/$', views.rol_buscar, name='rol_buscar'),
        url(r'^rol_buscar/$', views.rol_buscar, name='rol_buscar'),
]
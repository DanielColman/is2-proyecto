from django.conf.urls import url
from . import views

"""
Lista de URLs de Sprints
"""

urlpatterns = [
    url(r'^iniciar/(?P<proyecto_id>[0-9]+)/$', views.iniciar),
    url(r'^iniciar2/(?P<proyecto_id>[0-9]+)/$', views.iniciar2),
    url(r'^horas/(?P<teammember_id>[0-9]+)/(?P<proyecto_id>[0-9]+)/$', views.horas_disponibles),
    url(r'^finalizar/(?P<sprint_id>[0-9]+)/$', views.finalizar),
    url(r'^reunion/(?P<sprint_id>[0-9]+)/$', views.addReunion),
    url(r'^ver_reuniones/(?P<sprint_id>[0-9]+)/$', views.verReuniones),
    url(r'^control_calidad/(?P<sprint_id>[0-9]+)/$', views.control_calidad),
    url(r'^control_calidad_mover/(?P<sprint_id>[0-9]+)/$', views.control_calidad_mover),
    url(r'^elegir_para_importar/(?P<sprint_destino>[0-9]+)/$', views.elegir_para_importar),
    url(r'^importar/(?P<sprint_origen>[0-9]+)/(?P<sprint_destino>[0-9]+)/$', views.importar_sprint),
    url(r'^sprints_anteriores/(?P<id_proyecto>[0-9]+)', views.sprints_anteriores, name='sprints_anteriores'),
]
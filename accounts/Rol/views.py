from django.db.models import Q
from django.contrib import messages
from Rol.forms import RolForm, BuscarRol
from Rol.models import Rol, Permiso
from django.shortcuts import render, redirect
from django.views.generic import TemplateView

from django.contrib import messages
# Create your views here.

def index(request):
    """
    Esta view muestra el menu de Roles
    :param request:
    """
    return render(request, 'Rol/index.html')

def list_rol(request):
    """
    Esta view lista los Roles existentes
    :param request:
    """
    role = Rol.objects.all().order_by('id')
    contexto = {'roles': role}
    return render(request, 'Rol/visualizar_rol.html', contexto)

def rol_view(request):
    """
    Esta view permite crear un Rol
    :param request:
    """
    if request.method == 'POST':
        form = RolForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, ('Se creó el Rol seleccionado!'))
            return redirect('confirmado.html')
    else:
        form = RolForm()
    return render(request, 'Rol/Rol_form.html', {'form': form})

def rol_edit(request, id_rol):
    """
    Esta view permite modificar un Rol
    :param request:
    """
    rol = Rol.objects.get(id=id_rol)
    if request.method == 'GET':
        form = RolForm(instance=rol)
    else:
        form = RolForm(request.POST, instance=rol)
        if form.is_valid():
            form.save()
        messages.success(request, ('Se actualizo el Rol seleccionado!'))
        return redirect('confirmado')
    return render(request, 'Rol/rol_editar.html', {'form': form})

def rol_eliminar(request, id_rol):
    """
    Esta view permite borrar un Rol
    :param request:
    """
    rol = Rol.objects.get(id=id_rol)
    if request.method == 'POST':
        rol.delete()
        messages.success(request, ('Se elimino el perfil seleccionado!'))
        return redirect('confirmado')
    return render(request, 'Rol/rol_eliminar.html', {'Rol': rol})

def rol_buscar(request):
    """
    Buscar un Rol por nombre
    :param request:
    """
    if request.method == 'POST':
        busqueda = request.POST['buscalo']
        """
        Filtra de acuerdo a la palabra que se indico 
        :param request:
        """
        if busqueda:
            match = Rol.objects.filter(Q(nombre__contains=busqueda))
            if match:
                return render(request, 'Rol/rol_buscar.html', {'sr': match})
            else:
                messages.error(request, 'No existe un Rol con ese nombre')
        else:
            return redirect("/Rol/buscar/")

    return render(request, 'Rol/rol_buscar.html')

def post_list(request):
    """
    Esta view lista los roles
    :param request:
    """
    return render(request, 'Rol/post_list.html', {})

def confirmado (request):
    """
    Esta view indica que una operación fué realizada con éxito
    :param request:
    """
    return render(request, 'Rol/confirmado.html')
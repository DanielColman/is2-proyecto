from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.mail import EmailMessage
from django.db import transaction
from django.db.models import Q
from django.shortcuts import render, redirect
from usuario.forms import UserForm, UsuarioForm, CreateUserForm, CambiarPasswordForm, BuscarUsername, BuscarCI, \
    UserForm2
from .models import Usuario
from django.http import HttpResponseForbidden


"""
Las views para la administración de usuario
"""

@login_required
@transaction.atomic
def update_usuario(request, pk):
    """
    Esta view permite editar (o eliminar) un User
    :param request:
    :param pk:
    """
    if request.user.is_superuser:
        user = User.objects.get(pk=pk)
        if request.method == 'POST' and 'save' in request.POST:
            user_form = UserForm(request.POST,instance=user)
            usuario_form = UsuarioForm(request.POST ,instance=user.usuario)
            if user_form.is_valid() and usuario_form.is_valid():
                user_form.save()
                usuario_form.save()
                messages.success(request, ('Se actualizó el usuario seleccionado!'))
                return redirect('/usuario')
            else:
                messages.error(request, ('Corrija el error de abajo.'))
        elif request.method == 'POST' and 'eliminar' in request.POST:
            user_form = UserForm(request.POST, instance=user)
            usuario_form = UsuarioForm(request.POST, instance=user.usuario)
            user.delete()
            messages.success(request, ('Se eliminó el usuario seleccionado!'))
            return redirect('/usuario')
        else:
            user_form = UserForm(instance=user)
            usuario_form = UsuarioForm(instance=user.usuario)
        return render(request, 'usuario/update_usuario.html', {
            'user_form': user_form,
            'usuario_form': usuario_form,
            'user':user
        })
    else:
        return HttpResponseForbidden()

@login_required
@transaction.atomic
def eliminar_usuario(request, pk):
    """
    Esta view permite borrar un proyecto
    :param request:
    """
    if request.user.is_superuser:
        user = User.objects.get(pk=pk)
        if request.method == 'POST':
            user.delete()
            messages.success(request, ('Se eliminó el usuario seleccionado!'))
            return redirect('/usuario')
        return render(request, 'usuario/delete_usuario.html', {'user': user})
    return HttpResponseForbidden()

@login_required
@transaction.atomic
def crear_usuario(request):
    """
    Esta view permite crear un User
    :param request:
    """
    if request.user.is_superuser:
        if request.method == "POST":
            form = CreateUserForm(request.POST)
            form1 = UserForm2(request.POST)
            form2 = UsuarioForm(request.POST)
            if form.is_valid() and form1.is_valid() and form2.is_valid():
                passw = form.cleaned_data['password']
                new_user = User.objects.create_user(**form.cleaned_data)
                messages.success(request, ('Se creó el usuario seleccionado!'))
                fn = form1.cleaned_data['first_name']
                ln = form1.cleaned_data['last_name']
                ci = form2.cleaned_data['ci']
                tel = form2.cleaned_data['telefono']
                dir = form2.cleaned_data['direccion']
                des = form2.cleaned_data['descripcion']
                User.objects.filter(id = new_user.id).update(first_name=fn,last_name=ln)
                Usuario.objects.filter(user_id=new_user.id).update(ci = ci, telefono=tel,direccion=dir,descripcion=des)
                try:
                    email = EmailMessage('Bienvenido a SGPA', 'Le damos la bienvenida a SGPA (Sistema de Gestion de Proyectos Agiles)\nSu usuario es '+new_user.username+'.\nSu contaseña temporal es '+passw+'.', to=[new_user.email])
                    email.send()
                except:
                    pass
                return redirect('/usuario/')
        else:
            form = CreateUserForm()
            form1 = UserForm2()
            form2 = UsuarioForm()
        return render(request, 'usuario/create_usuario.html', {'form': form,
                                                               'form1': form1,
                                                               'form2':form2})
    else:
        return HttpResponseForbidden()


@login_required
@transaction.atomic
def list_usuario(request):
    """
      Buscar un usuario por username o ci
      :param request:
      """
    if request.user.is_superuser:
        if request.method == 'POST':
            busqueda = request.POST['buscalo']
            """
            Filtra de acuerdo a la palabra que se indico 
            :param request:
            """
            if busqueda:
                match = Usuario.objects.filter(Q(user__username__icontains=busqueda) | Q(ci__icontains=busqueda))

                if match:
                    return render(request, 'usuario/list_usuario.html', {'users': match})
                else:
                    messages.error(request, 'No existe un usuario con ese nombre')
            else:
                matchh = Usuario.objects.filter(Q(user__username__icontains=busqueda) | Q(ci__icontains=busqueda))
                return render(request, 'usuario/list_usuario.html', {'users': matchh.all()})
        else:
            users = Usuario.objects.all()
            return render(request, 'usuario/list_usuario.html', {'users': users})

        return render(request, 'usuario/list_usuario.html')
    else:
        return HttpResponseForbidden()
@login_required
@transaction.atomic
def cambiar_password(request):
    """
    Esta view permite a un User cambiar su propia contraseña
    :param request:
    """
    if request.method == "POST" and 'passw' in request.POST:
        form = CambiarPasswordForm(request.POST)
        if form.is_valid():
            u = User.objects.get(username__exact=request.user.username)
            passw = form.cleaned_data['password']
            u.set_password(passw)
            u.save()
            messages.success(request, ('Se cambió la contraseña!'))
            # redirect, or however you want to get to the main view
            return redirect('/menu')
    else:
        form = CambiarPasswordForm(request.POST)

    return render(request, 'usuario/cambiar_passw.html', {'form': form})

@login_required
@transaction.atomic
def buscar_username(request):
    """
    Buscar un usuario por username o ci
    :param request:
    """
    if request.user.is_superuser:
        if request.method == 'POST':
            busqueda = request.POST['buscalo']
            """
            Filtra de acuerdo a la palabra que se indico 
            :param request:
            """
            if busqueda:
                match = Usuario.objects.filter(Q(user__username__contains=busqueda) | Q(ci__contains=busqueda) )

                if match:
                    return render(request, 'usuario/buscar_username.html', {'sr': match})
                else:
                    messages.error(request, 'No existe un usuario con ese nombre')
            else:
                return redirect("/usuario/buscar_username/")
        else:
            users = Usuario.objects.all()
            return render(request, 'usuario/buscar_username.html', {'sr': users})

        return render(request, 'usuario/buscar_username.html')
    else:
        return HttpResponseForbidden()

@login_required
@transaction.atomic
def buscar_ci(request):
    """
    Buscar un usuario por ci
    :param request:
    """

    if request.method == 'POST':
        busqueda = request.POST['buscalo']
        """
        Filtra de acuerdo a la palabra que se indico 
        :param request:
        """
        if busqueda:
            match = Usuario.objects.filter(Q(ci__contains=busqueda))
            if match:
                return render(request, 'usuario/buscar_ci.html', {'sr': match})
            else:
                messages.error(request, 'No existe un usuario con ese CI')
        else:
            matchh = Usuario.objects.filter(Q(ci__contains=busqueda))
            return render(request, 'usuario/buscar_ci.html', {'sr': matchh.all()})

    return render(request, 'usuario/buscar_ci.html')



@login_required
@transaction.atomic
def confirmado (request,pk):
    """
    Página que confirma que un proceso fue realizado con éxito
    :param request:
    :param pk:
    """
    if request.user.is_superuser:
        return render(request, 'usuario/confirmado.html')
    else:
        return HttpResponseForbidden()

@login_required
@transaction.atomic
def confirmado2 (request):
    """
    Página que confirma que un proceso fue realizado con éxito
    :param request:
    """
    return render(request, 'usuario/confirmado.html')

@login_required
@transaction.atomic
def confirmado_usuario (request):
    """
    Página que confirma que un proceso fue realizado con éxito
    :param request:
    """
    return render(request, 'usuario/confirmado2.html')

@login_required
@transaction.atomic
def list_usuario2(request, pk):
    """
    Esta view lista los datos del rol de acuerdo al id seleccionado
    :param request:
    """
    if request.user.is_superuser:
        users = User.objects.filter(id=pk)
        usuario = Usuario.objects.filter(user_id=pk)
        contexto = {'users': users}
        return render(request, 'usuario/visualizar_usuario.html', contexto)
    else:
        return HttpResponseForbidden()

@login_required
@transaction.atomic
def visualisar_mio(request):
    """
    :param request:
    """
    user = request.user
    return render(request, 'usuario/visualizar_mio.html', {'user': user})

from django.db import models
from django.contrib.postgres.fields import ArrayField


class Permiso(models.Model):
    nombre = models.CharField(max_length=80)
    orden = models.IntegerField(default=0)

    def __str__(self):
        return self.nombre


class Rol(models.Model):
    id_rol = models.AutoField
    nombre = models.CharField(max_length=80, unique=True)
    descripcion = models.TextField(max_length=300)
    # debe contar con al menos un Rol
    permiso = models.ManyToManyField(Permiso, blank=False, null=True)

    def __str__(self):
        return self.nombre

    def set_nombre(self, nom):
        self.nombre = nom

    def get_nombre(self):
        return self.nombre

    def set_descripcion(self, desc):
        self.descripcion = desc

    def get_descripcion(self):
        return self.descripcion

    def set_permiso(self, permi):
        self.permiso = permi

    def get_permiso(self):
        return self.permiso

    def agregar_permisos(self, permiso):
        self.permisos.append(permiso)